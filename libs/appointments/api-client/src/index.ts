import {
  httpClientBase,
  HttpClientMethods,
  StorageKeys,
  StorageService,
  StorageType,
} from '@health-code-ui/shared/util';

const baseUrl = '/api/appointments';
const authState = StorageService.getItem(StorageKeys.AuthState, StorageType.SessionStorage);

export const getAllAppointments = (startDate: string, endDate: string) => {
  const url = `${baseUrl}?startDate=${startDate}&endDate=${endDate}`;

  return httpClientBase(url, { token: authState.user.jwttoken });
};

export const createAppointment = (body: any) =>
  httpClientBase(baseUrl, { body, token: authState.user.jwttoken, method: HttpClientMethods.POST });

export const getAppointmentById = (appointmentId: string) =>
  httpClientBase(`${baseUrl}/${appointmentId}`, { token: authState.user.jwttoken });

export const deleteAppointment = (appointmentId: string) =>
  httpClientBase(`${baseUrl}/${appointmentId}`, { token: authState.user.jwttoken, method: HttpClientMethods.DELETE });

export const getAvailableAppointmentSlots = (startDate: string, endDate: string) => {
  const url = `${baseUrl}/availability?startDate=${encodeURIComponent(startDate)}&endDate=${encodeURIComponent(
    endDate
  )}`;

  return httpClientBase(url, { token: authState.user.jwttoken });
};

export const rescheduleAppointment = (body: any) =>
  httpClientBase(`${baseUrl}/reschedule`, { body, token: authState.user.jwttoken, method: HttpClientMethods.POST });
