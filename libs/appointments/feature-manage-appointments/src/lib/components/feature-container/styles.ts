import { cssVariables } from '@health-code-ui/shared/ui';
import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
  backIconButton: {
    color: cssVariables.colors.tahitiGold,
  },
  lightText: {
    fontWeight: 300,
    marginRight: 8,
  },
  boldText: {
    color: cssVariables.colors.black,
    fontWeight: 400,
  },
  pageSubTitle: {
    fontWeight: 300,
    marginLeft: 50,
    marginBottom: 30,
    fontSize: 22,
  },
});
