import React, { useEffect, useState } from 'react';
import { ConfirmDialog, ConfirmDialogProps } from '@health-code-ui/shared/ui';
import { Box, IconButton, Typography } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import { useManageAppointments } from '../../context';
import { addYears } from 'date-fns';
import { quicklinksBasePath } from '@health-code-ui/quicklinks/state';
import { useStyles } from './styles';
import { ApplicationExecutionContext } from '@health-code-ui/shared/models';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import { AppointmentTable } from '../appointment-table';

export const ManageAppointmentsFeatureContainer = (props) => {
  const classes = useStyles();
  const history = useHistory();

  const { applicationContext } = props;
  const { actions } = useManageAppointments();
  const [openConfirmDialog, setOpenConfirmDialog] = useState<boolean>(false);

  const confirmDialogProps: ConfirmDialogProps = {
    open: openConfirmDialog,
    dialogTitle: 'Exit',
    dialogText: 'Would you like to go back to the home screen?',
    negativeActionText: 'No',
    positiveActionText: 'Yes',
    onClose: (result: boolean) => {
      if (result) {
        history.push(quicklinksBasePath);
      }

      setOpenConfirmDialog(false);
    },
  };

  useEffect(() => {
    const startDate = new Date();
    const endDate = addYears(startDate, 1);

    actions.adminGetAppointments(startDate.toISOString().replace('Z', ''), endDate.toISOString().replace('Z', ''));
  }, []);

  return (
    <>
      <Box paddingLeft="80px" paddingRight="80px" paddingTop="60px">
        <Box display="flex">
          <IconButton className={classes.backIconButton} onClick={() => setOpenConfirmDialog(true)}>
            <ChevronLeftIcon />
          </IconButton>
          <Typography variant="h4" className={classes.lightText}>
            Manage
          </Typography>
          <Typography variant="h4" className={classes.boldText}>
            Appointments
          </Typography>
        </Box>

        {applicationContext === ApplicationExecutionContext.Admin ? (
          <Typography variant="body1" className={classes.pageSubTitle}>
            Manage Client Appointments
          </Typography>
        ) : (
          <Typography variant="body1" className={classes.pageSubTitle}>
            Manage Booking Appointments
          </Typography>
        )}

        <AppointmentTable applicationContext={applicationContext} />
      </Box>
      <ConfirmDialog {...confirmDialogProps} />
    </>
  );
};
