import React, { memo } from 'react';
import { Avatar } from '@material-ui/core';
import { useStyles, colorsClasses } from './styles';

export const AppointmentTableRowAvatar = memo((props: any) => {
  const classes = useStyles({ colorIndex: Math.floor(Math.random() * colorsClasses.length) });
  const { name, surname } = props;

  return (
    <Avatar className={classes.root}>
      {name.charAt(0)}
      {surname.charAt(0)}
    </Avatar>
  );
});
