import {
  red,
  pink,
  purple,
  deepPurple,
  indigo,
  blue,
  lightBlue,
  cyan,
  teal,
  green,
  lime,
  lightGreen,
  yellow,
  amber,
  orange,
  deepOrange,
  brown,
} from '@material-ui/core/colors';
import { createStyles, makeStyles, Theme } from '@material-ui/core';

export const colorsClasses = [
  red,
  pink,
  purple,
  deepPurple,
  indigo,
  blue,
  lightBlue,
  cyan,
  teal,
  green,
  lime,
  lightGreen,
  yellow,
  amber,
  orange,
  deepOrange,
  brown,
];

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      color: (props: any) => theme.palette.getContrastText(colorsClasses[props.colorIndex][300]),
      backgroundColor: (props: any) => colorsClasses[props.colorIndex][300],
    },
  })
);
