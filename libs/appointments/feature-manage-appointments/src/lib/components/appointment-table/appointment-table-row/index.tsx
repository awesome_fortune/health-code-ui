import React, { useState } from 'react';
import { Box, Collapse, createStyles, TableCell, TableRow, Theme, Typography, withStyles } from '@material-ui/core';
import { cssVariables } from 'libs/shared/ui/src/themes/cssVariables';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import WarningIcon from '@material-ui/icons/Warning';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { AppointmentDetailsForm } from '../appointment-details-form';
import { AppointmentTableRowAvatar } from '../appointment-table-row-avatar';
import { useStyles } from './styles';
import { format } from 'date-fns';
import { ApplicationExecutionContext } from '@health-code-ui/shared/models';

export interface AppointmentTableRowProps {
  applicationContext: ApplicationExecutionContext;
  patient?: any;
  appointmentTime?: any;
  appointmentStatus?: any;
}

const StyledTableRow = withStyles((theme: Theme) =>
  createStyles({
    root: {
      backgroundColor: cssVariables.colors.white,
      boxShadow: `0px 2px 20px ${cssVariables.colors.blackAlpha2}`,
      borderRadius: '26px',
    },
  })
)(TableRow);

const StyledTableCell = withStyles((theme: Theme) =>
  createStyles({
    root: {
      borderBottom: 0,
    },
  })
)(TableCell);

export const AppointmentTableRow = (props: AppointmentTableRowProps) => {
  const classes = useStyles();
  const [rowOpen, setRowOpen] = useState<boolean>(false);
  const { patient, appointmentTime, appointmentStatus } = props;

  return (
    <>
      <StyledTableRow onClick={() => setRowOpen(!rowOpen)}>
        <StyledTableCell style={{ borderTopLeftRadius: 26, borderBottomLeftRadius: !rowOpen ? 26 : 0 }}>
          <AppointmentTableRowAvatar {...patient} />
        </StyledTableCell>
        <StyledTableCell>
          <Typography>{patient.name}</Typography>
        </StyledTableCell>
        <StyledTableCell>{patient.surname}</StyledTableCell>
        <StyledTableCell>
          <Box display="flex" alignItems="center">
            <AccessTimeIcon className={classes.icon} />
            <Typography>{format(appointmentTime, 'dd MMMM Y, p')}</Typography>
          </Box>
        </StyledTableCell>
        <StyledTableCell>
          <Box display="flex" alignItems="center">
            <WarningIcon className={classes.icon} />
            <Typography>{appointmentStatus}</Typography>
          </Box>
        </StyledTableCell>
        <StyledTableCell style={{ borderTopRightRadius: 26, borderBottomRightRadius: !rowOpen ? 26 : 0 }}>
          {rowOpen ? <ExpandLessIcon /> : <ExpandMoreIcon />}
        </StyledTableCell>
      </StyledTableRow>
      <TableRow style={{ backgroundColor: rowOpen ? cssVariables.colors.white : 'transparent' }}>
        <StyledTableCell style={{ borderBottomLeftRadius: 26 }} />
        <StyledTableCell colSpan={4}>
          <Collapse in={rowOpen} timeout="auto" unmountOnExit>
            <AppointmentDetailsForm {...props} />
          </Collapse>
        </StyledTableCell>
        <StyledTableCell style={{ borderBottomRightRadius: 26 }} />
      </TableRow>
    </>
  );
};
