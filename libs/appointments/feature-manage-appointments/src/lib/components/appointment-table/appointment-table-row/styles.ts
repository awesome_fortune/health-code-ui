import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
  icon: {
    marginRight: 10,
  },
});
