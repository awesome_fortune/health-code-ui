import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
  form: {
    display: 'flex',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    maxWidth: '90%',
    marginBottom: 20,
  },
  detailsHeaderText: {
    fontWeight: 300,
    fontSize: 22,
    marginBottom: 15,
  },
});
