import { ApplicationExecutionContext } from '@health-code-ui/shared/models';
import { Box, Button, TextField, Typography } from '@material-ui/core';
import { format } from 'date-fns';
import { useActionButtonStyles } from 'libs/shared/ui/src/themes/styles/useActionButtonStyles';
import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import { useStyles } from './styles';
import { appointmentsRoutes } from '@health-code-ui/appointments/state';
import { ConfirmDialog, ConfirmDialogProps } from '@health-code-ui/shared/ui';

export interface AppointmentDetailsFormProps {
  applicationContext: ApplicationExecutionContext;
  patient?: any;
  appointmentTime?: any;
}

export const AppointmentDetailsForm = (props: AppointmentDetailsFormProps) => {
  const classes = useStyles();
  const buttonClasses = useActionButtonStyles();
  const { register } = useForm();
  const { patient, appointmentTime, applicationContext } = props;
  const history = useHistory();
  const [openConfirmCancelAppointmentDialog, setOpenConfirmCancelAppointmentDialog] = useState<boolean>(false);
  const [openConfirmRescheduleAppointmentDialog, setOpenConfirmRescheduleAppointmentDialog] = useState<boolean>(false);

  const confirmCancleAppointmentProps: ConfirmDialogProps = {
    open: openConfirmCancelAppointmentDialog,
    dialogTitle: 'Cancel Appointment',
    dialogText: 'DO you want to cancel your appointment?',
    negativeActionText: 'No',
    positiveActionText: 'Yes',
    onClose: (result: boolean) => {
      if (result) {
        // Cancel appointment
      }

      setOpenConfirmCancelAppointmentDialog(false);
    },
  };

  const confirmRescheduleAppointmentProps: ConfirmDialogProps = {
    open: openConfirmRescheduleAppointmentDialog,
    dialogTitle: 'Reschedule Appointment',
    dialogText: 'Do you want to reschedule your appointment?',
    negativeActionText: 'No',
    positiveActionText: 'Yes',
    onClose: (result: boolean) => {
      if (result) {
        history.push(appointmentsRoutes.rescheduleAppointment.path);
      }

      setOpenConfirmRescheduleAppointmentDialog(false);
    },
  };

  return (
    <>
      <Box>
        <Typography variant="body1" className={classes.detailsHeaderText}>
          Appointment Details
        </Typography>
        <form noValidate className={classes.form}>
          <Box display="flex" flexBasis="30%" marginBottom="20px">
            <TextField
              inputRef={register}
              defaultValue={patient.name}
              fullWidth
              type="text"
              id="name"
              name="name"
              label="Name"
            />
          </Box>
          <Box display="flex" flexBasis="30%" marginBottom="20px">
            <TextField
              inputRef={register}
              defaultValue={patient.surname}
              fullWidth
              type="text"
              id="surname"
              name="surname"
              label="Surname"
            />
          </Box>
          <Box display="flex" flexBasis="30%" marginBottom="20px">
            <TextField
              inputRef={register}
              defaultValue={format(appointmentTime, 'dd MMMM Y')}
              fullWidth
              type="text"
              id="dateOfAppointment"
              name="dateOfAppointment"
              label="Date of Appointment"
            />
          </Box>
          <Box display="flex" flexBasis="30%" marginBottom="20px">
            <TextField
              inputRef={register}
              defaultValue={format(appointmentTime, 'p')}
              fullWidth
              type="text"
              id="timeOfAppointment"
              name="timeOfAppointment"
              label="Time of Appointment"
            />
          </Box>
          <Box display="flex" flexBasis="30%" marginBottom="20px">
            <TextField
              inputRef={register}
              defaultValue={patient.cellNumber}
              fullWidth
              type="tel"
              id="cellNumber"
              name="cellNumber"
              label="Cell Number"
            />
          </Box>
          <Box display="flex" flexBasis="30%" marginBottom="20px">
            <TextField
              inputRef={register}
              defaultValue={patient.emailAddress}
              fullWidth
              type="email"
              id="emailAddress"
              name="emailAddress"
              label="Email Address"
            />
          </Box>
        </form>
        {applicationContext === ApplicationExecutionContext.Admin ? (
          <Box display="flex" justifyContent="space-between" width="32%">
            <Button
              color="primary"
              variant="outlined"
              classes={{
                root: buttonClasses.root,
                containedPrimary: buttonClasses.containedPrimary,
                label: buttonClasses.label,
              }}
            >
              Accept
            </Button>
            <Button
              color="primary"
              variant="outlined"
              classes={{
                root: buttonClasses.root,
                containedPrimary: buttonClasses.containedPrimary,
                label: buttonClasses.label,
              }}
            >
              Defer
            </Button>
            <Button
              color="primary"
              variant="outlined"
              classes={{
                root: buttonClasses.root,
                containedPrimary: buttonClasses.containedPrimary,
                label: buttonClasses.label,
              }}
            >
              Reject
            </Button>
          </Box>
        ) : (
          <Box display="flex" justifyContent="space-between" width="32%">
            <Button
              onClick={() => setOpenConfirmRescheduleAppointmentDialog(true)}
              color="primary"
              variant="outlined"
              classes={{
                root: buttonClasses.root,
                containedPrimary: buttonClasses.containedPrimary,
                label: buttonClasses.label,
              }}
            >
              Reschedule
            </Button>
            <Button
              onClick={() => setOpenConfirmCancelAppointmentDialog(true)}
              color="primary"
              variant="outlined"
              classes={{
                root: buttonClasses.root,
                containedPrimary: buttonClasses.containedPrimary,
                label: buttonClasses.label,
              }}
            >
              Cancel
            </Button>
          </Box>
        )}
      </Box>

      <ConfirmDialog {...confirmRescheduleAppointmentProps} />
      <ConfirmDialog {...confirmCancleAppointmentProps} />
    </>
  );
};
