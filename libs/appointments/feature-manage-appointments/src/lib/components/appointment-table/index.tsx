import React from 'react';
import { Box, Table, TableBody, TableContainer } from '@material-ui/core';
import { AppointmentTableHead } from './appointment-table-head';
import { AppointmentTableRow } from './appointment-table-row';
import { useStyles } from './styles';
import { addDays, addHours } from 'date-fns';
import { ApplicationExecutionContext } from '@health-code-ui/shared/models';
import { useManageAppointments } from '../../context';

export interface AppointmentTableProps {
  applicationContext: ApplicationExecutionContext;
}

const appointments = [
  {
    id: 1,
    patient: {
      name: 'Kumbi',
      surname: 'Mtshakazi',
      emailAddress: 'kumbi@email.com',
      cellNumber: '0831111111',
    },
    appointmentTime: addHours(addDays(new Date(), 1), 3),
    appointmentStatus: 'Awaiting Confirmation',
  },
  {
    id: 2,
    patient: {
      name: 'Nkosinathi',
      surname: 'Maredi',
      emailAddress: 'nkosinathi@email.com',
      cellNumber: '083222222',
    },
    appointmentTime: addHours(addDays(new Date(), 2), 2),
    appointmentStatus: 'Awaiting Confirmation',
  },
  {
    id: 3,
    patient: {
      name: 'Sandra',
      surname: 'Jacobs',
      emailAddress: 'sandra@email.com',
      cellNumber: '083333333',
    },
    appointmentTime: addHours(addDays(new Date(), 3), 1),
    appointmentStatus: 'Awaiting Confirmation',
  },
];

export const AppointmentTable = (props: AppointmentTableProps) => {
  const classes = useStyles();
  const { state } = useManageAppointments();
  console.log(state);

  const { applicationContext } = props;

  return (
    <TableContainer component={Box}>
      <Table aria-label="Appointment table" className={classes.table}>
        <AppointmentTableHead />
        <TableBody>
          {appointments.map((appointment) => (
            <AppointmentTableRow key={appointment.id} applicationContext={applicationContext} {...appointment} />
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};
