import { createStyles, TableCell, TableHead, TableRow, Theme, withStyles } from '@material-ui/core';
import React from 'react';

const StyledTableCell = withStyles((theme: Theme) =>
  createStyles({
    root: {
      borderBottom: 0,
    },
  })
)(TableCell);

export const AppointmentTableHead = () => {
  return (
    <TableHead>
      <TableRow>
        <StyledTableCell></StyledTableCell>
        <StyledTableCell>Name</StyledTableCell>
        <StyledTableCell>Surname</StyledTableCell>
        <StyledTableCell>Appointment Time</StyledTableCell>
        <StyledTableCell>Status</StyledTableCell>
        <StyledTableCell></StyledTableCell>
      </TableRow>
    </TableHead>
  );
};
