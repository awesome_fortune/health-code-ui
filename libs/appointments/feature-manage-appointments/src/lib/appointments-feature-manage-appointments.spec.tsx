import React from 'react';
import { render } from '@testing-library/react';

import AppointmentsFeatureManageAppointments from './appointments-feature-manage-appointments';

describe('AppointmentsFeatureManageAppointments', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<AppointmentsFeatureManageAppointments />);
    expect(baseElement).toBeTruthy();
  });
});
