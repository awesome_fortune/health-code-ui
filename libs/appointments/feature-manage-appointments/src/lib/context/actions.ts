export enum ManageAppointmentsActionTypes {
  AcceptAppointment = 'AcceptAppointment',
  RejectAppointment = 'RejectAppointment',
  DeferAppointment = 'DeferAppointment',
  AdminGetAppointments = 'AdminGetAppointments',
  AdminGetAppointmentsSuccess = 'AdminGetAppointmentsSuccess',
  AdminGetAppointmentsError = 'AdminGetAppointmentsError',
  SetManageAppointmentsLoading = 'SetManageAppointmentsLoading',
}

export type ManageAppointmentsActions =
  | { type: ManageAppointmentsActionTypes.AcceptAppointment }
  | { type: ManageAppointmentsActionTypes.DeferAppointment }
  | { type: ManageAppointmentsActionTypes.RejectAppointment }
  | { type: ManageAppointmentsActionTypes.AdminGetAppointments; payload: any }
  | { type: ManageAppointmentsActionTypes.AdminGetAppointmentsError; payload: any }
  | { type: ManageAppointmentsActionTypes.AdminGetAppointmentsSuccess; payload: any }
  | { type: ManageAppointmentsActionTypes.SetManageAppointmentsLoading; payload: boolean };
