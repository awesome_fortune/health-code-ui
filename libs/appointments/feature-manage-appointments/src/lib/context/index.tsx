import React, { useContext, createContext, useReducer } from 'react';
import { ManageAppointmentsActions, ManageAppointmentsActionTypes } from './actions';
import * as appointmentsApiClient from '@health-code-ui/appointments/api-client';

export interface ManageAppointmentsState {
  isLoading: boolean;
  appointments: any;
}

const initialState: ManageAppointmentsState = {
  isLoading: false,
  appointments: null,
};

const ManageAppointmentsContext = createContext<any>({ state: initialState, actions: null });

const useActions = (dispatch) => ({
  adminGetAppointments: (startDate, endDate) =>
    dispatch({ type: ManageAppointmentsActionTypes.AdminGetAppointments, payload: { startDate, endDate } }),
});

const applySideEffectsMiddleware = (dispatch) => async (action: ManageAppointmentsActions) => {
  switch (action.type) {
    case ManageAppointmentsActionTypes.AdminGetAppointments:
      try {
        dispatch({ type: ManageAppointmentsActionTypes.SetManageAppointmentsLoading, payload: true });

        const { startDate, endDate } = action.payload;
        const response = await appointmentsApiClient.getAllAppointments(startDate, endDate);

        dispatch({ type: ManageAppointmentsActionTypes.AdminGetAppointmentsSuccess, payload: response });
      } catch (error) {
        dispatch({ type: ManageAppointmentsActionTypes.AdminGetAppointmentsError, payload: action.payload });
      }
      break;
    default:
      dispatch(action);
      break;
  }
};

const manageAppointmentsReducer = (state: ManageAppointmentsState, action: ManageAppointmentsActions) => {
  switch (action.type) {
    case ManageAppointmentsActionTypes.AcceptAppointment:
      return {
        ...state,
      };
    case ManageAppointmentsActionTypes.AdminGetAppointmentsSuccess:
      return {
        ...state,
        appointments: action.payload,
        isLoading: false,
      };
    case ManageAppointmentsActionTypes.AdminGetAppointmentsError:
      return {
        ...state,
        error: action.payload,
        isLoading: false,
      };
    case ManageAppointmentsActionTypes.SetManageAppointmentsLoading:
      return {
        ...state,
        isLoading: action.payload,
      };
    default:
      throw new Error(`Unsupported action type: ${action.type}`);
  }
};

export const useManageAppointments = () => {
  const context = useContext(ManageAppointmentsContext);

  if (!context) {
    throw new Error('useManageAppointments must be used within a ManageAppointmentsProvider');
  }

  return context;
};

export const ManageAppointmentsProvider = (props) => {
  const [state, dispatch] = useReducer(manageAppointmentsReducer, initialState);
  const actions = useActions(applySideEffectsMiddleware(dispatch));

  return <ManageAppointmentsContext.Provider value={{ state, actions }} {...props} />;
};
