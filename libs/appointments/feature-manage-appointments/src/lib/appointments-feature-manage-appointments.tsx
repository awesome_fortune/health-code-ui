import React from 'react';
import { ApplicationExecutionContext } from '@health-code-ui/shared/models';
import { ManageAppointmentsProvider } from './context';
import { ManageAppointmentsFeatureContainer } from './components/feature-container';

export interface AppointmentsFeatureManageAppointmentsProps {
  applicationContext: ApplicationExecutionContext;
}

export const AppointmentsFeatureManageAppointments = (props: AppointmentsFeatureManageAppointmentsProps) => {
  return (
    <ManageAppointmentsProvider>
      <ManageAppointmentsFeatureContainer {...props} />
    </ManageAppointmentsProvider>
  );
};

export default AppointmentsFeatureManageAppointments;
