import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
  buttonRoot: {
    marginLeft: 8,
  },
});
