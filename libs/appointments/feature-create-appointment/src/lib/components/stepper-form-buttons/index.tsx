import React from 'react';
import { useActionButtonStyles } from '@health-code-ui/shared/ui';
import { Button } from '@material-ui/core';
import { useStyles } from './styles';

export interface StepperFormButtonsProps {
  handleBackClick(): void;
}

export const StepperFormButtons = (props: StepperFormButtonsProps) => {
  const classes = useStyles();
  const buttonClasses = useActionButtonStyles();

  return (
    <>
      {
        <Button
          onClick={props.handleBackClick}
          variant="outlined"
          color="primary"
          classes={{
            root: buttonClasses.root,
            containedPrimary: buttonClasses.containedPrimary,
            label: buttonClasses.label,
          }}
        >
          Back
        </Button>
      }
      <Button
        type="submit"
        color="primary"
        variant="contained"
        classes={{
          root: `${buttonClasses.root} ${classes.buttonRoot}`,
          containedPrimary: buttonClasses.containedPrimary,
          label: buttonClasses.label,
        }}
      >
        Next
      </Button>
    </>
  );
};
