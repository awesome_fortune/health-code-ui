import React, { useState } from 'react';
import { Box, IconButton, Step, StepLabel, Stepper, Typography } from '@material-ui/core';
import { useCreateAppointment } from '../../context';
import { FamilyMembersStep } from '../form-steps/family-members-step';
import { PatientDetailsStep } from '../form-steps/patient-details-step';
import { PaymentTypeStep } from '../form-steps/payment-type-step';
import { PersonResponsibleForAccountStep } from '../form-steps/person-responsible-for-account-step';
import { SelectDateStep } from '../form-steps/select-date-step';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import { useStyles } from './styles';
import { useHistory } from 'react-router-dom';
import { ConfirmDialog, ConfirmDialogProps } from '@health-code-ui/shared/ui';
import { quicklinksBasePath } from '@health-code-ui/quicklinks/state';

const getSteps = () => [
  'Select Date',
  'Patient Details',
  'Person Responsible for Account',
  'Payment Type',
  'Family Members',
];

export const CreateAppointmentFeatureContainer = () => {
  const classes = useStyles();
  const { state } = useCreateAppointment();
  const { currentStep } = state;
  const history = useHistory();
  const [openConfirmDialog, setOpenConfirmDialog] = useState<boolean>(false);

  const confirmDialogProps: ConfirmDialogProps = {
    open: openConfirmDialog,
    dialogTitle: 'Exit',
    dialogText: 'Would you like to go back to the home screen?',
    negativeActionText: 'No',
    positiveActionText: 'Yes',
    onClose: (result: boolean) => {
      if (result) {
        history.push(quicklinksBasePath);
      }

      setOpenConfirmDialog(false);
    },
  };

  const handleBackButtonClick = () => setOpenConfirmDialog(true);

  const showStep = (step: number) => {
    switch (step) {
      case 0:
        return <SelectDateStep />;
      case 1:
        return <PatientDetailsStep />;
      case 2:
        return <PersonResponsibleForAccountStep />;
      case 3:
        return <PaymentTypeStep />;
      case 4:
        return <FamilyMembersStep />;
      default:
        throw new Error(`Step ${step} is invalid`);
    }
  };

  return (
    <>
      <Box paddingLeft="80px" paddingRight="80px" paddingTop="60px">
        <Box display="flex">
          <IconButton className={classes.backIconButton} onClick={handleBackButtonClick}>
            <ChevronLeftIcon />
          </IconButton>
          <Typography variant="h4" className={classes.lightText}>
            Create
          </Typography>
          <Typography variant="h4" className={classes.boldText}>
            Appointment
          </Typography>
        </Box>

        <Box display="flex" flexDirection="column">
          <Stepper activeStep={currentStep} className={classes.stepper}>
            {getSteps().map((label) => (
              <Step key={label}>
                <StepLabel>{label}</StepLabel>
              </Step>
            ))}
          </Stepper>

          {showStep(currentStep)}
        </Box>
      </Box>

      <ConfirmDialog {...confirmDialogProps} />
    </>
  );
};
