import { cssVariables } from '@health-code-ui/shared/ui';
import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
  tableRow: {
    borderTop: '20px solid transparent',
    borderBottom: '20px solid transparent',
    backgroundColor: cssVariables.colors.white,
    borderRadius: 7,
    boxShadow: `0px 2px 15px ${cssVariables.colors.blackAlpha2}`,
  },
  viewPatientButtonRoot: {
    backgroundColor: cssVariables.colors.white,
    border: `1px solid ${cssVariables.colors.tahitiGold}`,
    boxShadow: 'none',
    '&:hover': {
      backgroundColor: cssVariables.colors.tahitiGold,
    },
  },
  viewPatientButtonLabel: {
    color: cssVariables.colors.black,
    fontSize: 18,
    letterSpacing: '0.23px',
    fontWeight: 300,
    '&:hover': {
      color: cssVariables.colors.white,
    },
  },
  blueText: {
    color: cssVariables.colors.poloBlue,
    fontSize: 14,
    letterSpacing: '0.3px',
    fontWeight: 700,
  },
  patientNameText: {
    color: cssVariables.colors.black,
    fontWeight: 300,
    fontSize: 24,
    letterSpacing: '0.3px',
  },
  regularText: {
    fontSize: 14,
    color: cssVariables.colors.boulder,
    letterSpacing: '0.35px',
  },
  trashIcon: {
    color: cssVariables.colors.black,
  },
});
