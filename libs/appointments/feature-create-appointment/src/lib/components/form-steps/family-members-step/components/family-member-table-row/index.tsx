import React from 'react';
import { Box, Fab, IconButton, TableCell, TableRow, Typography } from '@material-ui/core';
import { format, parseISO } from 'date-fns';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import { useStyles } from './styles';

export const FamilyMemberTableRow = (props) => {
  const classes = useStyles();
  const { name, relation, dateAdded } = props.familyMember;

  return (
    <TableRow className={classes.tableRow}>
      <TableCell>
        <Fab classes={{ root: classes.viewPatientButtonRoot, label: classes.viewPatientButtonLabel }} color="primary">
          View
        </Fab>
      </TableCell>
      <TableCell className={classes.patientNameText}>{name}</TableCell>
      <TableCell className={classes.blueText}>{relation}</TableCell>
      <TableCell className={classes.regularText}>{format(parseISO(dateAdded), 'dd / LL / yyyy')}</TableCell>
      <TableCell>
        <Box display="flex" flexDirection="column" alignItems="center">
          <IconButton>
            <DeleteForeverIcon className={classes.trashIcon} />
          </IconButton>
          <Typography variant="body1" className={classes.regularText}>
            Remove
          </Typography>
        </Box>
      </TableCell>
    </TableRow>
  );
};
