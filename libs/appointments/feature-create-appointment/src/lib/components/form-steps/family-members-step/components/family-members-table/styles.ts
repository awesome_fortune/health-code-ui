import { cssVariables } from '@health-code-ui/shared/ui';
import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
  headerCellRoot: {
    borderBottom: 'none',
  },
  blueText: {
    color: cssVariables.colors.poloBlue,
    fontSize: 14,
    letterSpacing: '0.3px',
    fontWeight: 700,
  },
  regularText: {
    fontSize: 14,
    color: cssVariables.colors.boulder,
    letterSpacing: '0.35px',
  },
  table: {
    borderCollapse: 'separate',
    borderSpacing: '0 20px',
  },
});
