import React from 'react';
import { Box, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@material-ui/core';
import { FamilyMemberTableRow } from '../family-member-table-row';
import { useStyles } from './styles';

export const FamilyMembersTable = (props) => {
  const classes = useStyles();

  return (
    <TableContainer component={Box}>
      <Table aria-label="Family members table" className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell classes={{ root: classes.headerCellRoot }}></TableCell>
            <TableCell classes={{ root: `${classes.headerCellRoot} ${classes.blueText}` }}>Name and Surname</TableCell>
            <TableCell classes={{ root: `${classes.headerCellRoot} ${classes.blueText}` }}>Relation</TableCell>
            <TableCell classes={{ root: `${classes.headerCellRoot} ${classes.regularText}` }}>Date Added</TableCell>
            <TableCell classes={{ root: classes.headerCellRoot }}></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.familyMembers.map((familyMember, index) => (
            <FamilyMemberTableRow key={index} familyMember={familyMember} />
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};
