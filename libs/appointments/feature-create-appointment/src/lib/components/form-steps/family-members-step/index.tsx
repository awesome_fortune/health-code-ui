import React, { useEffect, useState } from 'react';
import { Box, Button } from '@material-ui/core';
import { FamilyMembersTable } from './components/family-members-table';
import { ConfirmDialog, ConfirmDialogProps, useActionButtonStyles } from '@health-code-ui/shared/ui';
import { addDays } from 'date-fns';
import { useCreateAppointment } from '../../../context';
import { useHistory } from 'react-router-dom';
import { appointmentsRoutes } from '@health-code-ui/appointments/state';

const familyMembers = [
  {
    name: 'John Doe',
    relation: 'Brother',
    dateAdded: addDays(new Date(), 1).toISOString(),
  },
  {
    name: 'Jane Doe',
    relation: 'Sister',
    dateAdded: addDays(new Date(), 2).toISOString(),
  },
];

export const FamilyMembersStep = () => {
  const buttonClasses = useActionButtonStyles();
  const { state, actions } = useCreateAppointment();
  const [openConfirmDialog, setOpenConfirmDialog] = useState<boolean>(false);
  const history = useHistory();

  const confirmDialogProps: ConfirmDialogProps = {
    open: openConfirmDialog,
    dialogTitle: 'Create appointment',
    dialogText: `Would you like to create an appointment for ${state.patientDetailsStepData.name} ${state.patientDetailsStepData.surname}?`,
    negativeActionText: 'No',
    positiveActionText: 'Yes',
    onClose: (result: boolean) => {
      if (result) {
        const payload = {
          patientId: 124,
          scheduleStartTime: state.selectDateStepData.currentDate,
        };

        actions.createAppointment(payload);
      }

      setOpenConfirmDialog(false);
    },
  };

  useEffect(() => {
    if (state.appointmentCreated) {
      history.push(appointmentsRoutes.manageAppointments.path);
    }
  }, [state.appointmentCreated]);

  return (
    <>
      <FamilyMembersTable familyMembers={familyMembers} />
      <Box display="flex" justifyContent="flex-end" marginBottom="40px">
        <Button
          variant="outlined"
          classes={{
            root: buttonClasses.root,
            containedPrimary: buttonClasses.containedPrimary,
            label: buttonClasses.label,
          }}
        >
          Add new
        </Button>
        <Button
          onClick={() => setOpenConfirmDialog(true)}
          style={{ marginLeft: 10 }}
          variant="outlined"
          color="primary"
          classes={{
            root: buttonClasses.root,
            containedPrimary: buttonClasses.containedPrimary,
            label: buttonClasses.label,
          }}
        >
          Finish
        </Button>
      </Box>

      <ConfirmDialog {...confirmDialogProps} />
    </>
  );
};
