import React, { useState } from 'react';
import { Box, Button } from '@material-ui/core';
import { AppointmentsCalendar, AppointmentsCalendarProps, useActionButtonStyles } from '@health-code-ui/shared/ui';
import { TimeSlot, TimeSlotStatus } from '@health-code-ui/shared/models';
import { addHours } from 'date-fns';
import { useCreateAppointment } from '../../../context';
import { FormProvider, useForm } from 'react-hook-form';
import { useStyles } from './styles';

export const SelectDateStep = () => {
  const classes = useStyles();
  const { actions } = useCreateAppointment();
  const buttonClasses = useActionButtonStyles();
  const [currentDate, setCurrentDate] = useState(new Date());
  const timeSlots: TimeSlot[] = [];
  const selectDateForm = useForm();

  const onSubmit = () => {
    actions.setSelectDateStepData(currentDate);
    actions.goToNextStep();
  };

  for (let i = 0; i < 6; i++) {
    timeSlots.push({
      dateTime: addHours(currentDate, i),
      name: `Available Slot ${i + 1}`,
      status: TimeSlotStatus.Available,
    });
  }

  const appointmentCalendarProps: AppointmentsCalendarProps = {
    currentDate,
    setCurrentDate,
    timeSlots,
  };

  return (
    <FormProvider {...selectDateForm}>
      <Box alignSelf="flex-start" marginBottom="40px">
        <form className={classes.form} noValidate autoComplete="off" onSubmit={selectDateForm.handleSubmit(onSubmit)}>
          <AppointmentsCalendar {...appointmentCalendarProps} />
          <Button
            type="submit"
            color="primary"
            variant="contained"
            classes={{
              root: `${buttonClasses.root} ${classes.buttonRoot}`,
              containedPrimary: buttonClasses.containedPrimary,
              label: buttonClasses.label,
            }}
          >
            Next
          </Button>
        </form>
      </Box>
    </FormProvider>
  );
};
