import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
  form: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-end',
  },
  buttonRoot: {
    marginTop: 20,
  },
});
