import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
  form: {
    display: 'flex',
    justifyContent: 'space-between',
    width: '81%',
    marginBottom: 40,
  },
});
