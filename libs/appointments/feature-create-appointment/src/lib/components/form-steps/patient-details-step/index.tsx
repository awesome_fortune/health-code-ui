import React from 'react';
import { useStyles } from './styles';
import { ContactDetailsSection, IdentificationDetailsSection, PersonalDetailsSection } from '@health-code-ui/shared/ui';
import { useCreateAppointment } from '../../../context';
import { FormProvider, useForm } from 'react-hook-form';
import { Box } from '@material-ui/core';
import { PatientDetailsModel } from '@health-code-ui/shared/models';
import { StepperFormButtons } from '../../stepper-form-buttons';

export const PatientDetailsStep = () => {
  const classes = useStyles();
  const { actions, state } = useCreateAppointment();
  const patientDetaisForm = useForm<PatientDetailsModel>({ mode: 'onBlur' });
  const { patientDetailsStepData }: { patientDetailsStepData: PatientDetailsModel } = state;

  const onSubmit = (data: PatientDetailsModel) => {
    if (patientDetaisForm.formState.isValid) {
      actions.setPatientDetailsStepData(data);
      actions.goToNextStep();
    }
  };

  const handleBackClick = () => actions.goToPreviousStep();

  return (
    <FormProvider {...patientDetaisForm}>
      <form className={classes.form} noValidate autoComplete="off" onSubmit={patientDetaisForm.handleSubmit(onSubmit)}>
        <Box>
          <IdentificationDetailsSection {...patientDetailsStepData} />
        </Box>
        <Box>
          <PersonalDetailsSection {...patientDetailsStepData} />
        </Box>
        <Box>
          <ContactDetailsSection {...patientDetailsStepData} />
          <Box display="flex" justifyContent="space-between" marginTop="20px">
            <StepperFormButtons handleBackClick={handleBackClick} />
          </Box>
        </Box>
      </form>
    </FormProvider>
  );
};
