import React, { useState } from 'react';
import { PaymentType } from '@health-code-ui/shared/models';
import { MedicalAidDetailsSection } from '@health-code-ui/shared/ui';
import { Box, FormControl, FormControlLabel, FormLabel, Radio, RadioGroup } from '@material-ui/core';
import { Controller, FormProvider, useForm } from 'react-hook-form';
import { useCreateAppointment } from '../../../context';
import { PaymentTypeStepModel } from '../../../models/payment-type-step.model';
import { StepperFormButtons } from '../../stepper-form-buttons';

export const PaymentTypeStep = () => {
  const { actions, state } = useCreateAppointment();
  const { paymentTypeStepData }: { paymentTypeStepData: PaymentTypeStepModel } = state;
  const paymentTypeForm = useForm({
    mode: 'onBlur',
    defaultValues: {
      ...paymentTypeStepData,
    },
  });
  const [displayPaymentTypeRadioGroup, setDisplayPaymentTypeRadioGroup] = useState<boolean>(true);

  const onSubmit = (data) => {
    if (paymentTypeForm.formState.isValid) {
      actions.setPaymentTypeStepData({ paymentType: paymentTypeStepData.paymentType, medicalAidDetails: data });
      actions.goToNextStep();
    }
  };

  const handleBackClick = () => actions.goToPreviousStep();
  const handlePaymentTypeSelectionChange = (data) => setDisplayPaymentTypeRadioGroup(data === PaymentType.Cash);

  const paymentTypeRadioGroup = () => (
    <>
      <FormControl component="fieldset" onChange={(e) => handlePaymentTypeSelectionChange(e.target.value)}>
        <FormLabel component="legend">How will you be paying?</FormLabel>
        <Controller
          name="paymentType"
          control={paymentTypeForm.control}
          as={
            <RadioGroup aria-label="Payment type">
              <Box>
                <FormControlLabel
                  label="CASH"
                  labelPlacement="bottom"
                  value={PaymentType.Cash}
                  control={<Radio color="primary" />}
                />
                <FormControlLabel
                  label="MEDICAL AID"
                  labelPlacement="bottom"
                  value={PaymentType.MedicalAid}
                  control={<Radio color="primary" />}
                />
              </Box>
            </RadioGroup>
          }
        />
      </FormControl>
      <Box marginTop="30px">
        <StepperFormButtons handleBackClick={handleBackClick} />
      </Box>
    </>
  );
  return (
    <FormProvider {...paymentTypeForm}>
      <form noValidate autoComplete="off" onSubmit={paymentTypeForm.handleSubmit(onSubmit)}>
        {displayPaymentTypeRadioGroup ? (
          paymentTypeRadioGroup()
        ) : (
          <>
            <MedicalAidDetailsSection {...paymentTypeStepData.medicalAidDetails} />
            <Box display="flex" justifyContent="flex-end" marginTop="30px">
              <StepperFormButtons handleBackClick={handleBackClick} />
            </Box>
          </>
        )}
      </form>
    </FormProvider>
  );
};
