import React, { useState } from 'react';
import { ContactDetailsSection, useActionButtonStyles } from '@health-code-ui/shared/ui';
import { Box, FormControl, FormControlLabel, FormLabel, Radio, RadioGroup } from '@material-ui/core';
import { useCreateAppointment } from '../../../context';
import { Controller, FormProvider, useForm } from 'react-hook-form';
import { PersonResponsibleForAccountSection } from './form-section-overrides/person-responsible-for-account-section';
import { PersonalDetailsSection } from './form-section-overrides/personal-details-section';
import { PersonResponsibleForAccountStepModel } from '../../../models/person-responsible-for-account-step.model';
import { StepperFormButtons } from '../../stepper-form-buttons';

export const PersonResponsibleForAccountStep = () => {
  const { actions, state } = useCreateAppointment();
  const {
    personResponsibleForAccountStepData,
  }: { personResponsibleForAccountStepData: PersonResponsibleForAccountStepModel } = state;
  const personResponsibleForAccountForm = useForm<PersonResponsibleForAccountStepModel>({
    mode: 'onBlur',
    defaultValues: {
      ...personResponsibleForAccountStepData,
    },
  });

  const [displayPersonReponsibleForAccountRadioGroup, setDisplayPersonReponsibleForAccountRadioGroup] = useState<
    boolean
  >(true);

  const onSubmit = (data: PersonResponsibleForAccountStepModel) => {
    if (personResponsibleForAccountForm.formState.isValid) {
      actions.setPersonResponsibleForAccountStepData(data);
      actions.goToNextStep();
    }
  };

  const handleBackClick = () => actions.goToPreviousStep();
  const handlePatientIsResponsibleForAccount = (data) => setDisplayPersonReponsibleForAccountRadioGroup(data === 'yes');

  const personResponsibleForAccountRadioGroup = () => (
    <>
      <FormControl component="fieldset" onChange={(e) => handlePatientIsResponsibleForAccount(e.target.value)}>
        <FormLabel component="legend">I am the responsible person for the account:</FormLabel>
        <Controller
          name="patientIsResponsibleForAccount"
          control={personResponsibleForAccountForm.control}
          as={
            <RadioGroup aria-label="Person responsible for the account">
              <Box>
                <FormControlLabel labelPlacement="bottom" value="yes" control={<Radio color="primary" />} label="YES" />
                <FormControlLabel labelPlacement="bottom" value="no" control={<Radio color="primary" />} label="NO" />
              </Box>
            </RadioGroup>
          }
        />
      </FormControl>
      <Box marginTop="30px">
        <StepperFormButtons handleBackClick={handleBackClick} />
      </Box>
    </>
  );

  return (
    <FormProvider {...personResponsibleForAccountForm}>
      <form noValidate autoComplete="off" onSubmit={personResponsibleForAccountForm.handleSubmit(onSubmit)}>
        {displayPersonReponsibleForAccountRadioGroup ? (
          personResponsibleForAccountRadioGroup()
        ) : (
          <Box display="flex" justifyContent="space-between" width="81%">
            <Box>
              <PersonResponsibleForAccountSection {...personResponsibleForAccountStepData} />
            </Box>
            <Box>
              <PersonalDetailsSection {...personResponsibleForAccountStepData} />
            </Box>
            <Box>
              <ContactDetailsSection {...personResponsibleForAccountStepData} />
              <Box display="flex" justifyContent="space-between">
                <StepperFormButtons handleBackClick={handleBackClick} />
              </Box>
            </Box>
          </Box>
        )}
      </form>
    </FormProvider>
  );
};
