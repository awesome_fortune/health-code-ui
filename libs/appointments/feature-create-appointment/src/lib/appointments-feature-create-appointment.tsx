import React from 'react';
import { CreateAppointmentFeatureContainer } from './components/feature-container';
import { CreateAppointmentProvider } from './context';

export const AppointmentsFeatureCreateAppointment = () => {
  return (
    <CreateAppointmentProvider>
      <CreateAppointmentFeatureContainer />
    </CreateAppointmentProvider>
  );
};

export default AppointmentsFeatureCreateAppointment;
