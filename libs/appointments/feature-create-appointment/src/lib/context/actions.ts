import { PatientDetailsModel, PersonResponsibleForAccountSectionModel } from '@health-code-ui/shared/models';

export enum CreateAppointmentActionTypes {
  GoToNextStep = 'GoToNextStep',
  GoToPreviousStep = 'GoToPreviousStep',
  CreateAppointment = 'CreateAppointment',
  CreateAppointmentError = 'CreateAppointmentError',
  CreateAppointmentSuccess = 'CreateAppointmentSuccess',
  SetSelectDateStepData = 'SetSelectDateStepData',
  SetPatientDetailsStepData = 'SetPatientDetailsStepData',
  SetPersonResponsibleForAccountStepData = 'SetPersonResponsibleForAccountStepData',
  SetPaymentTypeStepData = 'SetPaymentTypeStepData',
  SetFamilyMembersStepData = 'SetFamilyMembersStepData',
  SetCreateAppointmentLoading = 'SetCreateAppointmentLoading',
}

export type CreateAppointmentActions =
  | { type: CreateAppointmentActionTypes.GoToNextStep }
  | { type: CreateAppointmentActionTypes.GoToPreviousStep }
  | { type: CreateAppointmentActionTypes.CreateAppointment; payload: any }
  | { type: CreateAppointmentActionTypes.CreateAppointmentError; payload: any }
  | { type: CreateAppointmentActionTypes.CreateAppointmentSuccess; payload: any }
  | { type: CreateAppointmentActionTypes.SetSelectDateStepData; payload: any }
  | { type: CreateAppointmentActionTypes.SetPatientDetailsStepData; payload: PatientDetailsModel }
  | {
      type: CreateAppointmentActionTypes.SetPersonResponsibleForAccountStepData;
      payload: PersonResponsibleForAccountSectionModel;
    }
  | { type: CreateAppointmentActionTypes.SetPaymentTypeStepData; payload: any }
  | { type: CreateAppointmentActionTypes.SetFamilyMembersStepData; payload: any }
  | { type: CreateAppointmentActionTypes.SetCreateAppointmentLoading; payload: boolean };
