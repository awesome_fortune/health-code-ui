import React, { createContext, useContext, useReducer } from 'react';
import {
  Gender,
  IdentityDocumentType,
  MaritalStatus,
  MedicalAidName,
  MedicalAidPlanType,
  PatientDetailsModel,
  PaymentType,
  SouthAfricanLanguages,
  Title,
} from '@health-code-ui/shared/models';
import { CreateAppointmentActions, CreateAppointmentActionTypes } from './actions';
import { PersonResponsibleForAccountStepModel } from '../models/person-responsible-for-account-step.model';
import { PaymentTypeStepModel } from '../models/payment-type-step.model';
import { FamilyMembersStepModel } from '../models/family-members-step.model';
import { SelectDateStepModel } from '../models/select-date-step.model';
import * as appointmentApiClient from '@health-code-ui/appointments/api-client';

export interface CreateAppointmentState {
  currentStep: number;
  selectDateStepData: SelectDateStepModel;
  patientDetailsStepData: PatientDetailsModel;
  personResponsibleForAccountStepData: PersonResponsibleForAccountStepModel;
  paymentTypeStepData: PaymentTypeStepModel;
  familyMembersStepData: FamilyMembersStepModel;
  isLoading: boolean;
  appointmentCreated: boolean;
}

const initialState: CreateAppointmentState = {
  currentStep: 0,
  isLoading: false,
  appointmentCreated: false,
  selectDateStepData: {
    currentDate: new Date().toISOString(),
  },
  patientDetailsStepData: {
    personalEmailAddress: null,
    personalCellphoneNumber: null,
    homeNumber: null,
    maritalStatus: MaritalStatus.Single,
    employer: null,
    occupation: null,
    homeLanguage: SouthAfricanLanguages.English,
    preferredLanguageOfCommunication: SouthAfricanLanguages.English,
    numberOfDependants: 0,
    title: Title.Mr,
    name: null,
    surname: null,
    dateOfBirth: new Date().toISOString(),
    identityDocumentType: IdentityDocumentType.RsaID,
    identityNumber: null,
    gender: Gender.Male,
  },
  personResponsibleForAccountStepData: {
    title: Title.Mr,
    name: null,
    surname: null,
    gender: Gender.Male,
    dateOfBirth: new Date().toISOString(),
    maritalStatus: MaritalStatus.Single,
    employer: null,
    occupation: null,
    homeLanguage: SouthAfricanLanguages.English,
    preferredLanguageOfCommunication: SouthAfricanLanguages.English,
    homeNumber: null,
    personalCellphoneNumber: null,
    personalEmailAddress: null,
    patientIsResponsibleForAccount: 'yes',
  },
  paymentTypeStepData: {
    paymentType: PaymentType.Cash,
    medicalAidDetails: {
      title: Title.Mr,
      name: null,
      surname: null,
      medicalAidName: MedicalAidName.Discovery,
      medicalAidPlanType: MedicalAidPlanType.EssientialPlan,
      medicalAidNumber: null,
      numberOfDependants: 0,
    },
  },
  familyMembersStepData: {
    personalEmailAddress: null,
    personalCellphoneNumber: null,
    homeNumber: null,
    maritalStatus: null,
    employer: null,
    occupation: null,
    homeLanguage: null,
    preferredLanguageOfCommunication: null,
    numberOfDependants: null,
    title: null,
    name: null,
    surname: null,
    dateOfBirth: null,
    identityDocumentType: null,
    identityNumber: null,
    gender: null,
  },
};

const CreateAppointmentContext = createContext<any>({ state: initialState, actions: null });

const useActions = (dispatch) => ({
  goToNextStep: () => dispatch({ type: CreateAppointmentActionTypes.GoToNextStep }),
  goToPreviousStep: () => dispatch({ type: CreateAppointmentActionTypes.GoToPreviousStep }),
  setSelectDateStepData: (data) =>
    dispatch({ type: CreateAppointmentActionTypes.SetSelectDateStepData, payload: data }),
  setPatientDetailsStepData: (data) =>
    dispatch({ type: CreateAppointmentActionTypes.SetPatientDetailsStepData, payload: data }),
  setPersonResponsibleForAccountStepData: (data) =>
    dispatch({ type: CreateAppointmentActionTypes.SetPersonResponsibleForAccountStepData, payload: data }),
  setPaymentTypeStepData: (data) =>
    dispatch({ type: CreateAppointmentActionTypes.SetPaymentTypeStepData, payload: data }),
  setFamilyMembersStepData: (data) =>
    dispatch({ type: CreateAppointmentActionTypes.SetFamilyMembersStepData, payload: data }),
  createAppointment: (data) => dispatch({ type: CreateAppointmentActionTypes.CreateAppointment, payload: data }),
});

const applySideEffectsMiddleware = (dispatch) => async (action: CreateAppointmentActions) => {
  switch (action.type) {
    case CreateAppointmentActionTypes.CreateAppointment:
      try {
        dispatch({ type: CreateAppointmentActionTypes.SetCreateAppointmentLoading, payload: true });

        const response = await appointmentApiClient.createAppointment(action.payload);

        dispatch({ type: CreateAppointmentActionTypes.CreateAppointmentSuccess, payload: response });
      } catch (error) {
        dispatch({ type: CreateAppointmentActionTypes.CreateAppointmentError, payload: error });
      }
      break;
    default:
      dispatch(action);
      break;
  }
};

const createAppointmentReducer = (state: CreateAppointmentState, action: CreateAppointmentActions) => {
  switch (action.type) {
    case CreateAppointmentActionTypes.GoToPreviousStep:
      return {
        ...state,
        currentStep: state.currentStep - 1,
      };
    case CreateAppointmentActionTypes.GoToNextStep:
      return {
        ...state,
        currentStep: state.currentStep + 1,
      };
    case CreateAppointmentActionTypes.SetSelectDateStepData:
      return {
        ...state,
        selectDateStepData: { currentDate: new Date(action.payload).toISOString() },
      };
    case CreateAppointmentActionTypes.SetPatientDetailsStepData:
      return {
        ...state,
        patientDetailsStepData: action.payload,
      };
    case CreateAppointmentActionTypes.SetPersonResponsibleForAccountStepData:
      return {
        ...state,
        personResponsibleForAccountStepData: action.payload,
      };
    case CreateAppointmentActionTypes.SetPaymentTypeStepData:
      return {
        ...state,
        paymentTypeStepData: action.payload,
      };
    case CreateAppointmentActionTypes.SetFamilyMembersStepData:
      return {
        ...state,
        familyMembersStepData: action.payload,
      };
    case CreateAppointmentActionTypes.SetCreateAppointmentLoading:
      return {
        ...state,
        isLoading: action.payload,
      };
    case CreateAppointmentActionTypes.CreateAppointmentSuccess:
      return {
        ...state,
        isLoading: false,
        appointmentCreated: true,
      };
    case CreateAppointmentActionTypes.CreateAppointmentError:
      return {
        ...state,
        isLoading: false,
        error: action.payload,
      };
    default:
      throw new Error(`Unsupported action type: ${action.type}`);
  }
};

export const useCreateAppointment = () => {
  const context = useContext(CreateAppointmentContext);

  if (!context) {
    throw new Error('useCreateAppointment must be used within a CreateAppointmentProvider');
  }

  return context;
};

export const CreateAppointmentProvider = (props) => {
  const [state, dispatch] = useReducer(createAppointmentReducer, initialState);
  const actions = useActions(applySideEffectsMiddleware(dispatch));

  return <CreateAppointmentContext.Provider value={{ state, actions }} {...props} />;
};
