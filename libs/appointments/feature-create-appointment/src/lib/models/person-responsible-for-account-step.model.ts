import { Gender, MaritalStatus, SouthAfricanLanguages, Title } from '@health-code-ui/shared/models';

export interface PersonResponsibleForAccountStepModel {
  title?: Title;
  name?: string;
  surname?: string;
  gender?: Gender;
  dateOfBirth?: string;
  maritalStatus?: MaritalStatus;
  employer?: string;
  occupation?: string;
  homeLanguage?: SouthAfricanLanguages;
  preferredLanguageOfCommunication?: SouthAfricanLanguages;
  homeNumber?: string;
  personalCellphoneNumber?: string;
  personalEmailAddress?: string;
  patientIsResponsibleForAccount?: string;
}
