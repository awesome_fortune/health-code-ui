import { MedicalAidSectionModel, PaymentType } from '@health-code-ui/shared/models';

export interface PaymentTypeStepModel {
  paymentType?: PaymentType;
  medicalAidDetails?: MedicalAidSectionModel;
}
