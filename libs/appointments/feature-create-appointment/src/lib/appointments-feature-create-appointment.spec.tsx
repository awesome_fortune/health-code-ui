import React from 'react';
import { render } from '@testing-library/react';

import AppointmentsFeatureCreateAppointment from './appointments-feature-create-appointment';

describe('AppointmentsFeatureCreateAppointment', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<AppointmentsFeatureCreateAppointment />);
    expect(baseElement).toBeTruthy();
  });
});
