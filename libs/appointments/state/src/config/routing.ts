export const appointmentsBasePath = '/home/appointments';
export const appointmentsRoutes = {
  createAppointment: {
    path: `${appointmentsBasePath}/create-appointment`,
  },
  manageAppointments: {
    path: `${appointmentsBasePath}/manage-appointments`,
  },
  rescheduleAppointment: {
    path: `${appointmentsBasePath}/reschedule-appointment`,
  },
};
