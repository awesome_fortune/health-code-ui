import React from 'react';
import { render } from '@testing-library/react';

import AppointmentsShellAppointmentsEndUser from './appointments-shell-appointments-end-user';

describe('AppointmentsShellAppointmentsEndUser', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<AppointmentsShellAppointmentsEndUser />);
    expect(baseElement).toBeTruthy();
  });
});
