import React, { lazy, Suspense } from 'react';
import { Route } from 'react-router-dom';
import { appointmentsRoutes } from '@health-code-ui/appointments/state';
import { ApplicationExecutionContext } from '@health-code-ui/shared/models';

const AppointmentsFeatureCreateAppointment = lazy(() =>
  import('libs/appointments/feature-create-appointment/src/lib/appointments-feature-create-appointment')
);
const AppointmentsFeatureRescheduleAppoinment = lazy(() =>
  import('libs/appointments/feature-reschedule-appoinment/src/lib/appointments-feature-reschedule-appoinment')
);
const AppointmentsFeatureManageAppointments = lazy(() =>
  import('libs/appointments/feature-manage-appointments/src/lib/appointments-feature-manage-appointments')
);

export const AppointmentsShellAppointmentsEndUser = () => {
  return (
    <Suspense fallback={<div>Loading...</div>}>
      <Route path={appointmentsRoutes.createAppointment.path} component={AppointmentsFeatureCreateAppointment} />
      <Route path={appointmentsRoutes.rescheduleAppointment.path} component={AppointmentsFeatureRescheduleAppoinment} />
      <Route
        path={appointmentsRoutes.manageAppointments.path}
        render={() => (
          <AppointmentsFeatureManageAppointments applicationContext={ApplicationExecutionContext.EndUser} />
        )}
      />
    </Suspense>
  );
};

export default AppointmentsShellAppointmentsEndUser;
