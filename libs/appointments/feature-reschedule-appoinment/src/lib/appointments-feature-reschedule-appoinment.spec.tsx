import React from 'react';
import { render } from '@testing-library/react';

import AppointmentsFeatureRescheduleAppoinment from './appointments-feature-reschedule-appoinment';

describe('AppointmentsFeatureRescheduleAppoinment', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<AppointmentsFeatureRescheduleAppoinment />);
    expect(baseElement).toBeTruthy();
  });
});
