import React, { createContext, useContext, useReducer } from 'react';
import { RescheduleAppointmentActions, RescheduleAppointmentActionTypes } from './actions';

const RescheduleAppointmentContext = createContext<any>({ state: {}, actions: null });

const useActions = (dispatch) => ({
  rescheduleAppointment: () => dispatch({ type: RescheduleAppointmentActionTypes.RescheduleAppointment }),
});

const applySideEffectsMiddleware = (dispatch) => async (action: RescheduleAppointmentActions) => {
  switch (action.type) {
    default:
      dispatch(action);
      break;
  }
};

const rescheduleAppointmentReducer = (state: any, action: RescheduleAppointmentActions) => {
  switch (action.type) {
    case RescheduleAppointmentActionTypes.RescheduleAppointment:
      return {
        ...state,
      };

    default:
      throw new Error(`Unsupported action type: ${action.type}`);
  }
};

export const useRescheduleAppointment = () => {
  const context = useContext(RescheduleAppointmentContext);

  if (!context) {
    throw new Error('useRescheduleAppointment must be used within a RescheduleAppointmentProvider');
  }

  return context;
};

export const RescheduleAppointmentProvider = (props) => {
  const [state, dispatch] = useReducer(rescheduleAppointmentReducer, {});
  const actions = useActions(applySideEffectsMiddleware(dispatch));

  return <RescheduleAppointmentContext.Provider value={{ state, actions }} {...props} />;
};
