export enum RescheduleAppointmentActionTypes {
  RescheduleAppointment = 'RescheduleAppointment',
}

export type RescheduleAppointmentActions = { type: RescheduleAppointmentActionTypes.RescheduleAppointment };
