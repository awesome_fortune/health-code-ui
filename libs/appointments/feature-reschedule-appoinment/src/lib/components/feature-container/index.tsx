import React, { useState } from 'react';
import { Box, Button, IconButton, Typography } from '@material-ui/core';
import { useStyles } from './styles';
import { quicklinksBasePath } from '@health-code-ui/quicklinks/state';
import {
  AppointmentsCalendar,
  AppointmentsCalendarProps,
  ConfirmDialog,
  ConfirmDialogProps,
  useActionButtonStyles,
} from '@health-code-ui/shared/ui';
import { useHistory } from 'react-router-dom';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import { TimeSlot, TimeSlotStatus } from '@health-code-ui/shared/models';
import { addHours } from 'date-fns';

export const RescheduleAppointmentFeatureContainer = () => {
  const classes = useStyles();
  const [openConfirmExitDialog, setOpenConfirmExitDialog] = useState<boolean>(false);
  const [openConfirmRescheduleAppointmentDialog, setOpenConfirmRescheduleAppointmentDialog] = useState<boolean>(false);
  const history = useHistory();
  const [currentDate, setCurrentDate] = useState(new Date());
  const timeSlots: TimeSlot[] = [];
  const buttonClasses = useActionButtonStyles();

  for (let i = 0; i < 6; i++) {
    timeSlots.push({
      dateTime: addHours(currentDate, i),
      name: `Available Slot ${i + 1}`,
      status: TimeSlotStatus.Available,
    });
  }

  const confirmExitDialogProps: ConfirmDialogProps = {
    open: openConfirmExitDialog,
    dialogTitle: 'Exit',
    dialogText: 'Would you like to go back to the home screen?',
    negativeActionText: 'No',
    positiveActionText: 'Yes',
    onClose: (result: boolean) => {
      if (result) {
        history.push(quicklinksBasePath);
      }

      setOpenConfirmExitDialog(false);
    },
  };

  const confirmRescheduleAppointmentProps: ConfirmDialogProps = {
    open: openConfirmRescheduleAppointmentDialog,
    dialogTitle: 'Confirm Reschedule?',
    dialogText:
      'Should you with to reschedule your appointment. The doctor will only see you at the time you have chosen.',
    negativeActionText: 'Cancel',
    positiveActionText: 'Accept',
    onClose: (result: boolean) => {
      if (result) {
        // Reschedule appointment
      }

      setOpenConfirmRescheduleAppointmentDialog(false);
    },
  };

  const appointmentCalendarProps: AppointmentsCalendarProps = {
    currentDate,
    setCurrentDate,
    timeSlots,
  };

  const handleBackButtonClick = () => setOpenConfirmExitDialog(true);

  return (
    <>
      <Box paddingLeft="80px" paddingRight="80px" paddingTop="60px" marginBottom="80px">
        <Box display="flex" marginBottom="20px">
          <IconButton className={classes.backIconButton} onClick={handleBackButtonClick}>
            <ChevronLeftIcon />
          </IconButton>
          <Typography variant="h4" className={classes.lightText}>
            Reschedule
          </Typography>
          <Typography variant="h4" className={classes.boldText}>
            Appointment
          </Typography>
        </Box>

        <AppointmentsCalendar {...appointmentCalendarProps} />

        <Box display="flex" justifyContent="flex-end" width="75%" marginTop="20px">
          <Button
            onClick={() => setOpenConfirmRescheduleAppointmentDialog(true)}
            color="primary"
            variant="contained"
            classes={{
              root: buttonClasses.root,
              containedPrimary: buttonClasses.containedPrimary,
              label: buttonClasses.label,
            }}
          >
            Next
          </Button>
        </Box>
      </Box>

      <ConfirmDialog {...confirmExitDialogProps} />
      <ConfirmDialog {...confirmRescheduleAppointmentProps} />
    </>
  );
};
