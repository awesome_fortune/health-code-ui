import { makeStyles } from '@material-ui/core';
import { cssVariables } from '@health-code-ui/shared/ui';

export const useStyles = makeStyles({
  backIconButton: {
    color: cssVariables.colors.tahitiGold,
  },
  lightText: {
    fontWeight: 300,
    marginRight: 8,
  },
  boldText: {
    color: cssVariables.colors.black,
    fontWeight: 400,
  },
});
