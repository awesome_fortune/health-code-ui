import React from 'react';
import { RescheduleAppointmentFeatureContainer } from './components/feature-container';
import { RescheduleAppointmentProvider } from './context';

export const AppointmentsFeatureRescheduleAppoinment = () => {
  return (
    <RescheduleAppointmentProvider>
      <RescheduleAppointmentFeatureContainer />
    </RescheduleAppointmentProvider>
  );
};

export default AppointmentsFeatureRescheduleAppoinment;
