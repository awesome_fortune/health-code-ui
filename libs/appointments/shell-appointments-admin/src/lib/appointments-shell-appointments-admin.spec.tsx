import React from 'react';
import { render } from '@testing-library/react';

import AppointmentsShellAppointmentsAdmin from './appointments-shell-appointments-admin';

describe('AppointmentsShellAppointmentsAdmin', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<AppointmentsShellAppointmentsAdmin />);
    expect(baseElement).toBeTruthy();
  });
});
