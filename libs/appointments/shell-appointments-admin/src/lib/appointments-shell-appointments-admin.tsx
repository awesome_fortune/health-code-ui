import React, { lazy, Suspense } from 'react';
import { Route } from 'react-router-dom';
import { appointmentsRoutes } from '@health-code-ui/appointments/state';
import { ApplicationExecutionContext } from '@health-code-ui/shared/models';

const AppointmentsFeatureCreateAppointment = lazy(() =>
  import('libs/appointments/feature-create-appointment/src/lib/appointments-feature-create-appointment')
);
const AppointmentsFeatureManageAppointments = lazy(() =>
  import('libs/appointments/feature-manage-appointments/src/lib/appointments-feature-manage-appointments')
);

export const AppointmentsShellAppointmentsAdmin = () => {
  return (
    <Suspense fallback={<div>Loading...</div>}>
      <Route path={appointmentsRoutes.createAppointment.path} component={AppointmentsFeatureCreateAppointment} />
      <Route
        path={appointmentsRoutes.manageAppointments.path}
        render={() => <AppointmentsFeatureManageAppointments applicationContext={ApplicationExecutionContext.Admin} />}
      />
    </Suspense>
  );
};

export default AppointmentsShellAppointmentsAdmin;
