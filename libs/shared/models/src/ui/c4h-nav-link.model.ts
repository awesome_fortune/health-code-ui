export interface C4hNavLink {
  name: string;
  href: string;
}
