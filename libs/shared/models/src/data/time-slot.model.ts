import { TimeSlotStatus } from '../enums/time-slot-status.enum';

export interface TimeSlot {
  name: string;
  dateTime: Date;
  status: TimeSlotStatus;
}
