export type PreferredMethodOfCommunication = 'sms' | 'email';

export interface RegisterNewAccountFormModel {
  name?: string;
  surname?: string;
  cellNumber?: string;
  email?: string;
  preferredMethodOfCommunication?: PreferredMethodOfCommunication;
  username?: string;
  password?: string;
  confirmPassword?: string;
}
