import { Gender } from '../../enums/gender.enum';
import { IdentityDocumentType } from '../../enums/identity-document-type.enum';
import { Title } from '../../enums/title.enum';

export interface PersonResponsibleForAccountSectionModel {
  title?: Title;
  name?: string;
  surname?: string;
  identityDocumentType?: IdentityDocumentType;
  identityNumber?: string;
  gender?: Gender;
}
