import { Gender } from '../../enums/gender.enum';
import { IdentityDocumentType } from '../../enums/identity-document-type.enum';
import { MaritalStatus } from '../../enums/marital-status.enum';
import { SouthAfricanLanguages } from '../../enums/south-african-languages.enum';
import { Title } from '../../enums/title.enum';

export interface PatientDetailsModel {
  personalEmailAddress?: string;
  personalCellphoneNumber?: string;
  homeNumber?: string;
  maritalStatus?: MaritalStatus;
  employer?: string;
  occupation?: string;
  homeLanguage?: SouthAfricanLanguages;
  preferredLanguageOfCommunication?: SouthAfricanLanguages;
  numberOfDependants?: number;
  title?: Title;
  name?: string;
  surname?: string;
  dateOfBirth?: string;
  identityDocumentType?: IdentityDocumentType;
  identityNumber?: string;
  gender?: Gender;
}
