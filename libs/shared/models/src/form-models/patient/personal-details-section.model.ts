import { MaritalStatus } from '../../enums/marital-status.enum';
import { SouthAfricanLanguages } from '../../enums/south-african-languages.enum';

export interface PersonalDetailsSectionModel {
  maritalStatus?: MaritalStatus;
  employer?: string;
  occupation?: string;
  homeLanguage?: SouthAfricanLanguages;
  preferredLanguageOfCommunication?: SouthAfricanLanguages;
  numberOfDependants?: number;
}
