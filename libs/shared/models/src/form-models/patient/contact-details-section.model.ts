export interface ContactDetailsSectionModel {
  personalEmailAddress?: string;
  personalCellphoneNumber?: string;
  homeNumber?: string;
}
