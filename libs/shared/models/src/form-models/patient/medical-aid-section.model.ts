import { MedicalAidName } from '../../enums/medical-aid-name.enum';
import { MedicalAidPlanType } from '../../enums/medical-aid-plan-type.enum';
import { Title } from '../../enums/title.enum';

export interface MedicalAidSectionModel {
  title?: Title;
  name?: string;
  surname?: string;
  medicalAidName?: MedicalAidName;
  medicalAidPlanType?: MedicalAidPlanType;
  medicalAidNumber?: string;
  numberOfDependants?: number;
}
