import { Gender } from '../../enums/gender.enum';
import { IdentityDocumentType } from '../../enums/identity-document-type.enum';
import { Title } from '../../enums/title.enum';

export interface IdentificationDetailsSectionModel {
  title?: Title;
  name?: string;
  surname?: string;
  dateOfBirth?: string;
  identityDocumentType?: IdentityDocumentType;
  identityNumber?: string;
  gender?: Gender;
}
