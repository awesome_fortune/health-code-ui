import { PatientDetailsModel } from './patient-details.model';
import { PersonResponsibleForAccountSectionModel } from './person-responsible-for-account-section.model';

export interface PatientFile {
  patientDetails?: PatientDetailsModel;
  personResponsibleForAccount: PersonResponsibleForAccountSectionModel;
}
