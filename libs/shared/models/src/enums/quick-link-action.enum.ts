export enum QuickLinkAction {
  AdminCreateAppointment = 'AdminCreateAppointment',
  AdminManageAppointments = 'AdminManageAppointments',
  AdminCreatePatientFile = 'AdminCreatePatientFile',
  PatientCreateAppoinetment = 'PatientCreateAppointment',
  PatientManageAppointments = 'PatientManageAppointments',
  PatientCreatePatientFile = 'PatientCreatePatientFile',
}
