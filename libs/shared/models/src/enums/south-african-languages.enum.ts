export enum SouthAfricanLanguages {
  English = 'English',
  Afrikaans = 'Afrikaans',
  Zulu = 'Zulu',
  Xhosa = 'Xhosa',
  Venda = 'Venda',
  SouthernSotho = 'Southern Sotho',
  Tswana = 'Tswana',
  NorthernSotho = 'Northern Sotho',
  Swati = 'Swati',
  Ndebele = 'Ndebele',
}
