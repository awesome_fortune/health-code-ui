export enum TimeSlotStatus {
  Available = 'Available',
  Filled = 'Filled',
}
