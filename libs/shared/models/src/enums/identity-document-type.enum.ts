export enum IdentityDocumentType {
  RsaID = 'RSA ID',
  Passport = 'Passport',
}
