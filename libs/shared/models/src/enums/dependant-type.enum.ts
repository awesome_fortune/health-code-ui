export enum DependantType {
  Child = 'Child',
  Spouse = 'Spouse',
  FamilyMember = 'Family Member',
}
