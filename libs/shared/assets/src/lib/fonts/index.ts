export const SourceSansProBlack = {
  fontFamily: 'SourceSansProBlack',
  src: `local('SourceSansProBlack'), url('libs/shared/assets/src/lib/fonts/SourceSansPro-Black.ttf') format('truetype')`,
};

export const SourceSansProBlackItalic = {
  fontFamily: 'SourceSansProBlackItalic',
  src: `local('SourceSansProBlackItalic'), url('libs/shared/assets/src/lib/fonts/SourceSansPro-BlackItalic.ttf') format('truetype')`,
};

export const SourceSansProBold = {
  fontFamily: 'SourceSansProBold',
  src: `local('SourceSansProBold'), url('libs/shared/assets/src/lib/fonts/SourceSansPro-Bold.ttf') format('truetype')`,
};

export const SourceSansProBoldItalic = {
  fontFamily: 'SourceSansProBoldItalic',
  src: `local('SourceSansProBoldItalic'), url('libs/shared/assets/src/lib/fonts/SourceSansPro-BoldItalic.ttf') format('truetype')`,
};

export const SourceSansProExtraLight = {
  fontFamily: 'SourceSansProExtraLight',
  src: `local('SourceSansProExtraLight'), url('libs/shared/assets/src/lib/fonts/SourceSansPro-ExtraLight.ttf') format('truetype')`,
};

export const SourceSansProExtraLightItalic = {
  fontFamily: 'SourceSansProExtraLightItalic',
  src: `local('SourceSansProExtraLightItalic'), url('libs/shared/assets/src/lib/fonts/SourceSansPro-ExtraLightItalic.ttf') format('truetype')`,
};

export const SourceSansProItalic = {
  fontFamily: 'SourceSansProItalic',
  src: `local('SourceSansProItalic'), url('libs/shared/assets/src/lib/fonts/SourceSansPro-Italic.ttf') format('truetype')`,
};

export const SourceSansProLight = {
  fontFamily: 'SourceSansProLight',
  src: `local('SourceSansProLight'), url('libs/shared/assets/src/lib/fonts/SourceSansPro-Light.ttf') format('truetype')`,
};

export const SourceSansProLightItalic = {
  fontFamily: 'SourceSansProLightItalic',
  src: `local('SourceSansProLightItalic'), url('libs/shared/assets/src/lib/fonts/SourceSansPro-LightItalic.ttf') format('truetype')`,
};

export const SourceSansProRegular = {
  fontFamily: 'SourceSansProRegular',
  src: `local('SourceSansProRegular'), url('libs/shared/assets/src/lib/fonts/SourceSansPro-Regular.ttf') format('truetype')`,
};

export const SourceSansProSemiBold = {
  fontFamily: 'SourceSansProSemiBold',
  src: `local('SourceSansProSemiBold'), url('libs/shared/assets/src/lib/fonts/SourceSansPro-SemiBold.ttf') format('truetype')`,
};

export const SourceSansProSemiBoldItalic = {
  fontFamily: 'SourceSansProSemiBoldItalic',
  src: `local('SourceSansProSemiBoldItalic'), url('libs/shared/assets/src/lib/fonts/SourceSansPro-SemiBoldItalic.ttf') format('truetype')`,
};
