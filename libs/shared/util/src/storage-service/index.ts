export enum StorageKeys {
  AuthState = 'auth_state',
}

export enum StorageType {
  LocalStorage,
  SessionStorage,
}

export class StorageService {
  static storageContainer(storageType: StorageType) {
    return storageType === StorageType.LocalStorage ? localStorage : sessionStorage;
  }

  static setItem(key, value, storageType: StorageType = StorageType.LocalStorage, stringify = true) {
    const saveValue = stringify ? JSON.stringify(value) : value;
    StorageService.storageContainer(storageType).setItem(key, saveValue);
  }

  static getItem(key, storageType: StorageType = StorageType.LocalStorage, parse = true) {
    try {
      const item = StorageService.storageContainer(storageType).getItem(key);
      let returnVal = null;
      if (item) {
        returnVal = parse ? JSON.parse(item) : item;
      }
      return returnVal;
    } catch (error) {
      return null;
    }
  }

  static clearStorage(storageType: StorageType = StorageType.LocalStorage) {
    StorageService.storageContainer(storageType).clear();
  }

  static deleteItem(key, storageType: StorageType = StorageType.LocalStorage) {
    StorageService.storageContainer(storageType).removeItem(key);
  }
}
