export enum HttpClientMethods {
  GET = 'GET',
  POST = 'POST',
  PUT = 'PUT',
  DELETE = 'DELETE',
  PATCH = 'PATCH',
}

export const httpClientBase = (endpoint, { body, token = null, ...customConfig } = null) => {
  const headers = { 'Content-Type': 'application/json' };

  if (token) {
    headers['Authorization'] = `Bearer ${token}`;
  }

  const config = {
    method: body ? HttpClientMethods.POST : HttpClientMethods.GET,
    ...customConfig,
    headers: {
      ...headers,
      ...customConfig.headers,
    },
  };

  if (body) {
    config.body = JSON.stringify(body);
  }

  return fetch(endpoint, config).then(async (response) => {
    try {
      let data = await response.text();
      data = data.length ? JSON.parse(data) : {};

      if (response.ok) {
        return data;
      } else {
        return Promise.reject(data);
      }
    } catch (e) {
      console.error(e);
    }
  });
};
