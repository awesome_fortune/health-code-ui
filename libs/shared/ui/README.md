# Shared UI component library.

[![Netlify Status](https://api.netlify.com/api/v1/badges/3290ec4e-317e-4ed2-a5ac-669f20dcbd32/deploy-status)](https://app.netlify.com/sites/health-code-ui/deploys)


## Running unit tests

Run `nx test shared-ui` to execute the unit tests via [Jest](https://jestjs.io).

## Running storybook

Run `yarn start:shared-ui:storybook` to run an instance of storybook on localhost.

## Building storybook

Run `yarn build:shared-ui:storybook` for a production build of storybook.
