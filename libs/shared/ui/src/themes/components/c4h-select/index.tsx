import React from 'react';
import Select, { SelectClassKey, SelectProps } from '@material-ui/core/Select';
import { Theme, withStyles, createStyles } from '@material-ui/core/styles';
import KeyboardArrowDown from '@material-ui/icons/KeyboardArrowDown';
import { cssVariables } from '../../cssVariables';

interface Styles extends Partial<Record<SelectClassKey, string>> {}

interface Props extends SelectProps {
  classes: Styles;
}

export const C4hSelect = withStyles((theme: Theme) =>
  createStyles({
    icon: {
      color: cssVariables.colors.tahitiGold,
    },
  })
)(({ classes, ...props }: Props) => {
  return (
    <Select classes={{ icon: classes.icon }} IconComponent={KeyboardArrowDown} {...props}>
      {props.children}
    </Select>
  );
});
