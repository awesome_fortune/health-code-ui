import { createStyles, Theme, withStyles } from '@material-ui/core/styles';
import Switch, { SwitchClassKey, SwitchProps } from '@material-ui/core/Switch';
import React from 'react';
import { cssVariables } from '../../cssVariables';

interface Styles extends Partial<Record<SwitchClassKey, string>> {
  focusVisible?: string;
}

interface Props extends SwitchProps {
  classes: Styles;
}

export const C4hSwitch = withStyles((theme: Theme) =>
  createStyles({
    root: {
      width: 68,
      height: 38,
      padding: 0,
      margin: theme.spacing(1),
    },
    switchBase: {
      padding: 1,
      '&$checked': {
        transform: 'translateX(34px)',
        color: cssVariables.colors.white,
        '& + $track': {
          backgroundColor: cssVariables.colors.tahitiGold,
          opacity: 1,
          border: 'none',
        },
      },
      '&$focusVisible $thumb': {
        backgroundColor: cssVariables.colors.tahitiGold,
        border: `6px solid ${cssVariables.colors.white}`,
      },
    },
    thumb: {
      width: 32,
      height: 32,
      boxShadow: `0px 3px 2px ${cssVariables.colors.blackAlpha2}`,
      backgroundColor: cssVariables.colors.white,
    },
    track: {
      borderRadius: 16,
      height: 18,
      width: 64,
      transform: 'translate(2px, 10px)',
      backgroundColor: cssVariables.colors.boulder,
      opacity: 1,
    },
    checked: {},
    focusVisible: {},
  })
)(({ classes, ...props }: Props) => {
  return (
    <Switch
      classes={{
        root: classes.root,
        switchBase: classes.switchBase,
        thumb: classes.thumb,
        track: classes.track,
        checked: classes.checked,
      }}
      {...props}
    />
  );
});
