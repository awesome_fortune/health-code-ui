import { makeStyles } from '@material-ui/core';
import { cssVariables } from '../cssVariables';

export const useActionButtonStyles = makeStyles({
  root: {
    borderRadius: 24,
    boxShadow: 'none',
    minWidth: 140,
    paddingTop: 10,
    paddingBottom: 10,
  },
  label: {
    fontSize: 16,
    letterSpacing: 0,
  },
  containedPrimary: {
    color: cssVariables.colors.white,
  },
});
