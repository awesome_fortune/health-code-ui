import { createMuiTheme } from '@material-ui/core/styles';
import { cssVariables } from './cssVariables';

// tslint:disable-next-line: nx-enforce-module-boundaries
import {
  SourceSansProBlack,
  SourceSansProBlackItalic,
  SourceSansProBold,
  SourceSansProBoldItalic,
  SourceSansProExtraLight,
  SourceSansProItalic,
  SourceSansProLight,
  SourceSansProExtraLightItalic,
  SourceSansProRegular,
  SourceSansProSemiBold,
  SourceSansProSemiBoldItalic,
  SourceSansProLightItalic,
} from '../../../assets/src';

export const theme = createMuiTheme({
  typography: {
    fontFamily: 'Source Sans Pro, Roboto, sans-serif',
  },
  palette: {
    primary: {
      main: cssVariables.colors.tahitiGold,
    },
  },
  overrides: {
    MuiCssBaseline: {
      '@global': {
        '@font-face': [
          SourceSansProBlack,
          SourceSansProBlackItalic,
          SourceSansProBold,
          SourceSansProBoldItalic,
          SourceSansProExtraLight,
          SourceSansProExtraLightItalic,
          SourceSansProItalic,
          SourceSansProLight,
          SourceSansProBoldItalic,
          SourceSansProRegular,
          SourceSansProSemiBold,
          SourceSansProSemiBoldItalic,
          SourceSansProLightItalic,
        ],
      },
    },
  },
});
