import React, { useState } from 'react';
import { muiTheme } from 'storybook-addon-material-ui';
import { theme } from '@health-code-ui/shared/ui';
import { AppointmentsCalendar, AppointmentsCalendarProps } from '@health-code-ui/shared/ui';
import centered from '@storybook/addon-centered/react';
import { addHours } from 'date-fns';
import { TimeSlot, TimeSlotStatus } from '@health-code-ui/shared/models';

export default {
  title: 'Appointments module',
  decorators: [muiTheme([theme]), centered],
};

export const calendar = () => {
  const [currentDate, setCurrentDate] = useState(new Date());
  const timeSlots: TimeSlot[] = [];

  for (let i = 0; i < 6; i++) {
    timeSlots.push({
      dateTime: addHours(currentDate, i),
      name: `Available Slot ${i + 1}`,
      status: TimeSlotStatus.Available,
    });
  }

  const props: AppointmentsCalendarProps = {
    currentDate,
    setCurrentDate,
    timeSlots,
  };
  return <AppointmentsCalendar {...props} />;
};
