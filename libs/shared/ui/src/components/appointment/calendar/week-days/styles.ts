import { cssVariables } from '../../../../themes/cssVariables';
import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
  typography: {
    textTransform: 'uppercase',
    fontFamily: 'Roboto',
    color: cssVariables.colors.silver2,
    fontSize: 11,
    fontWeight: 400,
    lineHeight: '15px',
    letterSpacing: 0,
    textAlign: 'right',
    width: 53,
    height: 53,
    boxSizing: 'border-box',
    paddingRight: 13,
  },
});
