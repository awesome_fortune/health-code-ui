import { Box, Typography } from '@material-ui/core';
import React from 'react';
import { useStyles } from './styles';

const WeekDays = () => {
  const classes = useStyles();

  const days = ['s', 'm', 't', 'w', 't', 'f', 's'];
  return (
    <Box width="100%" display="flex" justifyContent="space-between" padding="25px 0px 55px 0">
      {days.map((day, index) => (
        <Typography key={index} className={classes.typography} variant="body1">
          {day}
        </Typography>
      ))}
    </Box>
  );
};

export default WeekDays;
