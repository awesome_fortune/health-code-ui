import { cssVariables } from '../../../../themes/cssVariables';
import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
  title: {
    fontFamily: 'Roboto',
    fontSize: 14,
    fontWeight: 700,
    lineHeight: '19px',
    letterSpacing: 0,
    color: cssVariables.colors.silver2,
    marginBottom: '1em',
  },
});
