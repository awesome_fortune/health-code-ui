import { cssVariables } from '../../../../../themes/cssVariables';
import { makeStyles } from '@material-ui/core';
import { TimeSlotItemProps } from '.';

export const useStyles = makeStyles({
  container: (props: TimeSlotItemProps) => ({
    borderBottom: props.showBorder ? `1px solid ${cssVariables.colors.mercuryAlpha1}` : 'none',
  }),
  slotIndicator: {
    backgroundColor: cssVariables.colors.tahitiGold,
    width: 6,
    height: 6,
    boxShadow: `0px 0px 6px ${cssVariables.colors.torchRedAlpha1}`,
    borderRadius: 3,
    marginRight: 20,
  },
  slotName: {
    fontFamily: 'Roboto',
    fontSize: 14,
    fontWeight: 700,
    color: cssVariables.colors.mineShaft2,
    lineHeight: '19px',
    marginBottom: 5,
    marginTop: 16,
  },
  slotDate: {
    fontFamily: 'Roboto',
    fontSize: 12,
    lineHeight: '16px',
    color: cssVariables.colors.silver2,
    marginBottom: 20,
  },
  slotTime: {
    backgroundColor: cssVariables.colors.tahitiGold,
    boxShadow: `0px 0px 5px ${cssVariables.colors.hotPinkAlpha1}`,
    borderRadius: 3,
    color: cssVariables.colors.white,
    fontFamily: 'Roboto',
    fontSize: 12,
    lineHeight: '16px',
    padding: 6,
  },
});
