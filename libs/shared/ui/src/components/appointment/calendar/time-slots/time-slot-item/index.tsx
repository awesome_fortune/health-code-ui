import React from 'react';
import { Box, Typography } from '@material-ui/core';
import { format } from 'date-fns';
import { useStyles } from './styles';
import { TimeSlot } from '@health-code-ui/shared/models';

export interface TimeSlotItemProps {
  timeSlot: TimeSlot;
  showBorder: boolean;
}

const TimeSlotItem = (props: TimeSlotItemProps) => {
  const classes = useStyles(props);
  const { name, dateTime } = props.timeSlot;

  return (
    <Box
      display="flex"
      flexBasis="40%"
      justifyContent="space-between"
      alignItems="center"
      className={classes.container}
      marginRight="5em"
    >
      <Box display="flex" alignItems="center">
        <div className={classes.slotIndicator} />
        <Box>
          <Typography variant="body1" className={classes.slotName}>
            {name}
          </Typography>
          <Typography variant="body1" className={classes.slotDate}>
            {format(dateTime, 'MMMM dd, yyyy')}
          </Typography>
        </Box>
      </Box>
      <Typography variant="body1" className={classes.slotTime}>
        {format(dateTime, 'kk:mm')}
      </Typography>
    </Box>
  );
};

export default TimeSlotItem;
