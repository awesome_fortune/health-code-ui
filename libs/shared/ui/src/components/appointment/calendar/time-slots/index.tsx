import React from 'react';
import { Box, Typography } from '@material-ui/core';
import TimeSlotItem from './time-slot-item';
import { useStyles } from './styles';
import { TimeSlot } from '@health-code-ui/shared/models';

export interface TimeSlotsProps {
  timeSlots: TimeSlot[];
}

const TimeSlots = (props: TimeSlotsProps) => {
  const classes = useStyles();
  const { timeSlots } = props;

  return (
    <Box>
      <Typography className={classes.title} variant="body1">
        Available Time Slots
      </Typography>
      <Box display="flex" width="100%" flexWrap="wrap">
        {timeSlots.map((timeSlot, index) => (
          <TimeSlotItem key={timeSlot.name} timeSlot={timeSlot} showBorder={index !== timeSlots.length - 1} />
        ))}
      </Box>
    </Box>
  );
};

export default TimeSlots;
