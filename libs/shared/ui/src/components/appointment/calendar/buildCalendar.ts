import { addDays, endOfMonth, endOfWeek, isBefore, startOfMonth, startOfWeek, subDays } from 'date-fns';

const buildCalendar = (currentDate) => {
  const monthStart = startOfMonth(currentDate);
  const monthEnd = endOfMonth(monthStart);
  const startDay = startOfWeek(monthStart);
  const endDay = endOfWeek(monthEnd);
  const calendar = [];
  let day = subDays(startDay, 1);

  while (isBefore(day, endDay)) {
    calendar.push(
      Array(7)
        .fill(0)
        .map(() => {
          day = addDays(day, 1);
          return day;
        })
    );
  }

  return calendar;
};

export default buildCalendar;
