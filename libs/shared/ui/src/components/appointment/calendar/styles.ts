import { cssVariables } from '../../../themes/cssVariables';
import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
  container: {
    backgroundColor: cssVariables.colors.white,
    boxShadow: `0px 2px 20px ${cssVariables.colors.blackAlpha2}`,
  },
  title: {
    fontWeight: 300,
    color: cssVariables.colors.black,
    letterSpacing: 0.3,
  },
});
