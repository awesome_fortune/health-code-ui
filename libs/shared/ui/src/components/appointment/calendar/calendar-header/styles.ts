import { cssVariables } from '../../../../themes/cssVariables';
import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
  icon: {
    color: cssVariables.colors.white,
  },
  buttonRoot: {
    backgroundColor: cssVariables.colors.mineShaft2,
    borderRadius: 3,
    boxShadow: `0px 0px 5px ${cssVariables.colors.mineShaft2Alpha1}`,
    '&:hover': {
      backgroundColor: cssVariables.colors.mineShaft2Alpha1,
    },
  },
  monthText: {
    fontFamily: 'Roboto',
    fontWeight: 300,
    marginRight: 8,
  },
  yearText: {
    fontFamily: 'Roboto',
    fontWeight: 500,
  },
});
