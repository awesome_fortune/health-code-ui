import React from 'react';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import { addMonths, format, subMonths } from 'date-fns';
import { useStyles } from './styles';
import { Box, IconButton, Typography } from '@material-ui/core';
import { AppointmentsCalendarProps } from '..';

const CalendarHeader = (props: AppointmentsCalendarProps) => {
  const classes = useStyles();
  const { currentDate, setCurrentDate } = props;

  const nextMonth = () => setCurrentDate(addMonths(currentDate, 1));
  const previousMonth = () => setCurrentDate(subMonths(currentDate, 1));

  return (
    <Box display="flex" justifyContent="space-between" alignItems="center" paddingTop="16px">
      <IconButton className={classes.buttonRoot} aria-label="Go to previous month" onClick={previousMonth}>
        <ArrowBackIcon fontSize="small" className={classes.icon} />
      </IconButton>

      <Box display="flex">
        <Typography variant="h6" className={classes.monthText}>
          {format(currentDate, 'MMMM')}
        </Typography>
        <Typography variant="h6" className={classes.yearText}>
          {format(currentDate, 'yyyy')}
        </Typography>
      </Box>

      <IconButton className={classes.buttonRoot} aria-label="go to next month" onClick={nextMonth}>
        <ArrowForwardIcon fontSize="small" className={classes.icon} />
      </IconButton>
    </Box>
  );
};

export default CalendarHeader;
