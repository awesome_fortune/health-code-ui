import React from 'react';
import { Box, Typography } from '@material-ui/core';
import CalendarHeader from './calendar-header';
import WeekDays from './week-days';
import DayCells from './day-cells';
import TimeSlots from './time-slots';
import { useStyles } from './styles';
import { TimeSlot } from '@health-code-ui/shared/models';

export interface AppointmentsCalendarProps {
  currentDate: Date;
  setCurrentDate?;
  timeSlots?: TimeSlot[];
}

export const AppointmentsCalendar = (props: AppointmentsCalendarProps) => {
  const classes = useStyles();
  const { timeSlots } = props;

  return (
    <Box padding="10px 120px 67px 22px" width="915px" className={classes.container}>
      <Typography variant="h5" className={classes.title}>
        Select Date and Time
      </Typography>
      <CalendarHeader {...props} />
      <Box paddingLeft="30px" paddingRight="30px">
        <WeekDays />
        <DayCells {...props} />
      </Box>
      <TimeSlots timeSlots={timeSlots} />
    </Box>
  );
};
