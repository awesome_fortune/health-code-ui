import { cssVariables } from '../../../../themes/cssVariables';
import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
  dayNumber: {
    color: cssVariables.colors.mineShaft2,
    fontSize: 13,
    fontWeight: 400,
    fontFamily: 'Roboto',
    lineHeight: '18px',
  },
});
