import { isSameDay, isSameMonth } from 'date-fns';

const isSelected = (day, currentDate) => isSameDay(day, currentDate);
const isNotSameMonth = (day, currentDate) => !isSameMonth(day, currentDate);
const isToday = (day) => isSameDay(new Date(), day);

const dayStyles = (day, currentDate) => {
  if (isNotSameMonth(day, currentDate)) return 'not-same-month';
  if (isSelected(day, currentDate)) return 'selected';
  return '';
};

export default dayStyles;
