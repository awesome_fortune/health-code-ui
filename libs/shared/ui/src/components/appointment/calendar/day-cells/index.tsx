import React, { useEffect, useState } from 'react';
import { format } from 'date-fns';
import { Box, Typography } from '@material-ui/core';
import buildCalendar from '../buildCalendar';
import dayStyles from './dayStyles';
import './day-cells.css';
import { useStyles } from './styles';
import { AppointmentsCalendarProps } from '@health-code-ui/shared/ui';

const DayCells = (props: AppointmentsCalendarProps) => {
  const classes = useStyles();
  const { currentDate, setCurrentDate } = props;
  const [calendar, setCalendar] = useState([]);

  useEffect(() => {
    setCalendar(buildCalendar(currentDate));
  }, [currentDate]);

  return (
    <Box width="100%" display="flex" flexDirection="column">
      {calendar.map((week, index: number) => (
        <Box key={index} width="100%" display="flex" justifyContent="space-between">
          {week.map((day: Date | Date, index: number) => (
            <Box
              key={index}
              className={dayStyles(day, currentDate) + ' cursor-pointer text-center'}
              paddingTop="6px"
              paddingRight="10px"
              display="flex"
              flexDirection="column"
              boxSizing="border-box"
              width="53px"
              height="53px"
              onClick={() => setCurrentDate(day)}
            >
              <Typography variant="body1" className={classes.dayNumber}>
                {format(day, 'dd')}
              </Typography>
              {dayStyles(day, currentDate) === 'selected' && (
                <Typography variant="body1">{format(day, 'EEE')}</Typography>
              )}
            </Box>
          ))}
        </Box>
      ))}
    </Box>
  );
};

export default DayCells;
