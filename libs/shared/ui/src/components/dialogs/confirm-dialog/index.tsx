import React, { ReactNode } from 'react';
import { Button, createStyles, Dialog, IconButton, Theme, Typography, withStyles, WithStyles } from '@material-ui/core';
import { useStyles } from './styles';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogActions from '@material-ui/core/DialogActions';
import MuiDialogContent from '@material-ui/core/DialogContent';
import CloseIcon from '@material-ui/icons/Close';
import { useActionButtonStyles } from '../../../themes/styles/useActionButtonStyles';

export interface ConfirmDialogProps {
  dialogTitle: string;
  open: boolean;
  onClose: (result: boolean) => void;
  dialogText: string;
  positiveActionText: string;
  negativeActionText: string;
}

const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      padding: theme.spacing(2),
      display: 'flex',
    },
    dialogTitle: {
      fontWeight: 300,
      fontSize: 42,
      marginLeft: theme.spacing(2),
    },
    closeButton: {
      position: 'absolute',
      right: theme.spacing(1),
      top: theme.spacing(3),
    },
  });

export interface ConfirmDialogTitleProps extends WithStyles<typeof styles> {
  id: string;
  children: ReactNode;
  onClose: (result: boolean) => void;
}

const ConfirmDialogTitle = withStyles(styles)((props: ConfirmDialogTitleProps) => {
  const { children, classes, onClose, ...other } = props;

  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6" className={classes.dialogTitle}>
        {children}
      </Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={() => onClose(false)}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const ConfirmDialogContent = withStyles((theme: Theme) => ({
  root: {
    padding: theme.spacing(4),
  },
}))(MuiDialogContent);

const ConfirmDialogActions = withStyles((theme: Theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
}))(MuiDialogActions);

export const ConfirmDialog = (props: ConfirmDialogProps) => {
  const classes = useStyles();
  const buttonClasses = useActionButtonStyles();
  const { open, onClose, dialogTitle, dialogText, positiveActionText, negativeActionText } = props;

  const handleClose = (value: boolean) => onClose(value);

  return (
    <Dialog open={open} onClose={handleClose} classes={{ paper: classes.dialogPaper }}>
      <ConfirmDialogTitle id="confirmDialogTItle" onClose={() => handleClose(false)}>
        {dialogTitle}
      </ConfirmDialogTitle>
      <ConfirmDialogContent>
        <Typography variant="body1" className={classes.dialogText}>
          {dialogText}
        </Typography>
      </ConfirmDialogContent>
      <ConfirmDialogActions>
        <Button
          onClick={() => handleClose(false)}
          variant="outlined"
          classes={{
            root: buttonClasses.root,
            containedPrimary: buttonClasses.containedPrimary,
            label: buttonClasses.label,
          }}
        >
          {negativeActionText}
        </Button>
        <Button
          onClick={() => handleClose(true)}
          color="primary"
          variant="outlined"
          classes={{
            root: buttonClasses.root,
            containedPrimary: buttonClasses.containedPrimary,
            label: buttonClasses.label,
          }}
        >
          {positiveActionText}
        </Button>
      </ConfirmDialogActions>
    </Dialog>
  );
};
