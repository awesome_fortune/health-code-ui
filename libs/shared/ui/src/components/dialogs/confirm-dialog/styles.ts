import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
  dialogPaper: {
    borderRadius: 0,
  },
  dialogText: {
    fontWeight: 300,
    fontSize: 24,
  },
});
