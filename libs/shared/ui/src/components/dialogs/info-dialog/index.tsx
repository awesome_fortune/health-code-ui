import React from 'react';
import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@material-ui/core';

export interface InfoDialogProps {
  dialogTitle: string;
  open: boolean;
  onClose: (value: string) => void;
  dialogText: string;
  positiveActionText: string;
  negativeActionText: string;
}

export const InfoDialog = (props: InfoDialogProps) => {
  const { open, onClose, dialogTitle, dialogText, positiveActionText, negativeActionText } = props;

  const handleClose = (value: string) => {
    onClose(value);
  };

  return (
    <Dialog open={open} onClose={handleClose}>
      <DialogTitle>{dialogTitle}</DialogTitle>
      <DialogContent>{dialogText}</DialogContent>
      <DialogActions>
        <Button>{negativeActionText}</Button>
        <Button>{positiveActionText}</Button>
      </DialogActions>
    </Dialog>
  );
};
