import React from 'react';
import { TextField } from '@material-ui/core';
import { useFormContext } from 'react-hook-form';
import { FormSectionItemContainer } from '../components/form-section-item-container';
import { SectionItemHeader } from '../components/section-item-header';
import { useStyles } from './styles';
import { ContactDetailsSectionModel } from '@health-code-ui/shared/models';

const emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

export const ContactDetailsSection = (props: ContactDetailsSectionModel) => {
  const classes = useStyles();
  const { register, errors } = useFormContext<ContactDetailsSectionModel>();
  const { homeNumber, personalCellphoneNumber, personalEmailAddress } = props;

  return (
    <FormSectionItemContainer>
      <SectionItemHeader>Contact details</SectionItemHeader>
      <TextField
        inputRef={register({
          required: {
            value: true,
            message: 'Personal email address is required',
          },
          pattern: {
            value: emailRegex,
            message: 'Value does not match email format',
          },
        })}
        className={classes.textField}
        type="email"
        id="personalEmailAddress"
        name="personalEmailAddress"
        label="Personal email address"
        placeholder="johndoe@gmail.com"
        error={!!errors.personalEmailAddress}
        helperText={errors.personalEmailAddress ? errors.personalEmailAddress.message : null}
        defaultValue={personalEmailAddress}
      />
      <TextField
        inputRef={register({ required: { value: true, message: 'Personal cell number is required' } })}
        className={classes.textField}
        type="tel"
        id="personalCellphoneNumber"
        name="personalCellphoneNumber"
        label="Personal cell number"
        placeholder="012 345 6789"
        error={!!errors.personalCellphoneNumber}
        helperText={errors.personalCellphoneNumber ? errors.personalCellphoneNumber.message : null}
        defaultValue={personalCellphoneNumber}
      />
      <TextField
        inputRef={register}
        className={classes.textField}
        id="homeNumber"
        name="homeNumber"
        type="tel"
        label="Home number"
        placeholder="012 345 6789"
        defaultValue={homeNumber}
      />
    </FormSectionItemContainer>
  );
};
