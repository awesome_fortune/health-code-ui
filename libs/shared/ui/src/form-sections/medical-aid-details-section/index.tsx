import React from 'react';
import { FormSectionItemContainer } from '../components/form-section-item-container';
import { SectionItemHeader } from '../components/section-item-header';
import { C4hSelect } from '../../themes/components/c4h-select';
import { MedicalAidName, MedicalAidPlanType, MedicalAidSectionModel, Title } from '@health-code-ui/shared/models';
import { Box, FormControl, InputLabel, MenuItem, TextField } from '@material-ui/core';
import { Controller, useFormContext } from 'react-hook-form';
import { useStyles } from './styles';

export const MedicalAidDetailsSection = (props: MedicalAidSectionModel) => {
  const classes = useStyles();
  const { register, errors } = useFormContext<MedicalAidSectionModel>();
  const { title, name, surname, medicalAidNumber, medicalAidName, medicalAidPlanType, numberOfDependants } = props;
  return (
    <FormSectionItemContainer>
      <SectionItemHeader>Medical Aid Details</SectionItemHeader>
      <Box display="flex" flexWrap="wrap">
        <FormControl className={classes.formControl} style={{ paddingRight: 30 }}>
          <InputLabel id="titleLabel">Title</InputLabel>
          <Controller
            name="title"
            id="title"
            defaultValue={title}
            as={
              <C4hSelect labelId="titleLabel">
                {Object.values(Title).map((value, index) => (
                  <MenuItem key={index} value={value}>
                    {value}
                  </MenuItem>
                ))}
              </C4hSelect>
            }
          />
        </FormControl>

        <TextField
          className={classes.formControl}
          style={{ paddingRight: 30 }}
          defaultValue={name}
          label="Name"
          name="name"
          id="name"
          inputRef={register({
            required: {
              value: true,
              message: 'Name is required',
            },
          })}
          error={!!errors.name}
          helperText={errors.name ? errors.name.message : null}
        />

        <TextField
          className={classes.formControl}
          defaultValue={surname}
          label="Surname"
          name="surname"
          id="surname"
          inputRef={register({
            required: { value: true, message: 'Surname is required' },
          })}
          error={!!errors.surname}
          helperText={errors.surname ? errors.surname.message : null}
        />

        <FormControl className={classes.formControl} style={{ paddingRight: 30 }}>
          <InputLabel id="medicalAidNameLabel">Name of Medical aid</InputLabel>
          <Controller
            name="medicalAidName"
            id="medicalAidName"
            defaultValue={medicalAidName}
            as={
              <C4hSelect labelId="medicalAidNameLabel">
                {Object.values(MedicalAidName).map((value, index) => (
                  <MenuItem key={index} value={value}>
                    {value}
                  </MenuItem>
                ))}
              </C4hSelect>
            }
          />
        </FormControl>

        <FormControl className={classes.formControl} style={{ paddingRight: 30 }}>
          <InputLabel id="medicalAidPlanTypeLabel">Plan</InputLabel>
          <Controller
            name="medicalAidPlanType"
            id="medicalAidPlanType"
            defaultValue={medicalAidPlanType}
            as={
              <C4hSelect labelId="medicalAidPlanTypeLabel">
                {Object.values(MedicalAidPlanType).map((value, index) => (
                  <MenuItem key={index} value={value}>
                    {value}
                  </MenuItem>
                ))}
              </C4hSelect>
            }
          />
        </FormControl>

        <TextField
          className={classes.formControl}
          defaultValue={medicalAidNumber}
          type="text"
          label="Medical Aid Number"
          id="medicalAidNumber"
          name="medicalAidNumber"
          inputRef={register({
            required: {
              value: true,
              message: 'Medical aid number is required',
            },
          })}
          error={!!errors.medicalAidNumber}
          helperText={errors.medicalAidNumber ? errors.medicalAidNumber.message : null}
        />

        <TextField
          className={classes.formControl}
          style={{ paddingRight: 30 }}
          type="number"
          label="Number of Dependants"
          name="numberOfDependants"
          id="numberOfDependants"
          inputRef={register}
          defaultValue={numberOfDependants}
        />
      </Box>
    </FormSectionItemContainer>
  );
};
