import React from 'react';
import { FormSectionItemContainer } from '../components/form-section-item-container';
import {
  Gender,
  IdentityDocumentType,
  PersonResponsibleForAccountSectionModel,
  Title,
} from '@health-code-ui/shared/models';
import { C4hSelect } from '../../themes/components/c4h-select';
import { Box, FormControl, InputLabel, MenuItem, TextField } from '@material-ui/core';
import { useStyles } from './styles';
import { Controller, useFormContext } from 'react-hook-form';

export const PersonResponsibleForAccountSection = () => {
  const classes = useStyles();
  const { register, errors } = useFormContext<PersonResponsibleForAccountSectionModel>();

  return (
    <FormSectionItemContainer>
      <Box display="flex" flexWrap="wrap">
        <FormControl className={classes.formControl} style={{ paddingRight: 30 }}>
          <InputLabel id="titleLabel">Title</InputLabel>
          <Controller
            name="title"
            id="title"
            defaultValue={Title.Mr}
            as={
              <C4hSelect labelId="titleLabel">
                {Object.values(Title).map((value, index) => (
                  <MenuItem key={index} value={value}>
                    {value}
                  </MenuItem>
                ))}
              </C4hSelect>
            }
          />
        </FormControl>

        <TextField
          className={classes.formControl}
          style={{ paddingRight: 30 }}
          label="Name"
          name="name"
          id="name"
          inputRef={register({
            required: {
              value: true,
              message: 'Name is required',
            },
          })}
          error={!!errors.name}
          helperText={errors.name ? errors.name.message : null}
        />

        <TextField
          className={classes.formControl}
          label="Surname"
          name="surname"
          id="surname"
          inputRef={register({
            required: {
              value: true,
              message: 'Surname is required',
            },
          })}
          error={!!errors.surname}
          helperText={errors.surname ? errors.surname.message : null}
        />

        <FormControl className={classes.formControl} style={{ paddingRight: 30 }}>
          <InputLabel id="identityDocumentTypeLabel">Identity document type</InputLabel>
          <Controller
            name="identityDocumentType"
            id="identityDocumentType"
            defaultValue={IdentityDocumentType.RsaID}
            as={
              <C4hSelect labelId="identityDocumentTypeLabel">
                {Object.values(IdentityDocumentType).map((value, index) => (
                  <MenuItem key={index} value={value}>
                    {value}
                  </MenuItem>
                ))}
              </C4hSelect>
            }
          />
        </FormControl>

        <TextField
          className={classes.formControl}
          label="Identity number"
          style={{ paddingRight: 30 }}
          inputRef={register({
            required: {
              value: true,
              message: 'Identity number is required',
            },
          })}
          id="identityNumber"
          name="identityNumber"
          error={!!errors.identityNumber}
          helperText={errors.identityNumber ? errors.identityNumber.message : null}
        />

        <FormControl className={classes.formControl}>
          <InputLabel id="genderLabel">Gender</InputLabel>
          <Controller
            name="gender"
            id="gender"
            defaultValue={Gender.Male}
            as={
              <C4hSelect labelId="genderLabel">
                {Object.values(Gender).map((value, index) => (
                  <MenuItem key={index} value={value}>
                    {value}
                  </MenuItem>
                ))}
              </C4hSelect>
            }
          />
        </FormControl>
      </Box>
    </FormSectionItemContainer>
  );
};
