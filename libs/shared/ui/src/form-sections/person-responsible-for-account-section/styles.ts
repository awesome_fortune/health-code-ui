import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
  formControl: {
    minWidth: 120,
    marginTop: 35,
    flexBasis: '33%',
    boxSizing: 'border-box',
  },
});
