import React from 'react';
import { muiTheme } from 'storybook-addon-material-ui';
import { theme } from '@health-code-ui/shared/ui';
import centered from '@storybook/addon-centered/react';
import { ContactDetailsSection } from '@health-code-ui/shared/ui';
import { PersonalDetailsSection } from '@health-code-ui/shared/ui';
import { NotificationSettingsSection } from '@health-code-ui/shared/ui';
import { MedicalAidDetailsSection } from '@health-code-ui/shared/ui';
import { PersonResponsibleForAccountSection } from '@health-code-ui/shared/ui';
import { DependantSection } from '@health-code-ui/shared/ui';
import { UserCredentialsSection } from '@health-code-ui/shared/ui';

export default {
  title: 'Form Sections',
  decorators: [muiTheme([theme]), centered],
};

export const contactDetails = () => {
  return <ContactDetailsSection />;
};

export const personalDetails = () => {
  return <PersonalDetailsSection />;
};

export const identificationDetails = () => {
  return <NotificationSettingsSection />;
};

export const notificationSettings = () => {
  return <NotificationSettingsSection />;
};

export const medicalAidDetails = () => {
  return <MedicalAidDetailsSection />;
};

export const personResponsibleForAccount = () => {
  return <PersonResponsibleForAccountSection />;
};

export const dependant = () => {
  return <DependantSection />;
};

export const userCredentials = () => {
  return <UserCredentialsSection />;
};
