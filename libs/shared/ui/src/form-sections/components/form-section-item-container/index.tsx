import { Box } from '@material-ui/core';
import React, { FunctionComponent } from 'react';
import { useStyles } from './styles';

export const FormSectionItemContainer: FunctionComponent = ({ children }) => {
  const containerClasses = useStyles();

  return (
    <Box display="flex" flexDirection="column" className={containerClasses.root}>
      {children}
    </Box>
  );
};
