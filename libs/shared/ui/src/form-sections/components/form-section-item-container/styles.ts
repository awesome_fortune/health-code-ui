import { makeStyles } from '@material-ui/core';
import { cssVariables } from '../../../themes/cssVariables';

export const useStyles = makeStyles({
  root: {
    minWidth: 384,
    backgroundColor: cssVariables.colors.white,
    boxShadow: `0px 2px 20px ${cssVariables.colors.blackAlpha2}`,
    padding: '35px 35px 50px 35px',
  },
});
