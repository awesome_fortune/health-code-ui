import React, { FunctionComponent } from 'react';
import { Typography } from '@material-ui/core';
import { useStyles } from './styles';

export const SectionItemHeader: FunctionComponent = ({ children }) => {
  const typographyClasses = useStyles();

  return (
    <Typography className={typographyClasses.h5} variant="h5">
      {children}
    </Typography>
  );
};
