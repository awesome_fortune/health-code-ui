import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
  h5: {
    fontWeight: 300,
    letterSpacing: 0.3,
  },
});
