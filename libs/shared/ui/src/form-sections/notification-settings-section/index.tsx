import React from 'react';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { C4hSwitch } from '../../themes/components/c4h-switch';
import { useStyles } from './styles';

export const NotificationSettingsSection = () => {
  const classes = useStyles();

  return (
    <FormGroup>
      <FormControlLabel
        classes={{ label: classes.formControlLabel }}
        control={<C4hSwitch />}
        label="Email notifications for Appointments"
      />
      <FormControlLabel
        classes={{ label: classes.formControlLabel }}
        control={<C4hSwitch />}
        label="SMS notifications for new Appointments"
      />
      <FormControlLabel
        classes={{ label: classes.formControlLabel }}
        control={<C4hSwitch />}
        label="SMS notifications for new Appointments"
      />
      <FormControlLabel
        classes={{ label: classes.formControlLabel }}
        control={<C4hSwitch />}
        label="SMS notification for member updating details"
      />
      <FormControlLabel
        classes={{ label: classes.formControlLabel }}
        control={<C4hSwitch />}
        label="SMS notification for member updating details"
      />
      <FormControlLabel
        classes={{ label: classes.formControlLabel }}
        control={<C4hSwitch />}
        label="SMS notification for member updating details"
      />
    </FormGroup>
  );
};
