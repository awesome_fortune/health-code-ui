import React, { useState } from 'react';
import { FormSectionItemContainer } from '../components/form-section-item-container';
import { C4hSelect } from '../../themes/components/c4h-select';
import { Box, Button, FormControl, InputLabel, MenuItem, TextField, makeStyles } from '@material-ui/core';
import userImage from '../../../../assets/src/lib/images/user.jpeg';
import { cssVariables } from '../../themes/cssVariables';
import { SectionItemHeader } from '../components/section-item-header';
import { DependantType } from '@health-code-ui/shared/models';

const useStyles = makeStyles({
  formControl: {
    marginTop: 35,
  },
  buttonRoot: {
    borderRadius: 16,
    height: 32,
  },
  buttonLabel: {
    letterSpacing: 0,
  },
  avatar: {
    backgroundImage: `url(${userImage})`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
  },
  buttonOutlined: {
    color: cssVariables.colors.white,
    borderColor: cssVariables.colors.white,
  },
});

export const DependantSection = () => {
  const classes = useStyles();
  const [dependantType, setDependantType] = useState<DependantType>(DependantType.FamilyMember);

  const handleDependantTypeChange = () => {
    //  TODO: implement dependant type change
  };

  return (
    <FormSectionItemContainer>
      <Box
        className={classes.avatar}
        alignSelf="center"
        marginBottom={3}
        paddingBottom={2}
        width={333}
        height={333}
        display="flex"
        alignItems="flex-end"
        justifyContent="center"
      >
        <Button
          classes={{ root: classes.buttonRoot, label: classes.buttonLabel, outlined: classes.buttonOutlined }}
          variant="outlined"
          style={{ marginRight: 15 }}
        >
          Edit dependant
        </Button>
        <Button
          classes={{ root: classes.buttonRoot, label: classes.buttonLabel, outlined: classes.buttonOutlined }}
          variant="outlined"
        >
          Remove dependant
        </Button>
      </Box>

      <SectionItemHeader>Dependant Details</SectionItemHeader>

      <TextField label="Name" className={classes.formControl} />

      <FormControl className={classes.formControl}>
        <InputLabel id="dependant-type-label">Dependant Type</InputLabel>
        <C4hSelect labelId="dependant-type-label" value={dependantType} onChange={handleDependantTypeChange}>
          {Object.values(DependantType).map((value, index) => (
            <MenuItem key={index} value={value}>
              {value}
            </MenuItem>
          ))}
        </C4hSelect>
      </FormControl>
    </FormSectionItemContainer>
  );
};
