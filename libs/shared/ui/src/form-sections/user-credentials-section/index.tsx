import React from 'react';
import { FormSectionItemContainer } from '../components/form-section-item-container';
import { SectionItemHeader } from '../components/section-item-header';
import { Box, Button, TextField, makeStyles } from '@material-ui/core';
import userImage from '../../../../assets/src/lib/images/user.jpeg';
import { cssVariables } from '../../themes/cssVariables';

const useStyles = makeStyles({
  formControl: {
    marginTop: 35,
  },
  buttonOutlined: {
    color: cssVariables.colors.white,
    borderColor: cssVariables.colors.white,
  },
  buttonRoot: {
    borderRadius: 16,
    height: 32,
  },
  buttonLabel: {
    letterSpacing: 0,
  },
  avatar: {
    backgroundImage: `url(${userImage})`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
  },
});

export const UserCredentialsSection = () => {
  const classes = useStyles();

  return (
    <FormSectionItemContainer>
      <Box
        className={classes.avatar}
        alignSelf="center"
        width={333}
        height={333}
        display="flex"
        marginBottom={3}
        paddingBottom={2}
        alignItems="flex-end"
        justifyContent="center"
      >
        <Button
          classes={{ root: classes.buttonRoot, label: classes.buttonLabel, outlined: classes.buttonOutlined }}
          variant="outlined"
          style={{ marginRight: 15 }}
        >
          Change picture
        </Button>
        <Button
          classes={{ root: classes.buttonRoot, label: classes.buttonLabel, outlined: classes.buttonOutlined }}
          variant="outlined"
        >
          Remove picture
        </Button>
      </Box>

      <SectionItemHeader>Change password</SectionItemHeader>

      <TextField label="Password" placeholder="Should be 8 characters long" className={classes.formControl} />

      <TextField label="Confirm password" placeholder="Should be 8 characters long" className={classes.formControl} />

      <Button
        variant="outlined"
        color="primary"
        style={{ marginTop: 45, width: 150 }}
        classes={{ root: classes.buttonRoot, label: classes.buttonLabel }}
      >
        Update password
      </Button>
    </FormSectionItemContainer>
  );
};
