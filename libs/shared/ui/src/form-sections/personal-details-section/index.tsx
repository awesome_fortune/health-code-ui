import React from 'react';
import { MaritalStatus, PersonalDetailsSectionModel } from '@health-code-ui/shared/models';
import { FormSectionItemContainer } from '../components/form-section-item-container';
import { SectionItemHeader } from '../components/section-item-header';
import { C4hSelect } from '../../themes/components/c4h-select';
import { SouthAfricanLanguages } from '@health-code-ui/shared/models';
import { useStyles } from './styles';
import { Box, FormControl, InputLabel, MenuItem, TextField } from '@material-ui/core';
import { Controller, useFormContext } from 'react-hook-form';

export const PersonalDetailsSection = (props: PersonalDetailsSectionModel) => {
  const classes = useStyles();
  const { register } = useFormContext<PersonalDetailsSectionModel>();
  const {
    maritalStatus,
    employer,
    occupation,
    homeLanguage,
    preferredLanguageOfCommunication,
    numberOfDependants,
  } = props;

  return (
    <FormSectionItemContainer>
      <SectionItemHeader>Personal details</SectionItemHeader>
      <Box display="flex">
        <FormControl className={classes.formControl} style={{ flexGrow: 1, marginRight: 15 }}>
          <InputLabel id="maritalStatusLabel">Marital Status</InputLabel>
          <Controller
            name="maritalStatus"
            id="maritalStatus"
            defaultValue={maritalStatus}
            as={
              <C4hSelect labelId="maritalStatusLabel">
                {Object.values(MaritalStatus).map((value, index) => (
                  <MenuItem key={index} value={value}>
                    {value}
                  </MenuItem>
                ))}
              </C4hSelect>
            }
          />
        </FormControl>

        <TextField
          className={classes.formControl}
          type="text"
          label="Employer"
          style={{ flexGrow: 2 }}
          inputRef={register}
          id="employer"
          name="employer"
          defaultValue={employer}
        />
      </Box>

      <Box display="flex" flexDirection="column">
        <TextField
          className={classes.formControl}
          type="text"
          label="Occupation"
          inputRef={register}
          name="occupation"
          id="occupation"
          defaultValue={occupation}
        />

        <FormControl className={classes.formControl}>
          <InputLabel id="homeLanguageLabel">Home Language</InputLabel>
          <Controller
            name="homeLanguage"
            id="homeLanguage"
            defaultValue={homeLanguage}
            as={
              <C4hSelect labelId="homeLanguageLabel">
                {Object.values(SouthAfricanLanguages).map((value, index) => (
                  <MenuItem key={index} value={value}>
                    {value}
                  </MenuItem>
                ))}
              </C4hSelect>
            }
          />
        </FormControl>

        <FormControl className={classes.formControl}>
          <InputLabel id="preferredLanguageOfCommunicationLabel">Preferred Language of Communication</InputLabel>
          <Controller
            name="preferredLanguageOfCommunication"
            id="preferredLanguageOfCommunication"
            defaultValue={preferredLanguageOfCommunication}
            as={
              <C4hSelect>
                {Object.values(SouthAfricanLanguages).map((value, index) => (
                  <MenuItem key={index} value={value}>
                    {value}
                  </MenuItem>
                ))}
              </C4hSelect>
            }
          />
        </FormControl>

        <TextField
          className={classes.formControl}
          type="number"
          label="Number of Dependants"
          inputRef={register}
          name="numberOfDependants"
          id="numberOfDependants"
          defaultValue={numberOfDependants}
        />
      </Box>
    </FormSectionItemContainer>
  );
};
