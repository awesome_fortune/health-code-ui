import React from 'react';
import { FormSectionItemContainer } from '../components/form-section-item-container';
import { SectionItemHeader } from '../components/section-item-header';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import { C4hSelect } from '../../themes/components/c4h-select';
import 'date-fns';
import { Gender, Title, IdentityDocumentType, IdentificationDetailsSectionModel } from '@health-code-ui/shared/models';
import { useStyles } from './styles';
import { Box, FormControl, InputLabel, MenuItem, TextField } from '@material-ui/core';
import { Controller, useFormContext } from 'react-hook-form';

export const IdentificationDetailsSection = (props: IdentificationDetailsSectionModel) => {
  const classes = useStyles();
  const { register, errors, getValues } = useFormContext<IdentificationDetailsSectionModel>();
  const { title, name, surname, dateOfBirth, identityDocumentType, identityNumber, gender } = props;

  return (
    <FormSectionItemContainer>
      <SectionItemHeader>Identification details</SectionItemHeader>
      <Box display="flex">
        <FormControl className={classes.formControl} style={{ flexGrow: 1, marginRight: 15 }}>
          <InputLabel id="titleLabel">Title</InputLabel>
          <Controller
            name="title"
            id="title"
            defaultValue={title}
            as={
              <C4hSelect labelId="titleLabel">
                {Object.values(Title).map((value) => (
                  <MenuItem key={value} value={value}>
                    {value}
                  </MenuItem>
                ))}
              </C4hSelect>
            }
          />
        </FormControl>

        <TextField
          className={classes.formControl}
          label="Name"
          name="name"
          id="name"
          inputRef={register({
            required: {
              value: true,
              message: 'Name is required',
            },
          })}
          error={!!errors.name}
          helperText={errors.name ? errors.name.message : null}
          defaultValue={name}
        />
      </Box>

      <Box display="flex" flexDirection="column">
        <TextField
          className={classes.formControl}
          label="Surname"
          name="surname"
          id="surname"
          inputRef={register({
            required: {
              value: true,
              message: 'Surname is required',
            },
          })}
          error={!!errors.surname}
          helperText={errors.surname ? errors.surname.message : null}
          defaultValue={surname}
        />

        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <Controller
            name="dateOfBirth"
            id="dateOfBirth"
            defaultValue={dateOfBirth}
            rules={{
              required: {
                value: true,
                message: 'Date of birth is required',
              },
            }}
            as={
              <KeyboardDatePicker
                disableFuture
                label="Date of birth"
                className={classes.formControl}
                variant="inline"
                format="MM/dd/yyyy"
                placeholder=""
                margin="normal"
                value={getValues('dateOfBirth')}
                onChange={null}
                KeyboardButtonProps={{ 'aria-label': 'Date of birth' }}
                error={!!errors.dateOfBirth}
                helperText={errors.dateOfBirth ? errors.dateOfBirth.message : null}
              />
            }
          />
        </MuiPickersUtilsProvider>

        <FormControl className={classes.formControl}>
          <InputLabel id="identityDocumentTypeLabel">Identity document type</InputLabel>
          <Controller
            name="identityDocumentType"
            id="identityDocumentType"
            defaultValue={identityDocumentType}
            as={
              <C4hSelect labelId="identityDocumentTypeLabel">
                {Object.values(IdentityDocumentType).map((value, index) => (
                  <MenuItem key={index} value={value}>
                    {value}
                  </MenuItem>
                ))}
              </C4hSelect>
            }
          />
        </FormControl>

        <TextField
          className={classes.formControl}
          label="Identity number"
          name="identityNumber"
          id="identityNumber"
          inputRef={register({
            required: {
              value: true,
              message: 'Identity number is required',
            },
          })}
          error={!!errors.identityNumber}
          helperText={errors.identityNumber ? errors.identityNumber.message : null}
          defaultValue={identityNumber}
        />

        <FormControl className={classes.formControl}>
          <InputLabel id="genderLabel">Gender</InputLabel>
          <Controller
            name="gender"
            id="gender"
            defaultValue={gender}
            as={
              <C4hSelect labelId="genderLabel">
                {Object.values(Gender).map((value, index) => (
                  <MenuItem key={index} value={value}>
                    {value}
                  </MenuItem>
                ))}
              </C4hSelect>
            }
          />
        </FormControl>
      </Box>
    </FormSectionItemContainer>
  );
};
