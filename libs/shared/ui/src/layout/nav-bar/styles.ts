import { cssVariables } from '../../themes/cssVariables';
import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
  root: {
    backgroundColor: cssVariables.colors.wildSand,
    border: `solid 1px ${cssVariables.colors.alto2}`,
  },
  appName: {
    color: cssVariables.colors.mineShaftAlpha1,
    fontSize: 18,
    marginLeft: '1em',
    margin: 15,
  },
  logout: {
    fontSize: 16,
    marginLeft: 15,
    marginRight: 40,
    cursor: 'pointer',
  },
  avatar: {
    marginRight: 20,
  },
  userDisplayName: {
    marginRight: 50,
  },
  links: {
    boxSizing: 'border-box',
    fontSize: 16,
    color: cssVariables.colors.mineShaftAlpha1,
    padding: '15px 30px 18px 15px',
    '&:first-child': {
      fontWeight: 700,
      borderBottom: `1px solid ${cssVariables.colors.tahitiGold}`,
    },
    '&:hover': {
      fontWeight: 700,
    },
  },
});
