import React, { useState } from 'react';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { C4hNavLink } from '@health-code-ui/shared/models';
import { Link } from '@material-ui/core';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import Avatar from '@material-ui/core/Avatar';
import { useAuth } from '@health-code-ui/auth/state';
import { useStyles } from './styles';
import { ConfirmDialog, ConfirmDialogProps } from '../../components/dialogs/confirm-dialog';

export interface NavBarProps {
  links: C4hNavLink[];
  userDisplayName: string;
}

export const NavBar = (props: NavBarProps) => {
  const classes = useStyles();
  const { state, actions } = useAuth();
  const { links } = props;
  const [openConfirmDialog, setOpenConfirmDialog] = useState<boolean>(false);
  const confirmDialogProps: ConfirmDialogProps = {
    open: openConfirmDialog,
    dialogTitle: 'Logout',
    dialogText: 'Would you like to log out of the system?',
    negativeActionText: 'No',
    positiveActionText: 'Yes',
    onClose: (result: boolean) => {
      if (result) {
        actions.logout();
      }

      setOpenConfirmDialog(false);
    },
  };

  const logout = () => setOpenConfirmDialog(true);

  return (
    <>
      <Box display="flex" justifyContent="space-between" alignItems="center" className={classes.root}>
        <Box>
          <Typography variant="body1" className={classes.appName}>
            Code 4 Hire H-Code
          </Typography>
        </Box>

        {state.user && (
          <Box display="flex" alignItems="center">
            <Typography variant="body1">
              {links.map((link, index) => (
                <Link underline="none" key={index} className={classes.links} href={link.href}>
                  {link.name}
                </Link>
              ))}
            </Typography>

            <Box display="flex" alignItems="center">
              <Typography variant="body1" className={classes.logout}>
                <Link underline="none" onClick={logout}>
                  Logout
                </Link>
              </Typography>
              <Avatar className={classes.avatar}>
                <AccountCircleIcon />
              </Avatar>
              <Typography className={classes.userDisplayName} variant="body1">
                {state.user.firstName} {state.user.surname}
              </Typography>
            </Box>
          </Box>
        )}
      </Box>
      <ConfirmDialog {...confirmDialogProps} />
    </>
  );
};
