import React from 'react';
import { muiTheme } from 'storybook-addon-material-ui';
import { theme } from '@health-code-ui/shared/ui';
import { C4hNavLink } from '@health-code-ui/shared/models';
import { NavBar, NavBarProps } from '@health-code-ui/shared/ui';
import { Footer, FooterProps } from '@health-code-ui/shared/ui';

export default {
  title: 'Layout',
  decorators: [muiTheme([theme])],
};

export const navBar = () => {
  const props: NavBarProps = {
    userDisplayName: 'John Doe',
    links: [
      {
        href: '#',
        name: 'Home',
      },
      {
        href: '#',
        name: 'Menu 1',
      },
      {
        href: '#',
        name: 'Menu 2',
      },
      {
        href: '#',
        name: 'Meu 3',
      },
    ],
  };

  return <NavBar {...props} />;
};

export const footer = () => {
  const links: C4hNavLink[] = [
    {
      href: '#',
      name: 'Privacy Policy',
    },
    {
      href: '#',
      name: 'Cookie Policy',
    },
    {
      href: '#',
      name: 'Terms & conditions',
    },
    {
      href: '#',
      name: 'Legal',
    },
  ];

  const props: FooterProps = {
    links,
    copyright: `Copyright ${new Date().getFullYear().toString()} Code4Hire H-Code`,
  };

  return <Footer {...props} />;
};
