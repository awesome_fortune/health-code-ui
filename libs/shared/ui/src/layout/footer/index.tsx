import React, { FunctionComponent } from 'react';
import { C4hNavLink } from '@health-code-ui/shared/models';
import { Box, Link, Typography } from '@material-ui/core';
import { useStyles } from './styles';

export interface FooterProps {
  links: C4hNavLink[];
  copyright: string;
}

export const Footer: FunctionComponent<FooterProps> = ({ links, copyright, children }) => {
  const classes = useStyles();

  return (
    <Box
      className={classes.root}
      display="flex"
      height="72px"
      width="100%"
      justifyContent="space-between"
      padding="1em 1em 1em 2em"
      boxSizing="border-box"
    >
      <Box display="flex">
        <Typography variant="body1">
          {links.map((link, index) => (
            <Link underline="none" key={index} className={classes.links} href={link.href}>
              {link.name}
            </Link>
          ))}
        </Typography>
      </Box>
      <Box display="flex" className={classes.copyright}>
        <Typography variant="body1">{copyright}</Typography>
      </Box>
    </Box>
  );
};
