import { cssVariables } from '../../themes/cssVariables';
import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
  root: {
    boxSizing: 'border-box',
    backgroundColor: cssVariables.colors.wildSand,
    border: `solid 1px ${cssVariables.colors.alto2}`,
  },
  links: {
    fontSize: 18,
    color: cssVariables.colors.boulder,
    padding: '0 1em',
    borderRight: `solid 1px ${cssVariables.colors.boulder}`,
    '&:last-child': {
      borderRight: 'none',
    },
  },
  copyright: {
    fontSize: 18,
    color: cssVariables.colors.boulder,
  },
});
