import React from 'react';
import { muiTheme } from 'storybook-addon-material-ui';
import { text } from '@storybook/addon-knobs';
import { faCalendarCheck } from '@fortawesome/free-solid-svg-icons';
import centered from '@storybook/addon-centered/react';
import { theme, DashboardCardProps } from '@health-code-ui/shared/ui';
import { PatientDetailsCard } from '@health-code-ui/shared/ui';
import { DashboardCard } from '@health-code-ui/shared/ui';
import { QuickLinkAction } from '@health-code-ui/shared/models';

export default {
  title: 'Cards',
  decorators: [muiTheme([theme]), centered],
};

export const dashboard = () => {
  const action = QuickLinkAction.AdminCreateAppointment;
  const onCardClick = (data: QuickLinkAction) => alert(data);

  const props: DashboardCardProps = {
    title: text('Card title', 'Manage Appointments'),
    icon: faCalendarCheck,
    action,
    onCardClick,
  };

  return <DashboardCard {...props} />;
};

export const patientDetails = () => {
  return <PatientDetailsCard />;
};
