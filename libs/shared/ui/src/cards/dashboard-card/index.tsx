import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { useStyles } from './styles';
import { Box, Typography } from '@material-ui/core';
import { cssVariables } from '../../themes/cssVariables';

export interface DashboardCardProps {
  title: string;
  icon: IconDefinition;
  onCardClick(): void;
  disabled: boolean;
}

const styleTitle = (title) => {
  const titleParts = title.split(' ');

  title = (
    <>
      {titleParts[0]}{' '}
      <span>{titleParts.length > 2 ? titleParts.splice(1, titleParts.length - 1).join(' ') : titleParts[1]}</span>
    </>
  );

  return title;
};

export const DashboardCard = (props: DashboardCardProps) => {
  const { title, icon, onCardClick, disabled } = props;

  const classes = useStyles({ disabled });

  const handleCardClick = () => {
    onCardClick();
  };

  return (
    <Box
      className={classes.container}
      display="flex"
      flexDirection="column"
      alignItems="center"
      onClick={handleCardClick}
    >
      <FontAwesomeIcon icon={icon} />
      <Typography className={classes.h5} variant={'h5'}>
        {title.split(' ').length > 1 ? styleTitle(title) : title}
      </Typography>
    </Box>
  );
};
