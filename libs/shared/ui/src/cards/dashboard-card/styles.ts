import { makeStyles } from '@material-ui/core';
import { cssVariables } from '../../themes/cssVariables';

export const useStyles = makeStyles({
  container: {
    boxShadow: `0px 2px 20px ${cssVariables.colors.blackAlpha2}`,
    borderRadius: 25,
    background: cssVariables.colors.white,
    width: 222,
    paddingTop: 40,
    paddingBottom: 40,
    cursor: (props: any) => (props.disabled ? 'not-allowed' : 'pointer'),
    '& svg': {
      fontSize: 70,
      color: (props: any) => (props.disabled ? cssVariables.colors.boulder : cssVariables.colors.tahitiGold),
    },
  },
  h5: {
    letterSpacing: 0.3,
    color: cssVariables.colors.black,
    marginTop: 13,
    fontWeight: 300,
    fontSize: 19,
    '& span': {
      fontWeight: 700,
    },
  },
});
