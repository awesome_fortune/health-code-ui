import React from 'react';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import { useStyles } from './styles';
import { Box, Button, Typography } from '@material-ui/core';
import { format, parseISO } from 'date-fns';

export interface PatientDetailsCardPrps {
  patient: any;
}

export const PatientDetailsCard = (props: PatientDetailsCardPrps) => {
  const classes = useStyles();
  const { patient } = props;

  return (
    <Box className={classes.container}>
      <div className={classes.patientDetailsContainer}>
        <Typography className={classes.h3} variant="h5">
          {patient.name} {patient.surname}
        </Typography>
        <Typography className={classes.dateAdded} variant="body1">
          Date Added to system
        </Typography>
        <Typography className={classes.requestedAdvice} variant="body1">
          {format(parseISO(patient.createDate), 'd MMMM y')}
        </Typography>
      </div>

      <Button
        classes={{
          root: classes.button,
          label: classes.buttonLabel,
        }}
        fullWidth={true}
        variant="text"
        endIcon={<KeyboardArrowRight style={{ fontSize: 30 }} />}
      >
        View details
      </Button>
    </Box>
  );
};
