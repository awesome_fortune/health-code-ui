import { cssVariables } from '../../themes/cssVariables';
import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
  container: {
    boxShadow: `0px 2px 15px ${cssVariables.colors.blackAlpha2}`,
    width: 283,
    backgroundColor: cssVariables.colors.white,
  },
  patientDetailsContainer: {
    paddingLeft: 25,
    paddingTop: 25,
    paddingBottom: '2rem',
  },
  button: {
    borderTop: `1px dashed ${cssVariables.colors.silverAlpha1}`,
    display: 'flex',
    justifyContent: 'space-between',
    padding: '10px 15px 25px 15px',
  },
  buttonLabel: {
    fontSize: 20,
    textTransform: 'none',
  },
  h3: {
    fontWeight: 300,
    letterSpacing: 0.3,
  },
  dateAdded: {
    fontWeight: 700,
    color: cssVariables.colors.poloBlue,
    fontSize: 14,
    letterSpacing: 0.3,
  },
  requestedAdvice: {
    letterSpacing: 0.3,
    color: cssVariables.colors.boulder,
  },
});
