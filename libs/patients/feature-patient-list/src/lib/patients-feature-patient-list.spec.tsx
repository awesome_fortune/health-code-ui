import React from 'react';
import { render } from '@testing-library/react';

import PatientsFeaturePatientList from './patients-feature-patient-list';

describe('PatientsFeaturePatientList', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<PatientsFeaturePatientList />);
    expect(baseElement).toBeTruthy();
  });
});
