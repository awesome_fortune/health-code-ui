import { cssVariables } from '@health-code-ui/shared/ui';
import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
  tableIcon: {
    color: (props: any) => props.tableIconColor,
  },
  gridIcon: {
    color: (props: any) => props.gridIconColor,
  },
});
