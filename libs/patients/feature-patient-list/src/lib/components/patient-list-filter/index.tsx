import React, { useState } from 'react';
import AppsIcon from '@material-ui/icons/Apps';
import TocIcon from '@material-ui/icons/Toc';
import { Box, Button, Chip, IconButton } from '@material-ui/core';
import { useStyles } from './styles';
import { cssVariables, useActionButtonStyles } from '@health-code-ui/shared/ui';
import { usePatientList } from '../../context';

export enum PatientListFilterDisplayMode {
  Table = 'Table',
  Grid = 'Grid',
}

export interface PatientListFilterProps {
  setPatientListDisplayMode(mode: PatientListFilterDisplayMode): void;
}

export const PatientListFilter = (props: PatientListFilterProps) => {
  const { state } = usePatientList();
  const { isLoading } = state;
  const [displayMode, setDisplayMode] = useState<PatientListFilterDisplayMode>(PatientListFilterDisplayMode.Grid);
  const buttonClasses = useActionButtonStyles();

  const classes = useStyles({
    tableIconColor:
      displayMode === PatientListFilterDisplayMode.Table ? cssVariables.colors.black : cssVariables.colors.dustyGray,
    gridIconColor:
      displayMode === PatientListFilterDisplayMode.Grid ? cssVariables.colors.black : cssVariables.colors.dustyGray,
  });

  const handleGridIconClick = () => {
    props.setPatientListDisplayMode(PatientListFilterDisplayMode.Grid);
    setDisplayMode(PatientListFilterDisplayMode.Grid);
  };

  const handleTableIconClick = () => {
    props.setPatientListDisplayMode(PatientListFilterDisplayMode.Table);
    setDisplayMode(PatientListFilterDisplayMode.Table);
  };

  return (
    <>
      <Box display="flex" justifyContent="space-between" marginTop="1em">
        <Box display="flex" justifyContent="space-between" flexBasis="35%">
          <Chip label="All Active" variant="outlined" />
          <Chip label="Med-Sa" variant="outlined" disabled />
          <Chip label="Discovery" variant="outlined" disabled />
          <Chip label="Momentum" variant="outlined" disabled />
          <Chip label="Bonitas" variant="outlined" disabled />
          <Chip label="Med AID" variant="outlined" disabled />
        </Box>
        <Box display="flex" justifyContent="space-between" flexBasis="20%">
          <Button
            variant="outlined"
            classes={{
              root: buttonClasses.root,
              containedPrimary: buttonClasses.containedPrimary,
              label: buttonClasses.label,
            }}
          >
            Search
          </Button>
          <Button
            color="primary"
            variant="contained"
            classes={{
              root: buttonClasses.root,
              containedPrimary: buttonClasses.containedPrimary,
              label: buttonClasses.label,
            }}
          >
            See all
          </Button>
        </Box>
      </Box>
      <Box>
        <IconButton onClick={handleGridIconClick} disabled={isLoading}>
          <AppsIcon className={classes.gridIcon} />
        </IconButton>
        <IconButton onClick={handleTableIconClick} disabled={isLoading}>
          <TocIcon className={classes.tableIcon} />
        </IconButton>
      </Box>
    </>
  );
};
