import React from 'react';
import { createStyles, Fab, TableCell, TableRow, Typography, withStyles } from '@material-ui/core';
import { cssVariables } from '@health-code-ui/shared/ui';
import { useStyles } from './styles';
import { format, parseISO } from 'date-fns';

export interface PatientListTableRowProps {
  patient: any;
}

const StyledTableRow = withStyles(() =>
  createStyles({
    root: {
      backgroundColor: cssVariables.colors.white,
      boxShadow: `0px 2px 20px ${cssVariables.colors.blackAlpha2}`,
      borderRadius: '26px',
    },
  })
)(TableRow);

const StyledTableCell = withStyles(() =>
  createStyles({
    root: {
      borderBottom: 0,
    },
  })
)(TableCell);

export const PatientListTableRow = (props: PatientListTableRowProps) => {
  const classes = useStyles();
  const { patient } = props;

  return (
    <StyledTableRow>
      <StyledTableCell style={{ borderTopLeftRadius: 7, borderBottomLeftRadius: 7 }}>
        <Fab classes={{ root: classes.viewPatientButtonRoot, label: classes.viewPatientButtonLabel }} color="primary">
          View
        </Fab>
      </StyledTableCell>
      <StyledTableCell>
        <Typography>
          {patient.name} {patient.surname}
        </Typography>
      </StyledTableCell>
      <StyledTableCell>
        <Typography>{format(parseISO(patient.createDate), 'd MMMM y')}</Typography>
      </StyledTableCell>
      <StyledTableCell style={{ borderTopRightRadius: 7, borderBottomRightRadius: 7 }}>
        <Typography>1 June 2020</Typography>
      </StyledTableCell>
    </StyledTableRow>
  );
};
