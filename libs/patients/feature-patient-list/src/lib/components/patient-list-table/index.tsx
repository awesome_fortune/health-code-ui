import React from 'react';
import { Box, Table, TableBody, TableContainer } from '@material-ui/core';
import { PatientListTableHead } from './patient-list-table-head';
import { PatientListTableRow } from './patient-list-table-row';
import { usePatientList } from '../../context';
import { useStyles } from './styles';

export const PatientListTable = () => {
  const { state } = usePatientList();
  const { patients } = state;
  const classes = useStyles();

  return (
    <TableContainer component={Box}>
      <Table aria-label="Patient list table" className={classes.table}>
        <PatientListTableHead />
        <TableBody>
          {patients.map((patient) => (
            <PatientListTableRow key={patient.id} patient={patient} />
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};
