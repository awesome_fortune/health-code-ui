import React from 'react';
import { createStyles, TableCell, TableHead, TableRow, withStyles } from '@material-ui/core';

const StyledTableCell = withStyles(() =>
  createStyles({
    root: {
      borderBottom: 0,
    },
  })
)(TableCell);

export const PatientListTableHead = () => {
  return (
    <TableHead>
      <TableRow>
        <StyledTableCell></StyledTableCell>
        <StyledTableCell>Name and Surname</StyledTableCell>
        <StyledTableCell>Date Added to System</StyledTableCell>
        <StyledTableCell>Last Visit</StyledTableCell>
      </TableRow>
    </TableHead>
  );
};
