import React from 'react';
import { PatientDetailsCard } from '@health-code-ui/shared/ui';
import { Box } from '@material-ui/core';
import { usePatientList } from '../../context';

export const PatientListGrid = () => {
  const { state } = usePatientList();
  const { isLoading, patients } = state;

  return (
    <Box display="flex" flexWrap="wrap">
      {!isLoading &&
        patients &&
        patients.map((patient) => (
          <Box padding="1em" key={patient.id}>
            <PatientDetailsCard patient={patient} />
          </Box>
        ))}
    </Box>
  );
};
