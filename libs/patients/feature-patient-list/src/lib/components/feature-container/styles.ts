import { makeStyles } from '@material-ui/core';
import bgImage from '../../../../../../shared/assets/src/lib/images/abstract_bg.jpg';
import { cssVariables } from '@health-code-ui/shared/ui';

export const useStyles = makeStyles({
  heroSection: {
    backgroundImage: `url(${bgImage})`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
  },
  heroTitle: {
    color: cssVariables.colors.white,
    fontWeight: 300,
  },
  heroSubTitle: {
    color: cssVariables.colors.white,
    fontWeight: 300,
  },
  iconButton: {
    color: cssVariables.colors.white,
  },
  iconButtonText: {
    color: cssVariables.colors.white,
    fontWeight: 400,
  },
  title: {
    fontWeight: 300,
    color: cssVariables.colors.black,
  },
  newPatientButton: {
    position: 'absolute',
    top: 260,
    left: 80,
    backgroundColor: cssVariables.colors.white,
    padding: '1em',
  },
  newPatientButtonText: {
    fontWeight: 300,
    color: cssVariables.colors.black,
    marginRight: '0.6em',
  },
  personAddIcon: {
    color: cssVariables.colors.acapulco,
  },
  personAddIconContainer: {
    border: `3px solid ${cssVariables.colors.acapulco}`,
    borderRadius: '50%',
    padding: '0.6em',
    marginRight: '0.6em',
  },
});
