import React, { useEffect, useState } from 'react';
import { Box, ButtonBase, IconButton, Typography } from '@material-ui/core';
import { usePatientList } from '../../context';
import WallpaperIcon from '@material-ui/icons/Wallpaper';
import { useStyles } from './styles';
import { PatientListFilter, PatientListFilterDisplayMode } from '../patient-list-filter';
import { PatientListGrid } from '../patient-list-grid';
import { PatientListTable } from '../patient-list-table';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import { ConfirmDialog, ConfirmDialogProps } from '@health-code-ui/shared/ui';
import { useHistory } from 'react-router-dom';
import { patientsRoutes } from '@health-code-ui/patients/state';

export const PatientListFeatureContainer = () => {
  const classes = useStyles();
  const history = useHistory();
  const { actions } = usePatientList();
  const [patientListDisplayMode, setPatientListDisplayMode] = useState<PatientListFilterDisplayMode>(
    PatientListFilterDisplayMode.Grid
  );
  const [openAddNewPatientConfirmDialog, setOpenAddNewPatientConfirmDialog] = useState<boolean>(false);

  const confirmOpenAddNewPatientDialogProps: ConfirmDialogProps = {
    open: openAddNewPatientConfirmDialog,
    dialogTitle: 'Add new patient',
    dialogText: 'Would you like to add a new patient?',
    negativeActionText: 'No',
    positiveActionText: 'Yes',
    onClose: (result: boolean) => {
      if (result) {
        history.push(patientsRoutes.createPatientFile.path);
      }

      setOpenAddNewPatientConfirmDialog(false);
    },
  };

  useEffect(() => {
    actions.getAllPatients();
  }, []);

  return (
    <Box position="relative">
      <Box
        className={classes.heroSection}
        paddingLeft="80px"
        paddingRight="80px"
        height="300px"
        display="flex"
        alignItems="center"
        justifyContent="space-between"
      >
        <Box>
          <Typography className={classes.heroTitle} variant="h4">
            Hi Dr John Doe
          </Typography>
          <Typography className={classes.heroSubTitle} variant="h6">
            Here are the patients our records
          </Typography>
        </Box>
        <Box display="flex" alignItems="center">
          <IconButton className={classes.iconButton} aria-label="Change backround">
            <WallpaperIcon />
          </IconButton>
          <Typography className={classes.iconButtonText} variant="body1">
            Change wallpaper
          </Typography>
        </Box>
      </Box>

      <ButtonBase className={classes.newPatientButton} onClick={() => setOpenAddNewPatientConfirmDialog(true)}>
        <Box className={classes.personAddIconContainer}>
          <PersonAddIcon className={classes.personAddIcon} />
        </Box>
        <Typography variant="h5" className={classes.newPatientButtonText}>
          Add New Patient
        </Typography>
        <ChevronRightIcon />
      </ButtonBase>

      <Box paddingLeft="80px" paddingRight="80px" paddingTop="80px">
        <Typography className={classes.title} variant="h5">
          Patients
        </Typography>

        <PatientListFilter setPatientListDisplayMode={setPatientListDisplayMode} />
        {patientListDisplayMode === PatientListFilterDisplayMode.Grid ? <PatientListGrid /> : <PatientListTable />}
      </Box>

      <ConfirmDialog {...confirmOpenAddNewPatientDialogProps} />
    </Box>
  );
};
