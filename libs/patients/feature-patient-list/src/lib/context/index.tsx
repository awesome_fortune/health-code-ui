import React, { createContext, useContext, useReducer } from 'react';
import { PatientListActions, PatientListActionTypes } from './actions';
import * as patientApiClient from '@health-code-ui/patients/api-client';

export interface PatientListState {
  patients: any;
  isLoading: boolean;
}

const initialState: PatientListState = {
  patients: null,
  isLoading: false,
};

const PatientListContext = createContext<any>({ state: initialState, actions: null });

const useActions = (dispatch) => ({
  getAllPatients: () => dispatch({ type: PatientListActionTypes.GetAllPatients }),
});

const applySideEffectsMiddleware = (dispatch) => async (action: PatientListActions) => {
  dispatch({ type: PatientListActionTypes.SetLoading, payload: true });
  switch (action.type) {
    case PatientListActionTypes.GetAllPatients:
      try {
        const response = await patientApiClient.getAllPatients();

        dispatch({ type: PatientListActionTypes.GetAllPatientsSuccess, payload: response });
      } catch (error) {
        dispatch({ type: PatientListActionTypes.GetAllPatientsError, payload: error });
      }
      break;
    default:
      dispatch(action);
      break;
  }
};

const patientListReducer = (state: PatientListState, action: PatientListActions) => {
  switch (action.type) {
    case PatientListActionTypes.GetAllPatientsSuccess:
      return {
        ...state,
        patients: action.payload,
        isLoading: false,
      };
    case PatientListActionTypes.GetAllPatientsError:
      return {
        ...state,
        error: action.payload,
        isLoading: false,
      };
    case PatientListActionTypes.SetLoading:
      return {
        ...state,
        isLoading: action.payload,
      };
    default:
      throw new Error(`Unsupported action type: ${action.type}`);
  }
};

export const usePatientList = () => {
  const context = useContext(PatientListContext);

  if (!context) {
    throw new Error('usePatientList must be used within a PatientListFileProvider');
  }

  return context;
};

export const PatientListProvider = (props) => {
  const [state, dispatch] = useReducer(patientListReducer, initialState);
  const actions = useActions(applySideEffectsMiddleware(dispatch));

  return <PatientListContext.Provider value={{ state, actions }} {...props} />;
};
