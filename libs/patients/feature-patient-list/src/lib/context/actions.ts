export enum PatientListActionTypes {
  GetAllPatients = 'GetAllPatients',
  GetAllPatientsSuccess = 'GetAllPatientsSuccess',
  GetAllPatientsError = 'GetAllPatientsError',
  SetLoading = 'SetLoading',
}

export type PatientListActions =
  | { type: PatientListActionTypes.GetAllPatients }
  | { type: PatientListActionTypes.GetAllPatientsSuccess; payload: any }
  | { type: PatientListActionTypes.GetAllPatientsError; payload: any }
  | { type: PatientListActionTypes.SetLoading; payload: boolean };
