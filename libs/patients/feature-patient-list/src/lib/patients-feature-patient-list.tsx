import React from 'react';
import { PatientListFeatureContainer } from './components/feature-container';
import { PatientListProvider } from './context';

export const PatientsFeaturePatientList = () => {
  return (
    <PatientListProvider>
      <PatientListFeatureContainer />
    </PatientListProvider>
  );
};

export default PatientsFeaturePatientList;
