export const patientsBasePath = '/home/patients';
export const patientsRoutes = {
  createPatientFile: {
    path: `${patientsBasePath}/create-file`,
  },
  patientList: {
    path: `${patientsBasePath}/patient-list`,
  },
};
