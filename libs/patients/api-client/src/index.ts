import {
  httpClientBase,
  HttpClientMethods,
  StorageKeys,
  StorageService,
  StorageType,
} from '@health-code-ui/shared/util';

const baseUrl = '/api/patients';
const authState = StorageService.getItem(StorageKeys.AuthState, StorageType.SessionStorage);

export const getAllPatients = () => httpClientBase(baseUrl, { token: authState.user.jwttoken });

export const getPatientById = (patientId: string) =>
  httpClientBase(`${baseUrl}/${patientId}`, { token: authState.user.jwttoken });

export const createPatient = (body: any) =>
  httpClientBase(baseUrl, { body, token: authState.user.jwttoken, method: HttpClientMethods.POST });

export const getPatientFile = (patientId: string) => {
  httpClientBase(`${baseUrl}/${patientId}/patientFile`, { token: authState.user.jwttoken });
};

export const getPatientAppointments = (patientId: string) =>
  httpClientBase(`${baseUrl}/${patientId}/appointments`, { token: authState.user.jwttoken });
