import React, { lazy, Suspense } from 'react';
import { Route } from 'react-router-dom';
import { patientsRoutes } from '@health-code-ui/patients/state';

const PatientsFeatureCreatePatientFile = lazy(() =>
  import('libs/patients/feature-create-patient-file/src/lib/patients-feature-create-patient-file')
);
const PatientsFeaturePatientList = lazy(() =>
  import('libs/patients/feature-patient-list/src/lib/patients-feature-patient-list')
);

export const PatientsShellPatientsAdmin = () => {
  return (
    <Suspense fallback={<div>Loading...</div>}>
      <Route path={patientsRoutes.createPatientFile.path} component={PatientsFeatureCreatePatientFile} />
      <Route path={patientsRoutes.patientList.path} component={PatientsFeaturePatientList} />
    </Suspense>
  );
};

export default PatientsShellPatientsAdmin;
