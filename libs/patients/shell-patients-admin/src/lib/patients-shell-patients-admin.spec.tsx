import React from 'react';
import { render } from '@testing-library/react';

import PatientsShellPatientsAdmin from './patients-shell-patients-admin';

describe('PatientsShellPatientsAdmin', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<PatientsShellPatientsAdmin />);
    expect(baseElement).toBeTruthy();
  });
});
