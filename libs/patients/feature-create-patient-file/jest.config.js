module.exports = {
  name: 'patients-feature-create-patient-file',
  preset: '../../../jest.config.js',
  transform: {
    '^.+\\.[tj]sx?$': ['babel-jest', { cwd: __dirname, configFile: './babel-jest.config.json' }],
  },
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx'],
  coverageDirectory: '../../../coverage/libs/patients/feature-create-patient-file',
};
