import { PatientDetailsModel, PersonResponsibleForAccountSectionModel } from '@health-code-ui/shared/models';

export enum CreatePatientFileActionTypes {
  GoToNextStep = 'GoToNextStep',
  GoToPreviousStep = 'GoToPreviousStep',
  CreatePatientFile = 'CreatePatientFile',
  CreatePatientFileError = 'CreatePatientFileError',
  CreatePatientFileSuccess = 'CreatePatientFileSuccess',
  SetPatientDetailsStepData = 'SetPatientDetailsStepData',
  SetPersonResponsibleForAccountStepData = 'SetPersonResponsibleForAccountStepData',
  SetMedicalAidStepData = 'SetMedicalAidStepData',
  SetFamilyMembersStepData = 'SetFamilyMembersStepData',
  SetCreatePatientFileLoading = 'SetCreatePatientFileLoading',
}

export type CreatePatientFileActions =
  | { type: CreatePatientFileActionTypes.CreatePatientFile; payload: any }
  | { type: CreatePatientFileActionTypes.CreatePatientFileError; payload: any }
  | { type: CreatePatientFileActionTypes.CreatePatientFileSuccess; payload: any }
  | { type: CreatePatientFileActionTypes.GoToNextStep }
  | { type: CreatePatientFileActionTypes.GoToPreviousStep }
  | { type: CreatePatientFileActionTypes.SetPatientDetailsStepData; payload: PatientDetailsModel }
  | {
      type: CreatePatientFileActionTypes.SetPersonResponsibleForAccountStepData;
      payload: PersonResponsibleForAccountSectionModel;
    }
  | { type: CreatePatientFileActionTypes.SetMedicalAidStepData; payload: any }
  | { type: CreatePatientFileActionTypes.SetFamilyMembersStepData; payload: any }
  | { type: CreatePatientFileActionTypes.SetCreatePatientFileLoading; payload: boolean };
