import React, { createContext, useContext, useReducer } from 'react';
import {
  MedicalAidSectionModel,
  PatientDetailsModel,
  PersonResponsibleForAccountSectionModel,
} from '@health-code-ui/shared/models';
import { CreatePatientFileActions, CreatePatientFileActionTypes } from './actions';
import * as patientApiClient from '@health-code-ui/patients/api-client';

export interface CreatePatientFileState {
  currentStep: number;
  patientDetailsStepData: PatientDetailsModel;
  personResponsibleForAccountStepData: PersonResponsibleForAccountSectionModel;
  medicalAidStepData: MedicalAidSectionModel;
  familyMembersStepData: any;
  patientFileCreated: boolean;
  isLoading: boolean;
}

const initialState: CreatePatientFileState = {
  currentStep: 0,
  isLoading: false,
  patientFileCreated: false,
  patientDetailsStepData: {
    personalEmailAddress: null,
    personalCellphoneNumber: null,
    homeNumber: null,
    maritalStatus: null,
    employer: null,
    occupation: null,
    homeLanguage: null,
    preferredLanguageOfCommunication: null,
    numberOfDependants: null,
    title: null,
    name: null,
    surname: null,
    dateOfBirth: null,
    identityDocumentType: null,
    identityNumber: null,
    gender: null,
  },
  personResponsibleForAccountStepData: {},
  medicalAidStepData: {},
  familyMembersStepData: {
    personalEmailAddress: null,
    personalCellphoneNumber: null,
    homeNumber: null,
    maritalStatus: null,
    employer: null,
    occupation: null,
    homeLanguage: null,
    preferredLanguageOfCommunication: null,
    numberOfDependants: null,
    title: null,
    name: null,
    surname: null,
    dateOfBirth: null,
    identityDocumentType: null,
    identityNumber: null,
    gender: null,
  },
};

const CreatePatientFileContext = createContext<any>({ state: initialState, actions: null });

const useActions = (dispatch) => ({
  goToNextStep: () => dispatch({ type: CreatePatientFileActionTypes.GoToNextStep }),
  goToPreviousStep: () => dispatch({ type: CreatePatientFileActionTypes.GoToPreviousStep }),
  setPatientDetailsStepData: (data: PatientDetailsModel) =>
    dispatch({ type: CreatePatientFileActionTypes.SetPatientDetailsStepData, payload: data }),
  setPersonResponsibleForAccountStepData: (data: PersonResponsibleForAccountSectionModel) =>
    dispatch({ type: CreatePatientFileActionTypes.SetPersonResponsibleForAccountStepData, payload: data }),
  setMedicalAidStepData: (data: any) =>
    dispatch({ type: CreatePatientFileActionTypes.SetMedicalAidStepData, payload: data }),
  setFamilyMembersStepData: (data: any) =>
    dispatch({ type: CreatePatientFileActionTypes.SetFamilyMembersStepData, payload: data }),
  createPatientFile: (data: any) => dispatch({ type: CreatePatientFileActionTypes.CreatePatientFile, payload: data }),
});

const applySideEffectsMiddleware = (dispatch) => async (action: CreatePatientFileActions) => {
  switch (action.type) {
    case CreatePatientFileActionTypes.CreatePatientFile:
      try {
        dispatch({ type: CreatePatientFileActionTypes.SetCreatePatientFileLoading, payload: true });

        const response = await patientApiClient.createPatient(action.payload);

        dispatch({ type: CreatePatientFileActionTypes.CreatePatientFileSuccess, payload: response });
      } catch (error) {
        dispatch({ type: CreatePatientFileActionTypes.CreatePatientFileError, payload: error });
      }
      break;
    default:
      dispatch(action);
      break;
  }
};

const createPatientFileReducer = (state: CreatePatientFileState, action: CreatePatientFileActions) => {
  switch (action.type) {
    case CreatePatientFileActionTypes.GoToNextStep:
      return {
        ...state,
        currentStep: state.currentStep + 1,
      };

    case CreatePatientFileActionTypes.GoToPreviousStep:
      return {
        ...state,
        currentStep: state.currentStep - 1,
      };
    case CreatePatientFileActionTypes.SetPatientDetailsStepData:
      return {
        ...state,
        patientDetailsStepData: action.payload,
      };
    case CreatePatientFileActionTypes.SetPersonResponsibleForAccountStepData:
      return {
        ...state,
        personResponsibleForAccountStepData: action.payload,
      };
    case CreatePatientFileActionTypes.SetMedicalAidStepData:
      return {
        ...state,
        medicalAidStepData: action.payload,
      };
    case CreatePatientFileActionTypes.SetFamilyMembersStepData:
      return {
        ...state,
        familyMembersStepData: action.payload,
      };
    case CreatePatientFileActionTypes.CreatePatientFileSuccess:
      return {
        ...state,
        patientFileCreated: true,
        isLoading: false,
      };
    case CreatePatientFileActionTypes.CreatePatientFileError:
      return {
        ...state,
        error: action.payload,
        isLoading: false,
      };
    case CreatePatientFileActionTypes.SetCreatePatientFileLoading:
      return {
        ...state,
        isLoading: action.payload,
      };
    default:
      throw new Error(`Unsupported action type: ${action.type}`);
  }
};

export const useCreatePatientFile = () => {
  const context = useContext(CreatePatientFileContext);

  if (!context) {
    throw new Error('useCreatePatientFile must be used within a CreatePatientFileProvider');
  }

  return context;
};

export const CreatePatientFileProvider = (props) => {
  const [state, dispatch] = useReducer(createPatientFileReducer, initialState);
  const actions = useActions(applySideEffectsMiddleware(dispatch));

  return (
    <CreatePatientFileContext.Provider
      value={{
        state,
        actions,
      }}
      {...props}
    />
  );
};
