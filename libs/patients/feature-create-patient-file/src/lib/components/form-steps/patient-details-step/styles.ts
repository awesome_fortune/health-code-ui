import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
  buttonRoot: {
    marginTop: 20,
    float: 'right',
  },
  form: {
    display: 'flex',
    justifyContent: 'space-between',
    width: '81%',
  },
});
