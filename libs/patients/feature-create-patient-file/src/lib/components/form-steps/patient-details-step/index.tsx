import React from 'react';
import {
  ContactDetailsSection,
  IdentificationDetailsSection,
  PersonalDetailsSection,
  useActionButtonStyles,
} from '@health-code-ui/shared/ui';
import { Box, Button } from '@material-ui/core';
import { useCreatePatientFile } from '../../../context';
import { FormProvider, useForm } from 'react-hook-form';
import {
  ContactDetailsSectionModel,
  Gender,
  IdentificationDetailsSectionModel,
  IdentityDocumentType,
  MaritalStatus,
  PatientDetailsModel,
  PersonalDetailsSectionModel,
  SouthAfricanLanguages,
  Title,
} from '@health-code-ui/shared/models';
import { useStyles } from './styles';

export const PatientDetailsStep = () => {
  const classes = useStyles();
  const buttonClasses = useActionButtonStyles();
  const { actions, state } = useCreatePatientFile();
  const patientDetaisForm = useForm<PatientDetailsModel>({ mode: 'onBlur' });
  const { patientDetailsStepData }: { patientDetailsStepData: PatientDetailsModel } = state;

  const onSubmit = (data: PatientDetailsModel) => {
    if (patientDetaisForm.formState.isValid) {
      actions.setPatientDetailsStepData(data);
      actions.goToNextStep();
    }
  };

  const contactDetailsSectionProps: ContactDetailsSectionModel = {
    homeNumber: patientDetailsStepData.homeNumber,
    personalCellphoneNumber: patientDetailsStepData.personalCellphoneNumber,
    personalEmailAddress: patientDetailsStepData.personalEmailAddress,
  };

  const personalDetailsSectionProps: PersonalDetailsSectionModel = {
    maritalStatus: patientDetailsStepData.maritalStatus ?? MaritalStatus.Single,
    employer: patientDetailsStepData.employer,
    occupation: patientDetailsStepData.occupation,
    homeLanguage: patientDetailsStepData.homeLanguage ?? SouthAfricanLanguages.English,
    preferredLanguageOfCommunication:
      patientDetailsStepData.preferredLanguageOfCommunication ?? SouthAfricanLanguages.English,
    numberOfDependants: patientDetailsStepData.numberOfDependants ?? 0,
  };

  const identificationDetailsSectionProps: IdentificationDetailsSectionModel = {
    title: patientDetailsStepData.title ?? Title.Mr,
    name: patientDetailsStepData.name,
    surname: patientDetailsStepData.surname,
    dateOfBirth: patientDetailsStepData.dateOfBirth ?? new Date().toISOString(),
    identityDocumentType: patientDetailsStepData.identityDocumentType ?? IdentityDocumentType.RsaID,
    identityNumber: patientDetailsStepData.identityNumber,
    gender: patientDetailsStepData.gender ?? Gender.Male,
  };

  return (
    <FormProvider {...patientDetaisForm}>
      <form className={classes.form} noValidate autoComplete="off" onSubmit={patientDetaisForm.handleSubmit(onSubmit)}>
        <Box>
          <IdentificationDetailsSection {...identificationDetailsSectionProps} />
        </Box>
        <Box>
          <PersonalDetailsSection {...personalDetailsSectionProps} />
        </Box>
        <Box>
          <ContactDetailsSection {...contactDetailsSectionProps} />
          <Button
            type="submit"
            color="primary"
            variant="contained"
            classes={{
              root: `${buttonClasses.root} ${classes.buttonRoot}`,
              containedPrimary: buttonClasses.containedPrimary,
              label: buttonClasses.label,
            }}
          >
            Next
          </Button>
        </Box>
      </form>
    </FormProvider>
  );
};
