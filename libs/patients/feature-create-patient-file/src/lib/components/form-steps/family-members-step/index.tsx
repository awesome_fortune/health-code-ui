import React, { useEffect, useState } from 'react';
import {
  ConfirmDialog,
  ConfirmDialogProps,
  ContactDetailsSection,
  IdentificationDetailsSection,
  PersonalDetailsSection,
  useActionButtonStyles,
} from '@health-code-ui/shared/ui';
import { Box, Button } from '@material-ui/core';
import { useCreatePatientFile } from '../../../context';
import { FormProvider, useForm } from 'react-hook-form';
import { useStyles } from './styles';
import {
  ContactDetailsSectionModel,
  FamilyMembersSectionModel,
  Gender,
  IdentificationDetailsSectionModel,
  IdentityDocumentType,
  MaritalStatus,
  PatientDetailsModel,
  PersonalDetailsSectionModel,
  SouthAfricanLanguages,
  Title,
} from '@health-code-ui/shared/models';
import { patientsRoutes } from '@health-code-ui/patients/state';
import { useHistory } from 'react-router-dom';

const patientFilePayload = (familyMember: FamilyMembersSectionModel, patient: PatientDetailsModel) => ({
  employer: patient.employer,
  gender: patient.gender,
  homeLanguageId: 1,
  idNumber: patient.identityNumber,
  maritalStatusId: 1,
  name: patient.name,
  nextOfKinList: [
    {
      contactNumber: familyMember.personalCellphoneNumber,
      name: familyMember.name,
      relationship: '',
      surname: familyMember.surname,
    },
  ],
  occupation: patient.occupation,
  preferredLanguageId: 1,
  surname: patient.surname,
  title: patient.title,
});

export const FamilyMembersStep = () => {
  const classes = useStyles();
  const history = useHistory();
  const buttonClasses = useActionButtonStyles();
  const [openConfirmDialog, setOpenConfirmDialog] = useState<boolean>(false);
  const { actions, state } = useCreatePatientFile();
  const {
    familyMembersStepData,
    patientDetailsStepData,
  }: { patientDetailsStepData: PatientDetailsModel; familyMembersStepData: FamilyMembersSectionModel } = state;
  const familyMembersForm = useForm<FamilyMembersSectionModel>({ mode: 'onBlur' });

  const handleBackClick = () => {
    actions.goToPreviousStep();
  };

  const onSubmit = (data) => {
    if (familyMembersForm.formState.isValid) {
      actions.setFamilyMembersStepData(data);
      setOpenConfirmDialog(true);
    }
  };

  const contactDetailsSectionProps: ContactDetailsSectionModel = {
    homeNumber: familyMembersStepData.homeNumber,
    personalCellphoneNumber: familyMembersStepData.personalCellphoneNumber,
    personalEmailAddress: familyMembersStepData.personalEmailAddress,
  };

  const personalDetailsSectionProps: PersonalDetailsSectionModel = {
    maritalStatus: familyMembersStepData.maritalStatus ?? MaritalStatus.Single,
    employer: familyMembersStepData.employer,
    occupation: familyMembersStepData.occupation,
    homeLanguage: familyMembersStepData.homeLanguage ?? SouthAfricanLanguages.English,
    preferredLanguageOfCommunication:
      familyMembersStepData.preferredLanguageOfCommunication ?? SouthAfricanLanguages.English,
    numberOfDependants: familyMembersStepData.numberOfDependants ?? 0,
  };

  const identificationDetailsSectionProps: IdentificationDetailsSectionModel = {
    title: familyMembersStepData.title ?? Title.Mr,
    name: familyMembersStepData.name,
    surname: familyMembersStepData.surname,
    dateOfBirth: familyMembersStepData.dateOfBirth ?? new Date().toISOString(),
    identityDocumentType: familyMembersStepData.identityDocumentType ?? IdentityDocumentType.RsaID,
    identityNumber: familyMembersStepData.identityNumber,
    gender: familyMembersStepData.gender ?? Gender.Male,
  };

  const confirmDialogProps: ConfirmDialogProps = {
    open: openConfirmDialog,
    dialogTitle: 'Create patient file',
    dialogText: `Would you like to create a patient file for ${patientDetailsStepData.name} ${patientDetailsStepData.surname}?`,
    negativeActionText: 'No',
    positiveActionText: 'Yes',
    onClose: (result: boolean) => {
      if (result) {
        const payload = patientFilePayload(familyMembersStepData, patientDetailsStepData);
        actions.createPatientFile(payload);
      }

      setOpenConfirmDialog(false);
    },
  };

  useEffect(() => {
    if (state.patientFileCreated) {
      history.push(patientsRoutes.patientList.path);
    }
  }, [state.patientFileCreated]);

  return (
    <>
      <FormProvider {...familyMembersForm}>
        <form
          className={classes.form}
          noValidate
          autoComplete="off"
          onSubmit={familyMembersForm.handleSubmit(onSubmit)}
        >
          <Box>
            <IdentificationDetailsSection {...identificationDetailsSectionProps} />
          </Box>
          <Box>
            <PersonalDetailsSection {...personalDetailsSectionProps} />
          </Box>
          <Box display="flex" flexDirection="column">
            <ContactDetailsSection {...contactDetailsSectionProps} />
            <Box alignSelf="center" marginTop="20px">
              <Button
                onClick={handleBackClick}
                variant="outlined"
                color="primary"
                classes={{
                  root: buttonClasses.root,
                  containedPrimary: buttonClasses.containedPrimary,
                  label: buttonClasses.label,
                }}
              >
                Back
              </Button>
              <Button
                disabled={state.isLoading}
                type="submit"
                color="primary"
                variant="contained"
                style={{ marginLeft: 10 }}
                classes={{
                  root: buttonClasses.root,
                  containedPrimary: buttonClasses.containedPrimary,
                  label: buttonClasses.label,
                }}
              >
                Submit
              </Button>
            </Box>
          </Box>
        </form>
      </FormProvider>

      <ConfirmDialog {...confirmDialogProps} />
    </>
  );
};
