import React from 'react';
import { PersonResponsibleForAccountSection, useActionButtonStyles } from '@health-code-ui/shared/ui';
import { Box, Button } from '@material-ui/core';
import { useCreatePatientFile } from '../../../context';
import { FormProvider, useForm } from 'react-hook-form';
import { PersonResponsibleForAccountSectionModel } from '@health-code-ui/shared/models';
import { useStyles } from './styles';

export const PersonResponsibleForAccountStep = () => {
  const classes = useStyles();
  const buttonClasses = useActionButtonStyles();
  const { actions } = useCreatePatientFile();
  const personResponsibleForAccountForm = useForm<PersonResponsibleForAccountSectionModel>({ mode: 'onBlur' });

  const onSubmit = (data: PersonResponsibleForAccountSectionModel) => {
    if (personResponsibleForAccountForm.formState.isValid) {
      actions.setPersonResponsibleForAccountStepData(data);
      actions.goToNextStep();
    }
  };

  const handleBackClick = () => {
    actions.goToPreviousStep();
  };

  return (
    <FormProvider {...personResponsibleForAccountForm}>
      <form
        className={classes.form}
        noValidate
        autoComplete="off"
        onSubmit={personResponsibleForAccountForm.handleSubmit(onSubmit)}
      >
        <PersonResponsibleForAccountSection />
        <Box alignSelf="flex-end" marginTop="20px">
          <Button
            onClick={handleBackClick}
            variant="outlined"
            color="primary"
            classes={{
              root: buttonClasses.root,
              containedPrimary: buttonClasses.containedPrimary,
              label: buttonClasses.label,
            }}
          >
            Back
          </Button>
          <Button
            type="submit"
            color="primary"
            variant="contained"
            style={{ marginLeft: 10 }}
            classes={{
              root: buttonClasses.root,
              containedPrimary: buttonClasses.containedPrimary,
              label: buttonClasses.label,
            }}
          >
            Next
          </Button>
        </Box>
      </form>
    </FormProvider>
  );
};
