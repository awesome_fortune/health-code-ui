import React from 'react';
import { Box, Button } from '@material-ui/core';
import { MedicalAidDetailsSection, useActionButtonStyles } from '@health-code-ui/shared/ui';
import { useCreatePatientFile } from '../../../context';
import { useStyles } from './styles';
import { FormProvider, useForm } from 'react-hook-form';
import { MedicalAidSectionModel } from '@health-code-ui/shared/models';

export const MedicalAidStep = () => {
  const classes = useStyles();
  const buttonClasses = useActionButtonStyles();
  const { actions } = useCreatePatientFile();
  const medicalAidForm = useForm<MedicalAidSectionModel>({ mode: 'onBlur' });

  const handleBackClick = () => {
    actions.goToPreviousStep();
  };

  const onSubmit = (data: MedicalAidSectionModel) => {
    if (medicalAidForm.formState.isValid) {
      actions.setMedicalAidStepData(data);
      actions.goToNextStep();
    }
  };

  return (
    <FormProvider {...medicalAidForm}>
      <form className={classes.form} noValidate autoComplete="off" onSubmit={medicalAidForm.handleSubmit(onSubmit)}>
        <MedicalAidDetailsSection />
        <Box alignSelf="flex-end" marginTop="20px">
          <Button
            onClick={handleBackClick}
            variant="outlined"
            color="primary"
            classes={{
              root: buttonClasses.root,
              containedPrimary: buttonClasses.containedPrimary,
              label: buttonClasses.label,
            }}
          >
            Back
          </Button>
          <Button
            type="submit"
            color="primary"
            variant="contained"
            style={{ marginLeft: 10 }}
            classes={{
              root: buttonClasses.root,
              containedPrimary: buttonClasses.containedPrimary,
              label: buttonClasses.label,
            }}
          >
            Next
          </Button>
        </Box>
      </form>
    </FormProvider>
  );
};
