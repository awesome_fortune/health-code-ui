import React from 'react';
import { CreatePatientFileProvider } from './context';
import { CreatePatientFileFeatureContainer } from './components/feature-conainer';

export const PatientsFeatureCreatePatientFile = () => (
  <CreatePatientFileProvider>
    <CreatePatientFileFeatureContainer />
  </CreatePatientFileProvider>
);

export default PatientsFeatureCreatePatientFile;
