import React from 'react';
import { render } from '@testing-library/react';

import PatientsFeatureCreatePatientFile from './patients-feature-create-patient-file';

describe('PatientsFeatureCreatePatientFile', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<PatientsFeatureCreatePatientFile />);
    expect(baseElement).toBeTruthy();
  });
});
