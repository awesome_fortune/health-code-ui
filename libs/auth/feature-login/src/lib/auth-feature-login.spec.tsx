import React from 'react';
import { render } from '@testing-library/react';

import AuthFeatureLogin from './auth-feature-login';

describe(' AuthFeatureLogin', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<AuthFeatureLogin />);
    expect(baseElement).toBeTruthy();
  });
});
