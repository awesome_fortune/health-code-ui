import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core';
import { cssVariables } from '@health-code-ui/shared/ui';
import backgroundImage from '../../../../shared/assets/src/lib/images/antibiotic-capsules.png';
import { Box, Snackbar, Typography } from '@material-ui/core';
import { LoginFormModel } from '@health-code-ui/shared/models';
import { useAuth } from '@health-code-ui/auth/state';
import { Alert, AlertTitle, Color } from '@material-ui/lab';
import { LoginForm, LoginFormProps } from '@health-code-ui/auth/ui-components';

const useStyles = makeStyles({
  root: {
    backgroundImage: `url(${backgroundImage})`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
  },
  primaryTypography: {
    marginTop: 30,
    fontSize: 36,
    fontWeight: 300,
    color: cssVariables.colors.tahitiGold,
  },
  secondaryTypography: {
    fontSize: 48,
    fontWeight: 600,
    color: cssVariables.colors.tahitiGold,
  },
});

export const AuthFeatureLogin = () => {
  const classes = useStyles();
  const { actions, state } = useAuth();
  const [displaySnackbar, setDisplaySnackbar] = useState<boolean>(false);
  const [alertMessage, setAlertMessage] = useState<string>(null);
  const [alertSeverity, setAlertSeverity] = useState<Color>(null);
  const [alertTitle, setAlertTitle] = useState<string>(null);

  const submitLoginForm = (data: LoginFormModel) => {
    actions.login(data);
  };

  const closeSnackbar = (event?: React.SyntheticEvent, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }

    setDisplaySnackbar(false);
  };

  const loginFormProps: LoginFormProps = {
    loading: state.isLoading,
    submitLoginForm,
  };

  useEffect(() => {
    if (state.error) {
      setAlertTitle('Error');
      setAlertSeverity('error');
      setAlertMessage('Invalid login credentials');
      setDisplaySnackbar(true);
    }
  }, [state.error]);

  return (
    <>
      <Box
        display="flex"
        width="100%"
        height="100%"
        justifyContent="space-between"
        padding="60px 185px"
        className={classes.root}
      >
        <Box>
          <Typography variant="h3" className={classes.primaryTypography}>
            Welcome to <br /> Code 4 Hire
          </Typography>
          <Typography variant="h3" className={classes.secondaryTypography}>
            H-Code
          </Typography>
        </Box>
        <Box>
          <LoginForm {...loginFormProps} />
        </Box>
      </Box>
      <Snackbar
        open={displaySnackbar}
        autoHideDuration={3000}
        onClose={closeSnackbar}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      >
        <Alert severity={alertSeverity}>
          <AlertTitle>{alertTitle}</AlertTitle>
          {alertMessage}
        </Alert>
      </Snackbar>
    </>
  );
};

export default AuthFeatureLogin;
