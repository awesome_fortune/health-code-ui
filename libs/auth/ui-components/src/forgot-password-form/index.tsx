import React from 'react';
import { useLinkStyles, useTypographyStyles, useContainerStyles, useFormStyles } from './styles';
import { useForm } from 'react-hook-form';
import { ForgotPasswordFormModel } from '@health-code-ui/shared/models';
import { Button, Link, Paper, TextField, Typography } from '@material-ui/core';
import { useActionButtonStyles } from '@health-code-ui/shared/ui';
import { authRoutes } from '@health-code-ui/auth/state';

export interface ForgotPasswordFormProps {
  submitForgotPasswordForm(data: ForgotPasswordFormModel): void;
}

export const ForgotPasswordForm = (props: ForgotPasswordFormProps) => {
  const buttonClasses = useActionButtonStyles();
  const typographyClasses = useTypographyStyles();
  const containerClasses = useContainerStyles();
  const formClasses = useFormStyles();
  const linkStyles = useLinkStyles();
  const { submitForgotPasswordForm } = props;
  const { register, handleSubmit, errors } = useForm<ForgotPasswordFormModel>();

  const onSubmit = (data) => submitForgotPasswordForm(data);

  return (
    <Paper square={true} elevation={0} className={containerClasses.root}>
      <div className={containerClasses.mainContent}>
        <Typography className={typographyClasses.h3} variant="h3">
          Forgot password
        </Typography>
        <Typography className={typographyClasses.h5} variant="h5">
          An access code has been sent to you by email <br /> or SMS. Please enter the code to continue.
        </Typography>
        <form className={formClasses.root} noValidate autoComplete="off" onSubmit={handleSubmit(onSubmit)}>
          <TextField
            inputRef={register({ required: { value: true, message: 'Access code is required' } })}
            className={formClasses.accessCodeField}
            type="password"
            label="Access code"
            placeholder="****"
            id="accessCode"
            name="accessCode"
            error={!!errors.accessCode}
            helperText={errors.accessCode ? errors.accessCode.message : null}
          />
          <Button
            type="submit"
            variant="contained"
            color="primary"
            classes={{
              root: buttonClasses.root,
              containedPrimary: buttonClasses.containedPrimary,
              label: buttonClasses.label,
            }}
          >
            Verify password
          </Button>
        </form>
      </div>
      <div className={containerClasses.footer}>
        <Typography className={typographyClasses.paragraph}>
          If your access code isn’t working, you can{' '}
          <Link underline="none" className={linkStyles.root}>
            resend it.
          </Link>
        </Typography>
        <Typography style={{ marginTop: 8 }} className={typographyClasses.paragraph}>
          If you’re having issues registering, please contact us on <br /> 0860 100 333 or email help@hcode.com
        </Typography>
        <Typography style={{ marginTop: 8 }} className={typographyClasses.paragraph}>
          <Link underline="none" className={linkStyles.root} href={`${authRoutes.login.path}`}>
            Back to log in
          </Link>
        </Typography>
      </div>
    </Paper>
  );
};
