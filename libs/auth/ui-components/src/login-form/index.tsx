import React from 'react';
import { useLinkStyles, useContainerStyles, useFormStyles, useTypographyStyles } from './styles';
import { useForm } from 'react-hook-form';
import { LoginFormModel } from '@health-code-ui/shared/models';
import { Button, CircularProgress, Link, Paper, TextField, Typography } from '@material-ui/core';
import { authRoutes } from '@health-code-ui/auth/state';
import { useActionButtonStyles } from '@health-code-ui/shared/ui';

export interface LoginFormProps {
  loading: boolean;
  submitLoginForm(data: LoginFormModel): void;
}

export const LoginForm = (props: LoginFormProps) => {
  const buttonClasses = useActionButtonStyles();
  const containerClasses = useContainerStyles();
  const formClasses = useFormStyles();
  const typographyClasses = useTypographyStyles();
  const linkClasses = useLinkStyles();
  const { submitLoginForm, loading } = props;
  const { register, handleSubmit, errors } = useForm<LoginFormModel>();

  const onSubmit = (data) => submitLoginForm(data);

  return (
    <Paper square={true} elevation={0} className={containerClasses.root}>
      <div className={containerClasses.mainContent}>
        <Typography className={typographyClasses.h3} variant="h3">
          Hello,
        </Typography>
        <Typography className={typographyClasses.h5} variant="h5">
          Please log in with your <br /> credentials.
        </Typography>
        <form className={formClasses.root} noValidate onSubmit={handleSubmit(onSubmit)}>
          <TextField
            inputRef={register({ required: { value: true, message: 'Username is required' } })}
            className={formClasses.userNameField}
            type="text"
            id="username"
            name="username"
            label="Username"
            placeholder="johndoe@gmail.com"
            error={!!errors.username}
            helperText={errors.username ? errors.username.message : null}
            disabled={loading}
            autoComplete="username"
          />

          <TextField
            inputRef={register({
              required: { value: true, message: 'Password is required' },
              minLength: { value: 8, message: 'Password should be at least 8 characters long' },
            })}
            className={formClasses.passwordField}
            type="password"
            name="password"
            id="password"
            label="Password"
            placeholder="Should be 8 characters long"
            error={!!errors.password}
            helperText={errors.password ? errors.password.message : null}
            disabled={loading}
            autoComplete="current-password"
          />

          <Button
            type="submit"
            color="primary"
            variant="contained"
            classes={{
              root: buttonClasses.root,
              containedPrimary: buttonClasses.containedPrimary,
              label: buttonClasses.label,
            }}
            disabled={loading}
          >
            {loading && <CircularProgress size={28} />}
            {!loading && 'Log In'}
          </Button>
        </form>
      </div>
      <div className={containerClasses.footer}>
        <Typography className={typographyClasses.paragraph} variant="body1">
          If you don’t have a profile,{' '}
          <Link className={linkClasses.root} underline="none" href={`${authRoutes.registerNewAccount.path}`}>
            please register here.
          </Link>
        </Typography>
        <Typography className={typographyClasses.paragraph} variant="body1">
          <Link className={linkClasses.root} underline="none" href={`${authRoutes.forgotPassword.path}`}>
            Forgot password?
          </Link>{' '}
          <Link className={linkClasses.root} underline="none" href={`${authRoutes.forgotPassword.path}`}>
            Forgot username?
          </Link>
        </Typography>
      </div>
    </Paper>
  );
};
