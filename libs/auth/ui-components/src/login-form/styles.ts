import { makeStyles } from '@material-ui/core';
import { cssVariables } from '@health-code-ui/shared/ui';

export const useContainerStyles = makeStyles({
  root: {
    background: cssVariables.colors.white,
    width: 582,
    boxShadow: `0px 1px 31px ${cssVariables.colors.blackAlpha2}`,
    display: 'flex',
    flexDirection: 'column',
    '& button': {
      maxWidth: 135,
    },
  },
  mainContent: {
    paddingLeft: 40,
    paddingTop: 45,
    paddingBottom: 45,
  },
  footer: {
    paddingLeft: 40,
    paddingTop: 40,
    paddingBottom: 40,
    backgroundColor: cssVariables.colors.mineShaft,
  },
});

export const useFormStyles = makeStyles({
  root: {
    display: 'flex',
    flexDirection: 'column',
    marginTop: 25,
  },
  userNameField: {
    width: 305,
  },
  passwordField: {
    width: 305,
    marginTop: 35,
    marginBottom: 35,
  },
});

export const useTypographyStyles = makeStyles({
  paragraph: {
    color: cssVariables.colors.white,
    fontSize: 18,
  },
  h3: {
    fontWeight: 300,
  },
  h5: {
    fontWeight: 300,
  },
});

export const useLinkStyles = makeStyles({
  root: {
    fontWeight: 700,
    color: cssVariables.colors.white,
    cursor: 'pointer',
  },
});
