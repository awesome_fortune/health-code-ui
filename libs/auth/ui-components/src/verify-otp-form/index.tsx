import React from 'react';
import { useTypographyStyles, useContainerStyles, useFormStyles, useLinkStyles } from './styles';
import { useForm } from 'react-hook-form';
import { VerifyOtpFormModel } from '@health-code-ui/shared/models';
import { Button, Link, Paper, TextField, Typography } from '@material-ui/core';
import { useActionButtonStyles } from '@health-code-ui/shared/ui';

export interface VerifyOtpFormProps {
  submitVerifyOtpForm(data: VerifyOtpFormModel): void;
}

export const VerifyOtpForm = (props: VerifyOtpFormProps) => {
  const buttonClasses = useActionButtonStyles();
  const typographyClasses = useTypographyStyles();
  const containerClasses = useContainerStyles();
  const formClasses = useFormStyles();
  const linkClasses = useLinkStyles();
  const { submitVerifyOtpForm } = props;
  const { register, handleSubmit, errors } = useForm<VerifyOtpFormModel>();

  const onSubmit = (data) => submitVerifyOtpForm(data);

  return (
    <Paper square={true} elevation={0} className={containerClasses.root}>
      <div className={containerClasses.mainContent}>
        <Typography className={typographyClasses.h3} variant="h3">
          Number verification
        </Typography>
        <Typography className={typographyClasses.h5} variant="h5">
          We have sent an access code to the following <br /> number: *** *** 1234
        </Typography>
        <form className={formClasses.root} noValidate autoComplete="off" onSubmit={handleSubmit(onSubmit)}>
          <TextField
            inputRef={register({ required: { value: true, message: 'Access code is required' } })}
            className={formClasses.accessCodeField}
            type="password"
            label="Access code"
            id="accessCode"
            name="accessCode"
            placeholder="****"
            error={!!errors.accessCode}
            helperText={errors.accessCode ? errors.accessCode.message : null}
          />
          <Button
            type="submit"
            variant="contained"
            color="primary"
            classes={{
              root: buttonClasses.root,
              containedPrimary: buttonClasses.containedPrimary,
              label: buttonClasses.label,
            }}
          >
            Verify
          </Button>
        </form>
      </div>
      <div className={containerClasses.footer}>
        <Typography className={typographyClasses.paragraph} variant="body1">
          Already have an existing H-Code profile?{' '}
          <Link className={linkClasses.root} underline="none">
            Log in here.
          </Link>
        </Typography>
        <Typography className={typographyClasses.paragraph} variant="body1">
          If you’re having issues registering, please contact us on{' '}
          <Link className={linkClasses.root} underline="none">
            0860 100 333
          </Link>{' '}
          or{' '}
          <Link className={linkClasses.root} underline="none">
            email help@hcode.com
          </Link>
        </Typography>
      </div>
    </Paper>
  );
};
