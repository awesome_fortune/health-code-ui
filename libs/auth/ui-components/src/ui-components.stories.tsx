import React from 'react';
import { muiTheme } from 'storybook-addon-material-ui';
import { theme } from '@health-code-ui/shared/ui';
import centered from '@storybook/addon-centered/react';
import {
  RegisterNewAccountFormModel,
  ForgotPasswordFormModel,
  VerifyOtpFormModel,
  LoginFormModel,
} from '@health-code-ui/shared/models';
import { boolean } from '@storybook/addon-knobs';
import { LoginForm, LoginFormProps } from '@health-code-ui/auth/ui-components';
import { VerifyOtpForm, VerifyOtpFormProps } from '@health-code-ui/auth/ui-components';
import { ForgotPasswordForm, ForgotPasswordFormProps } from '@health-code-ui/auth/ui-components';
import { RegisterNewAccountForm, RegisterNewAccountFormProps } from '@health-code-ui/auth/ui-components';

export default {
  title: 'Auth Forms',
  decorators: [muiTheme([theme]), centered],
};

export const login = () => {
  const submitLoginForm = (data: LoginFormModel) => alert(JSON.stringify(data));
  const props: LoginFormProps = {
    loading: boolean('Loading', false),
    submitLoginForm,
  };

  return <LoginForm {...props} />;
};

export const verifyOtp = () => {
  const submitVerifyOtpForm = (data: VerifyOtpFormModel) => alert(JSON.stringify(data));
  const props: VerifyOtpFormProps = {
    submitVerifyOtpForm,
  };

  return <VerifyOtpForm {...props} />;
};

export const forgotPassword = () => {
  const submitForgotPasswordForm = (data: ForgotPasswordFormModel) => alert(JSON.stringify(data));
  const props: ForgotPasswordFormProps = {
    submitForgotPasswordForm,
  };

  return <ForgotPasswordForm {...props} />;
};

export const registerNewAccount = () => {
  const submitRegisterNewAccountForm = (data: RegisterNewAccountFormModel) => alert(JSON.stringify(data));
  const props: RegisterNewAccountFormProps = {
    loading: boolean('Loading', false),
    submitRegisterNewAccountForm,
  };

  return <RegisterNewAccountForm {...props} />;
};
