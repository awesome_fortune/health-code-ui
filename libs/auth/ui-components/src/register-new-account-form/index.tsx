import React from 'react';
import { useForm, Controller } from 'react-hook-form';
import {
  Button,
  CircularProgress,
  FormControl,
  FormControlLabel,
  FormLabel,
  Grid,
  Link,
  Paper,
  Radio,
  RadioGroup,
  TextField,
  Typography,
} from '@material-ui/core';
import { useTypographyStyles, useContainerStyles, useLinkStyles, useGridStyles } from './styles';
import { RegisterNewAccountFormModel } from '@health-code-ui/shared/models';
import { authRoutes } from '@health-code-ui/auth/state';
import { useActionButtonStyles } from '@health-code-ui/shared/ui';

export interface RegisterNewAccountFormProps {
  loading: boolean;
  submitRegisterNewAccountForm(data: RegisterNewAccountFormModel): void;
}

const defaultFormValues: RegisterNewAccountFormModel = {
  preferredMethodOfCommunication: 'email',
};

const emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

export const RegisterNewAccountForm = (props: RegisterNewAccountFormProps) => {
  const buttonClasses = useActionButtonStyles();
  const typographyClasses = useTypographyStyles();
  const containerClasses = useContainerStyles();
  const linkClasses = useLinkStyles();
  const gridClasses = useGridStyles();
  const { loading, submitRegisterNewAccountForm } = props;
  const { register, handleSubmit, errors, control, getValues } = useForm<RegisterNewAccountFormModel>({
    defaultValues: defaultFormValues,
  });

  const onSubmit = (data: RegisterNewAccountFormModel) => submitRegisterNewAccountForm(data);

  const passwordMatchValidation = () => getValues('password') === getValues('confirmPassword');

  const passwordHelperText = () => {
    if (errors.password) {
      switch (errors.password.type) {
        case 'validate':
          return 'Password needs to be the same as Confirm password';
        case 'minLength':
        case 'required':
          return errors.password.message;
      }
    }
  };

  const confirmPasswordHelperText = () => {
    if (errors.confirmPassword) {
      switch (errors.confirmPassword.type) {
        case 'validate':
          return 'Confirm password needs to be the same as Password';
        case 'minLength':
        case 'required':
          return errors.confirmPassword.message;
      }
    }
  };

  return (
    <Paper square={true} elevation={0} className={containerClasses.root}>
      <div className={containerClasses.mainContent}>
        <Typography className={typographyClasses.h3} variant="h3">
          New account
        </Typography>
        <Typography className={typographyClasses.h5} variant="h5">
          Please register below.
        </Typography>
        <form noValidate autoComplete="off" onSubmit={handleSubmit(onSubmit)}>
          <Grid container wrap="wrap">
            <Grid item className={gridClasses.item}>
              <TextField
                inputRef={register({ required: { value: true, message: 'Name is required' } })}
                name="name"
                type="text"
                label="Name"
                placeholder="Name"
                error={!!errors.name}
                helperText={errors.name ? errors.name.message : null}
                disabled={loading}
              />
            </Grid>
            <Grid item className={gridClasses.item}>
              <TextField
                inputRef={register({ required: { value: true, message: 'Surname is required' } })}
                name="surname"
                type="text"
                label="Surname"
                placeholder="Surname"
                error={!!errors.surname}
                helperText={errors.surname ? errors.surname.message : null}
                disabled={loading}
              />
            </Grid>
            <Grid item className={gridClasses.item}>
              <TextField
                inputRef={register({
                  required: {
                    value: getValues('preferredMethodOfCommunication') === 'sms',
                    message: 'Cell number is required',
                  },
                })}
                name="cellNumber"
                type="tel"
                label="Cell number"
                placeholder="Cell number"
                error={!!errors.cellNumber}
                helperText={errors.cellNumber ? errors.cellNumber.message : null}
                disabled={loading}
              />
            </Grid>
            <Grid item className={gridClasses.item}>
              <TextField
                inputRef={register({
                  required: {
                    value: getValues('preferredMethodOfCommunication') === 'email',
                    message: 'Email is required',
                  },
                  pattern: {
                    value: emailRegex,
                    message: 'Value does not match email format',
                  },
                })}
                name="email"
                type="email"
                label="Email"
                placeholder="Email"
                error={!!errors.email}
                helperText={errors.email ? errors.email.message : null}
                disabled={loading}
              />
            </Grid>
            <Grid item style={{ marginTop: '1rem' }}>
              <FormControl component="fieldset">
                <FormLabel component="legend">Please select your preferred method of communication</FormLabel>
                <Controller
                  name="preferredMethodOfCommunication"
                  control={control}
                  as={
                    <RadioGroup aria-label="Preferred method of communication">
                      <Grid container>
                        <Grid item>
                          <FormControlLabel
                            labelPlacement="bottom"
                            value="sms"
                            control={<Radio color="primary" />}
                            label="SMS"
                            disabled={loading}
                          />
                        </Grid>
                        <Grid item>
                          <FormControlLabel
                            labelPlacement="bottom"
                            value="email"
                            control={<Radio color="primary" />}
                            label="Email"
                            disabled={loading}
                          />
                        </Grid>
                      </Grid>
                    </RadioGroup>
                  }
                />
                <hr />
              </FormControl>
            </Grid>
          </Grid>

          <Typography style={{ fontWeight: 500, marginTop: '1rem' }} variant="body1">
            Create your username and password
          </Typography>

          <Grid container wrap="wrap">
            <Grid item className={gridClasses.item}>
              <TextField
                inputRef={register({ required: { value: true, message: 'Username is required' } })}
                name="username"
                type="text"
                label="Username"
                placeholder="Username"
                error={!!errors.username}
                helperText={errors.username ? errors.username.message : null}
                disabled={loading}
              />
            </Grid>
            <Grid container>
              <Grid item className={gridClasses.item}>
                <TextField
                  inputRef={register({
                    required: { value: true, message: 'Password is required' },
                    minLength: { value: 8, message: 'Password should be at least 8 characters long' },
                    validate: passwordMatchValidation,
                  })}
                  name="password"
                  type="password"
                  label="Password"
                  placeholder="Should be 8 characters long"
                  error={!!errors.password}
                  helperText={passwordHelperText()}
                  disabled={loading}
                />
              </Grid>
              <Grid item className={gridClasses.item}>
                <TextField
                  inputRef={register({
                    required: { value: true, message: 'Confirm password is required' },
                    minLength: { value: 8, message: 'Confirm password should be at least 8 characters long' },
                    validate: passwordMatchValidation,
                  })}
                  name="confirmPassword"
                  type="password"
                  label="Confirm password"
                  placeholder="Should be 8 characters long"
                  error={!!errors.confirmPassword}
                  helperText={confirmPasswordHelperText()}
                  disabled={loading}
                />
              </Grid>
            </Grid>
          </Grid>

          <Button
            type="submit"
            classes={{
              root: buttonClasses.root,
              containedPrimary: buttonClasses.containedPrimary,
              label: buttonClasses.label,
            }}
            color="primary"
            variant="contained"
            disabled={loading}
          >
            {loading && <CircularProgress size={28} />}
            {!loading && 'Register'}
          </Button>
        </form>
      </div>
      <div className={containerClasses.footer}>
        <Typography className={typographyClasses.paragraph} variant="body1">
          Already have an existing H-Code profile?{' '}
          <Link
            className={linkClasses.root}
            underline="none"
            href={`${window.location.origin}${authRoutes.login.path}`}
          >
            Log in here.
          </Link>
        </Typography>
        <Typography className={typographyClasses.paragraph} variant="body1">
          If you’re having issues registering, please contact us on{' '}
          <Link className={linkClasses.root} underline="none">
            0860 100 333
          </Link>{' '}
          or{' '}
          <Link className={linkClasses.root} underline="none">
            email help@hcode.com
          </Link>
        </Typography>
      </div>
    </Paper>
  );
};
