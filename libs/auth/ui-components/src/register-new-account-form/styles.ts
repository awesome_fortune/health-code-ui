import { makeStyles } from '@material-ui/core';
import { cssVariables } from '@health-code-ui/shared/ui';

export const useContainerStyles = makeStyles({
  root: {
    background: cssVariables.colors.white,
    width: 582,
    boxShadow: `0px 2px 20px ${cssVariables.colors.blackAlpha2}`,
    display: 'flex',
    flexDirection: 'column',
    '& button': {
      marginTop: '2rem',
    },
  },
  mainContent: {
    paddingLeft: 40,
    paddingTop: 45,
    paddingBottom: 45,
  },
  footer: {
    paddingLeft: 40,
    paddingTop: 40,
    paddingBottom: 40,
    backgroundColor: cssVariables.colors.mineShaft,
  },
});

export const useTypographyStyles = makeStyles({
  paragraph: {
    color: cssVariables.colors.white,
    fontSize: 18,
  },
  h3: {
    fontWeight: 300,
  },
  h5: {
    fontWeight: 300,
  },
});

export const useLinkStyles = makeStyles({
  root: {
    fontWeight: 700,
    color: cssVariables.colors.white,
    cursor: 'pointer',
  },
});

export const useGridStyles = makeStyles({
  item: {
    flexBasis: '45%',
    marginRight: '1rem',
    '& > * ': { width: '100%', marginTop: 25 },
  },
});
