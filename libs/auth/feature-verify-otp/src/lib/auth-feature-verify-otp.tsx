import React from 'react';
import { cssVariables } from '@health-code-ui/shared/ui';
import backgroundImage from '../../../../shared/assets/src/lib/images/antibiotic-capsules.png';
import { VerifyOtpFormModel } from '@health-code-ui/shared/models';
import { VerifyOtpForm, VerifyOtpFormProps } from '@health-code-ui/auth/ui-components';
import { makeStyles, Box, Typography } from '@material-ui/core';

const useStyles = makeStyles({
  root: {
    backgroundImage: `url(${backgroundImage})`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
  },
  primaryTypography: {
    marginTop: 30,
    fontSize: 36,
    fontWeight: 300,
    color: cssVariables.colors.tahitiGold,
    marginBottom: 50,
  },
});

export const AuthFeatureVerifyOtp = () => {
  const classes = useStyles();

  const submitVerifyOtpForm = (data: VerifyOtpFormModel) => {
    // Integrate with API
  };

  const verifyOtpFormProps: VerifyOtpFormProps = {
    submitVerifyOtpForm,
  };

  return (
    <Box display="flex" flexDirection="column" width="100%" height="100%" padding="60px 200px" className={classes.root}>
      <Typography variant="h3" className={classes.primaryTypography}>
        Welcome to <br /> Code 4 Hire H-Code
      </Typography>

      <VerifyOtpForm {...verifyOtpFormProps} />
    </Box>
  );
};

export default AuthFeatureVerifyOtp;
