import React from 'react';
import { render } from '@testing-library/react';

import AuthFeatureVerifyOtp from './auth-feature-verify-otp';

describe(' AuthFeatureVerifyOtp', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<AuthFeatureVerifyOtp />);
    expect(baseElement).toBeTruthy();
  });
});
