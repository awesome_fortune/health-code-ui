import { httpClientBase, HttpClientMethods } from '@health-code-ui/shared/util';
import { LoginFormModel, RegisterNewAccountFormModel } from '@health-code-ui/shared/models';

const baseUrl = '/api/authenticate';

export const login = (body: LoginFormModel) =>
  httpClientBase(`${baseUrl}/username`, {
    body,
    method: HttpClientMethods.POST,
  });

export const forgotPassword = (body: any) =>
  httpClientBase(`${baseUrl}/forgotPassword`, {
    body,
    method: HttpClientMethods.POST,
  });

export const registerByCellphone = (body: RegisterNewAccountFormModel) =>
  httpClientBase(`${baseUrl}/registerByCellphone`, {
    body,
    method: HttpClientMethods.POST,
  });

export const registerByEmail = (body: RegisterNewAccountFormModel) =>
  httpClientBase(`${baseUrl}/registerByEmail`, {
    body,
    method: HttpClientMethods.POST,
  });

export const requestOtp = (body: any) =>
  httpClientBase(`${baseUrl}/requestOtp`, {
    body,
    method: HttpClientMethods.POST,
  });

export const resetPassword = (body: any) =>
  httpClientBase(`${baseUrl}/resetPassword`, {
    body,
    method: HttpClientMethods.POST,
  });

export const verifyOtp = (body: any) =>
  httpClientBase(`${baseUrl}/verifyOtp`, {
    body,
    method: HttpClientMethods.POST,
  });

export * from './models/login-response.model';
