export interface LoginResponseModel {
  firstName: string;
  surname: string;
  jwttoken: string;
}
