import React, { useEffect, lazy, Suspense } from 'react';
import { Redirect, Route, useHistory } from 'react-router-dom';
import { authBasePath, authRoutes, AuthStateTypes, useAuth } from '@health-code-ui/auth/state';
import { quicklinksBasePath } from '@health-code-ui/quicklinks/state';

const AuthFeatureLogin = lazy(() => import('libs/auth/feature-login/src/lib/auth-feature-login'));
const AuthFeatureForgotPassword = lazy(() =>
  import('libs/auth/feature-forgot-password/src/lib/auth-feature-forgot-password')
);
const AuthFeatureVerifyOtp = lazy(() => import('libs/auth/feature-verify-otp/src/lib/auth-feature-verify-otp'));

export const AuthShellAuthAdmin = () => {
  const { state } = useAuth();
  const history = useHistory();

  useEffect(() => {
    switch (state.authStateType) {
      case AuthStateTypes.LoggedIn:
        history.push(quicklinksBasePath);
        break;
    }
  }, [state.authStateType]);

  return (
    <Suspense fallback={<div>Loading...</div>}>
      <Route exact path={authBasePath}>
        <Redirect to={authRoutes.login.path} />
      </Route>
      <Route path={authRoutes.login.path} component={AuthFeatureLogin} />
      <Route path={authRoutes.forgotPassword.path} component={AuthFeatureForgotPassword} />
      <Route path={authRoutes.verifyOtp.path} component={AuthFeatureVerifyOtp} />
    </Suspense>
  );
};
