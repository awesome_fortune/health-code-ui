import React from 'react';
import { render } from '@testing-library/react';

import AuthShellAuthAdmin from './auth-shell-auth-admin';

describe(' AuthShellAuthAdmin', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<AuthShellAuthAdmin />);
    expect(baseElement).toBeTruthy();
  });
});
