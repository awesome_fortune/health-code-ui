import React, { SyntheticEvent, useEffect, useState } from 'react';
import { cssVariables } from '@health-code-ui/shared/ui';
import backgroundImage from '../../../../shared/assets/src/lib/images/antibiotic-capsules.png';
import { RegisterNewAccountFormModel } from '@health-code-ui/shared/models';
import { authRoutes, AuthStateTypes, useAuth } from '@health-code-ui/auth/state';
import { Box, Snackbar, Typography, makeStyles } from '@material-ui/core';
import { Alert, AlertTitle } from '@material-ui/lab';
import { useHistory } from 'react-router-dom';
import { RegisterNewAccountForm, RegisterNewAccountFormProps } from '@health-code-ui/auth/ui-components';

const useStyles = makeStyles({
  root: {
    backgroundImage: `url(${backgroundImage})`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    overflow: 'scroll',
  },
  primaryTypography: {
    marginTop: 30,
    fontSize: 36,
    fontWeight: 300,
    color: cssVariables.colors.tahitiGold,
  },
  secondaryTypography: {
    fontSize: 48,
    fontWeight: 600,
    color: cssVariables.colors.tahitiGold,
  },
});

export const AuthFeatureRegisterNewAccount = () => {
  const classes = useStyles();
  const history = useHistory();
  const { actions, state } = useAuth();
  const [displaySnackbar, setDisplaySnackbar] = useState<boolean>(false);
  const [alertMessage, setAlertMessage] = useState<string>(null);

  const submitRegisterNewAccountForm = (data: RegisterNewAccountFormModel) => {
    actions.registerNewAccount(data);
  };

  const closeSnackbar = (event?: SyntheticEvent, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }

    setDisplaySnackbar(false);
  };

  useEffect(() => {
    if (state.error) {
      setAlertMessage(state.error.message);
      setDisplaySnackbar(true);
    }
  }, [state.error]);

  useEffect(() => {
    if (state.authStateType === AuthStateTypes.RegisterNewAccountSuccess) {
      history.push(authRoutes.login.path);
    }
  }, [state.authStateType]);

  const registerNewAccountFormProps: RegisterNewAccountFormProps = {
    loading: state.isLoading,
    submitRegisterNewAccountForm,
  };

  return (
    <>
      <Box
        display="flex"
        width="100%"
        height="100%"
        justifyContent="space-between"
        padding="60px 185px 900px 185px"
        className={classes.root}
      >
        <Box>
          <Typography variant="h3" className={classes.primaryTypography}>
            Welcome to <br /> Code 4 Hire
          </Typography>
          <Typography variant="h3" className={classes.secondaryTypography}>
            H-Code
          </Typography>
        </Box>

        <Box>
          <RegisterNewAccountForm {...registerNewAccountFormProps} />
        </Box>
      </Box>
      <Snackbar
        open={displaySnackbar}
        autoHideDuration={3000}
        onClose={closeSnackbar}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      >
        <Alert severity="error">
          <AlertTitle>Error</AlertTitle>
          {alertMessage}
        </Alert>
      </Snackbar>
    </>
  );
};

export default AuthFeatureRegisterNewAccount;
