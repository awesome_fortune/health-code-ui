import React from 'react';
import { render } from '@testing-library/react';

import AuthFeatureRegisterNewAccount from './auth-feature-register-new-account';

describe(' AuthFeatureRegisterNewAccount', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<AuthFeatureRegisterNewAccount />);
    expect(baseElement).toBeTruthy();
  });
});
