import React, { lazy, Suspense, useEffect, useState } from 'react';
import { Route, Redirect, useHistory } from 'react-router-dom';
import { authBasePath, authRoutes, AuthStateTypes, useAuth } from '@health-code-ui/auth/state';
import { Alert, AlertTitle, Color } from '@material-ui/lab';
import { Snackbar } from '@material-ui/core';
import { quicklinksBasePath } from '@health-code-ui/quicklinks/state';

const AuthFeatureLogin = lazy(() => import('libs/auth/feature-login/src/lib/auth-feature-login'));
const AuthFeatureForgotPassword = lazy(() =>
  import('libs/auth/feature-forgot-password/src/lib/auth-feature-forgot-password')
);
const AuthFeatureVerifyOtp = lazy(() => import('libs/auth/feature-verify-otp/src/lib/auth-feature-verify-otp'));
const AuthFeatureRegisterNewAccount = lazy(() =>
  import('libs/auth/feature-register-new-account/src/lib/auth-feature-register-new-account')
);

export const AuthShellAuthEndUser = () => {
  const { state } = useAuth();
  const [displaySnackbar, setDisplaySnackbar] = useState<boolean>(false);
  const [alertMessage, setAlertMessage] = useState<string>(null);
  const [alertSeverity, setAlertSeverity] = useState<Color>(null);
  const [alertTitle, setAlertTitle] = useState<string>(null);
  const history = useHistory();

  useEffect(() => {
    switch (state.authStateType) {
      case AuthStateTypes.LoggedIn:
        history.push(quicklinksBasePath);
        break;

      case AuthStateTypes.RegisterNewAccountSuccess:
        history.push(authRoutes.login.path);
        setAlertTitle('Success');
        setAlertSeverity('success');
        setAlertMessage('Account registration successful');
        setDisplaySnackbar(true);
        break;
    }
  }, [state.authStateType]);

  const closeSnackbar = (event?: React.SyntheticEvent, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }

    setDisplaySnackbar(false);
  };

  return (
    <Suspense fallback={<div>Loading...</div>}>
      <Route exact path={authBasePath}>
        <Redirect to={authRoutes.login.path} />
      </Route>
      <Route path={authRoutes.login.path} component={AuthFeatureLogin} />
      <Route path={authRoutes.forgotPassword.path} component={AuthFeatureForgotPassword} />
      <Route path={authRoutes.verifyOtp.path} component={AuthFeatureVerifyOtp} />
      <Route path={authRoutes.registerNewAccount.path} component={AuthFeatureRegisterNewAccount} />
      <Snackbar
        open={displaySnackbar}
        autoHideDuration={3000}
        onClose={closeSnackbar}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      >
        <Alert severity={alertSeverity}>
          <AlertTitle>{alertTitle}</AlertTitle>
          {alertMessage}
        </Alert>
      </Snackbar>
    </Suspense>
  );
};
