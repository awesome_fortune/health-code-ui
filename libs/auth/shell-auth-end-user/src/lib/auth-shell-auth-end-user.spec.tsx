import React from 'react';
import { render } from '@testing-library/react';

import AuthShellAuthEndUser from './auth-shell-auth-end-user';

describe(' AuthShellAuthEndUser', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<AuthShellAuthEndUser />);
    expect(baseElement).toBeTruthy();
  });
});
