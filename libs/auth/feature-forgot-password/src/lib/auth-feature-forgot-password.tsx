import React from 'react';
import { cssVariables } from '@health-code-ui/shared/ui';
import backgroundImage from '../../../../shared/assets/src/lib/images/antibiotic-capsules.png';
import { ForgotPasswordFormModel } from '@health-code-ui/shared/models';
import { Box, Typography, makeStyles } from '@material-ui/core';
import { ForgotPasswordFormProps, ForgotPasswordForm } from '@health-code-ui/auth/ui-components';

const useStyles = makeStyles({
  root: {
    backgroundImage: `url(${backgroundImage})`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    overflow: 'scroll',
  },
  primaryTypography: {
    marginTop: 30,
    fontSize: 36,
    fontWeight: 300,
    color: cssVariables.colors.tahitiGold,
    marginBottom: 50,
  },
});

export const AuthFeatureForgotPassword = () => {
  const classes = useStyles();

  const submitForgotPasswordForm = (data: ForgotPasswordFormModel) => {
    // Integrate with API
  };

  const forgotPasswordFormProps: ForgotPasswordFormProps = {
    submitForgotPasswordForm,
  };

  return (
    <Box display="flex" flexDirection="column" width="100%" height="100%" padding="60px 200px" className={classes.root}>
      <Typography variant="h3" className={classes.primaryTypography}>
        Welcome to <br /> Code 4 Hire H-Code
      </Typography>

      <ForgotPasswordForm {...forgotPasswordFormProps} />
    </Box>
  );
};

export default AuthFeatureForgotPassword;
