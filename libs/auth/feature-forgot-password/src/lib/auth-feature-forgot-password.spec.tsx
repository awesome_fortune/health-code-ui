import React from 'react';
import { render } from '@testing-library/react';

import AuthFeatureForgotPassword from './auth-feature-forgot-password';

describe(' AuthFeatureForgotPassword', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<AuthFeatureForgotPassword />);
    expect(baseElement).toBeTruthy();
  });
});
