export const authBasePath = '/auth';
export const authRoutes = {
  login: {
    path: `${authBasePath}/login`,
  },
  forgotPassword: {
    path: `${authBasePath}/forgot-password`,
  },
  verifyOtp: {
    path: `${authBasePath}/verify-otp`,
  },
  registerNewAccount: {
    path: `${authBasePath}/register-new-account`,
  },
};
