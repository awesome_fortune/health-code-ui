import { AuthState, AuthStateTypes } from '../context/auth-context';

export enum AuthUtilActionsTypes {
  SetAuthLoading = 'SetAuthLoading',
  ClearAuthErrors = 'ClearAuthErrors',
  SetAuthStateType = 'SetAuthStateType',
  Logout = 'Logout',
  LoadAuthState = 'LoadAuthState',
}

export type AuthUtilActions =
  | { type: AuthUtilActionsTypes.SetAuthLoading; payload: boolean }
  | { type: AuthUtilActionsTypes.ClearAuthErrors }
  | { type: AuthUtilActionsTypes.SetAuthStateType; payload: AuthStateTypes }
  | { type: AuthUtilActionsTypes.Logout }
  | { type: AuthUtilActionsTypes.LoadAuthState; payload: AuthState };
