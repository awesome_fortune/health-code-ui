import { VerifyOtpFormModel } from '@health-code-ui/shared/models';

export enum RequestOtpActionTypes {
  RequestOtp = 'RequestOtp',
  RequestOtpError = 'RequestOtpError',
  RequestOtpSuccess = 'RequestOtpSuccess',
}

export type RequestOtpActions =
  | { type: RequestOtpActionTypes.RequestOtp; payload: VerifyOtpFormModel }
  | { type: RequestOtpActionTypes.RequestOtpError; payload: any }
  | { type: RequestOtpActionTypes.RequestOtpSuccess; payload; any };
