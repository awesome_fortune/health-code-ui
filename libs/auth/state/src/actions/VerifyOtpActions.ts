import { VerifyOtpFormModel } from '@health-code-ui/shared/models';

export enum VerifyOtpActionTypes {
  VerifyOtp = 'VerifyOtp',
  VerifyOtpError = 'VerifyOtpError',
  VerifyOtpSuccess = 'VerifyOtpSuccess',
}

export type VerifyOtpActions =
  | { type: VerifyOtpActionTypes.VerifyOtp; payload: VerifyOtpFormModel }
  | { type: VerifyOtpActionTypes.VerifyOtpError; payload: any }
  | { type: VerifyOtpActionTypes.VerifyOtpSuccess; payload; any };
