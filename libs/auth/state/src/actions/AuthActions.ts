import { AuthUtilActions } from './AuthUtilActions';
import { ForgotPasswordActions } from './ForgotPasswordActions';
import { LoginActions } from './LoginActions';
import { RegisterNewAccountActions } from './RegisterNewAccountActions';
import { RequestOtpActions } from './RequestOtpActions';
import { VerifyOtpActions } from './VerifyOtpActions';

export type AuthActions =
  | LoginActions
  | AuthUtilActions
  | RegisterNewAccountActions
  | ForgotPasswordActions
  | RequestOtpActions
  | VerifyOtpActions;
