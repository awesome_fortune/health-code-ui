import { LoginFormModel } from '@health-code-ui/shared/models';

export enum LoginActionTypes {
  Login = 'Login',
  LoginError = 'LoginError',
  LoginSuccess = 'LoginSuccess',
}

export type LoginActions =
  | { type: LoginActionTypes.Login; payload: LoginFormModel }
  | { type: LoginActionTypes.LoginSuccess; payload: any }
  | { type: LoginActionTypes.LoginError; payload: any };
