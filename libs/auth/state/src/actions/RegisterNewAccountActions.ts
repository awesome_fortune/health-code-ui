import { RegisterNewAccountFormModel } from '@health-code-ui/shared/models';

export enum RegisterNewAccountActionTypes {
  RegisterNewAccount = 'RegisterNewAccount',
  RegisterNewAccountError = 'RegisterNewAccountError',
  RegisterNewAccountSuccess = 'RegisterNewAccountSuccess',
}

export type RegisterNewAccountActions =
  | { type: RegisterNewAccountActionTypes.RegisterNewAccount; payload: RegisterNewAccountFormModel }
  | { type: RegisterNewAccountActionTypes.RegisterNewAccountError; payload: any }
  | { type: RegisterNewAccountActionTypes.RegisterNewAccountSuccess; payload: any };
