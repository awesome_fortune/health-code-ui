import { ForgotPasswordFormModel } from '@health-code-ui/shared/models';

export enum ForgotPasswordActionTypes {
  ForgotPassword = 'ForgotPassword',
  ForgotPasswordError = 'ForgotPasswordError',
  ForgotPasswordSuccess = 'ForgotPasswordSuccess',
}

export type ForgotPasswordActions =
  | { type: ForgotPasswordActionTypes.ForgotPassword; payload: ForgotPasswordFormModel }
  | { type: ForgotPasswordActionTypes.ForgotPasswordError; payload: any }
  | { type: ForgotPasswordActionTypes.ForgotPasswordSuccess; payload; any };
