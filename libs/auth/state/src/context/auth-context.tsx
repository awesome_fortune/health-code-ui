import React, { createContext, useContext, useReducer } from 'react';
import { LoginActionTypes } from '../actions/LoginActions';
import { LoginFormModel, RegisterNewAccountFormModel, VerifyOtpFormModel } from '@health-code-ui/shared/models';
import { RegisterNewAccountActionTypes } from '../actions/RegisterNewAccountActions';
import { AuthUtilActionsTypes } from '../actions/AuthUtilActions';
import { StorageKeys, StorageService, StorageType } from '@health-code-ui/shared/util';
import { AuthActions } from '../actions/AuthActions';
import { ForgotPasswordActionTypes } from '../actions/ForgotPasswordActions';
import { RequestOtpActionTypes } from '../actions/RequestOtpActions';
import { VerifyOtpActionTypes } from '../actions/VerifyOtpActions';
import * as authApiClient from '@health-code-ui/auth/api-client';
import jwt_decode from 'jwt-decode';

export interface AuthState {
  isLoading: boolean;
  user: any;
  error: any;
  authStateType: AuthStateTypes;
}

export enum AuthStateTypes {
  LoggedIn = 'LoggedIn',
  LoggedOut = 'LoggedOut',
  RegisterNewAccountSuccess = 'RegisterNewAccountSuccess',
}

const initialState: AuthState = StorageService.getItem(StorageKeys.AuthState, StorageType.SessionStorage) ?? {
  isLoading: false,
  user: null,
  error: null,
  authStateType: AuthStateTypes.LoggedOut,
};

const AuthContext = createContext({ state: initialState, actions: null });

const useActions = (dispatch) => ({
  login: (data: LoginFormModel) => dispatch({ type: LoginActionTypes.Login, payload: data }),
  registerNewAccount: (data: RegisterNewAccountFormModel) =>
    dispatch({ type: RegisterNewAccountActionTypes.RegisterNewAccount, payload: data }),
  forgotPassword: (data: VerifyOtpFormModel) =>
    dispatch({ type: ForgotPasswordActionTypes.ForgotPassword, payload: data }),
  requestOtp: (data: VerifyOtpFormModel) => dispatch({ type: RequestOtpActionTypes.RequestOtp, payload: data }),
  verifyOtp: (data: VerifyOtpFormModel) => dispatch({ type: VerifyOtpActionTypes.VerifyOtp, payload: data }),
  logout: () => dispatch({ type: AuthUtilActionsTypes.Logout }),
  loadState: (data: AuthState) => dispatch({ type: AuthUtilActionsTypes.LoadAuthState, payload: data }),
});

const applySideEffectsMiddleware = (dispatch) => async (action: AuthActions) => {
  dispatch({ type: AuthUtilActionsTypes.ClearAuthErrors });
  dispatch({ type: AuthUtilActionsTypes.SetAuthLoading, payload: true });

  switch (action.type) {
    case LoginActionTypes.Login:
      try {
        StorageService.deleteItem(StorageKeys.AuthState, StorageType.SessionStorage);
        const response = await authApiClient.login(action.payload);

        dispatch({ type: LoginActionTypes.LoginSuccess, payload: response });
      } catch (error) {
        dispatch({ type: LoginActionTypes.LoginError, payload: error });
      }
      break;
    case RegisterNewAccountActionTypes.RegisterNewAccount:
      try {
        const response = await authApiClient.registerByEmail(action.payload);

        dispatch({ type: RegisterNewAccountActionTypes.RegisterNewAccountSuccess, payload: response });
      } catch (error) {
        dispatch({ type: RegisterNewAccountActionTypes.RegisterNewAccountError, payload: error.errors[0] });
      }
      break;
    default:
      dispatch(action);
      break;
  }
};

export const useAuth = () => {
  const context = useContext(AuthContext);

  if (!context) {
    throw new Error('useAuth must be used within an AuthProvider');
  }

  return context;
};

const authReducer = (state: AuthState, action: AuthActions) => {
  switch (action.type) {
    case AuthUtilActionsTypes.LoadAuthState:
      state = action.payload;

      return {
        ...state,
      };
    case AuthUtilActionsTypes.Logout:
      StorageService.clearStorage(StorageType.SessionStorage);
      location.reload();

      return {
        ...initialState,
        authStateType: AuthStateTypes.LoggedOut,
      };
    case LoginActionTypes.LoginSuccess:
      const { jwttoken } = action.payload;
      const decodedToken = jwt_decode(jwttoken);

      // @TODO: apend role from tokey to user object?

      const newState = {
        ...state,
        user: action.payload,
        isLoading: false,
        authStateType: AuthStateTypes.LoggedIn,
      };

      StorageService.setItem(StorageKeys.AuthState, newState, StorageType.SessionStorage);

      return newState;
    case AuthUtilActionsTypes.SetAuthLoading:
      return {
        ...state,
        isLoading: action.payload,
      };
    case AuthUtilActionsTypes.ClearAuthErrors:
      return {
        ...state,
        error: null,
      };
    case RegisterNewAccountActionTypes.RegisterNewAccountSuccess:
      return {
        ...state,
        isLoading: false,
        authStateType: AuthStateTypes.RegisterNewAccountSuccess,
      };
    case AuthUtilActionsTypes.SetAuthStateType:
      return {
        ...state,
        authStateType: action.payload,
      };
    case LoginActionTypes.LoginError:
    case RegisterNewAccountActionTypes.RegisterNewAccountError:
      return {
        ...state,
        error: action.payload,
        isLoading: false,
      };
    default: {
      throw new Error(`Unsupported action type: ${action.type}`);
    }
  }
};

export const AuthProvider = (props) => {
  const [state, dispatch] = useReducer(authReducer, initialState);
  const actions = useActions(applySideEffectsMiddleware(dispatch));

  return <AuthContext.Provider value={{ state, actions }} {...props} />;
};
