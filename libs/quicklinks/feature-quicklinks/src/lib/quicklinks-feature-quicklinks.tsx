import React from 'react';
import { cssVariables, DashboardCard, DashboardCardProps } from '@health-code-ui/shared/ui';
import { Box, Typography, makeStyles } from '@material-ui/core';
import bgImage from '../../../../shared/assets/src/lib/images/doctor_bg.png';
import { useAuth } from '@health-code-ui/auth/state';
import { format } from 'date-fns';

export interface QuicklinksFeatureQuicklinksProps {
  quickLinks: DashboardCardProps[];
}

const useStyles = makeStyles({
  container: {
    backgroundImage: `url(${bgImage})`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
  },
  pageTitle: {
    color: cssVariables.colors.white,
    fontWeight: 300,
    lineHeight: '60px',
    letterSpacing: '0.3px',
    fontSize: 48,
  },
  pageSubTitle: {
    color: cssVariables.colors.white,
    fontWeight: 300,
    lineHeight: '38px',
    letterSpacing: '0.3px',
    fontSize: 30,
  },
});

export const QuicklinksFeatureQuicklinks = (props: QuicklinksFeatureQuicklinksProps) => {
  const classes = useStyles();
  const { state } = useAuth();
  const { quickLinks } = props;

  const partOfDay = () => {
    const hour = Number.parseInt(format(new Date(), 'kk'), 10);
    return hour <= 11 ? 'morning' : hour > 11 && hour <= 18 ? 'afternoon' : 'evening';
  };

  const greeting = () => {
    return `Good ${partOfDay()} ${state.user.firstName} ${state.user.surname}`;
  };

  return (
    <Box className={classes.container} height="100%" paddingTop="70px" paddingLeft="80px" paddingRight="80px">
      <Typography variant="h4" className={classes.pageTitle}>
        {greeting()}
      </Typography>
      <Typography variant="h6" className={classes.pageSubTitle}>
        What would you like to do?
      </Typography>

      <Box display="flex" justifyContent="center" flexWrap="wrap">
        {quickLinks.map((link, index) => (
          <Box margin="30px" key={index}>
            <DashboardCard
              icon={link.icon}
              title={link.title}
              onCardClick={link.onCardClick}
              disabled={link.disabled}
            />
          </Box>
        ))}
      </Box>
    </Box>
  );
};
