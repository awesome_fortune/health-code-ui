import React from 'react';
import { render } from '@testing-library/react';

import QuicklinksFeatureQuicklinks from './quicklinks-feature-quicklinks';

describe('QuicklinksFeatureQuicklinks', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<QuicklinksFeatureQuicklinks />);
    expect(baseElement).toBeTruthy();
  });
});
