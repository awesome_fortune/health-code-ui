import React from 'react';
import { render } from '@testing-library/react';

import QuicklinksShellQuicklinksEndUser from './quicklinks-shell-quicklinks-end-user';

describe('QuicklinksShellQuicklinksEndUser', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<QuicklinksShellQuicklinksEndUser />);
    expect(baseElement).toBeTruthy();
  });
});
