import React from 'react';
import { useHistory } from 'react-router-dom';
import { DashboardCardProps } from '@health-code-ui/shared/ui';
import { faCalendarCheck } from '@fortawesome/free-solid-svg-icons';
import { QuicklinksFeatureQuicklinks } from '@health-code-ui/quicklinks/feature-quicklinks';
import { appointmentsRoutes } from '@health-code-ui/appointments/state';

export const QuicklinksShellQuicklinksEndUser = () => {
  const history = useHistory();
  const quickLinks: DashboardCardProps[] = [
    {
      title: 'Create Appointment',
      icon: faCalendarCheck,
      disabled: false,
      onCardClick: () => history.push(appointmentsRoutes.createAppointment.path),
    },
    {
      title: 'Manage Appointments',
      icon: faCalendarCheck,
      disabled: false,
      onCardClick: () => history.push(appointmentsRoutes.manageAppointments.path),
    },
  ];

  return <QuicklinksFeatureQuicklinks quickLinks={quickLinks} />;
};

export default QuicklinksShellQuicklinksEndUser;
