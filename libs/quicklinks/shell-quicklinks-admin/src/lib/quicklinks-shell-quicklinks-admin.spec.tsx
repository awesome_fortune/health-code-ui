import React from 'react';
import { render } from '@testing-library/react';

import QuicklinksShellQuicklinksAdmin from './quicklinks-shell-quicklinks-admin';

describe('QuicklinksShellQuicklinksAdmin', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<QuicklinksShellQuicklinksAdmin />);
    expect(baseElement).toBeTruthy();
  });
});
