import React from 'react';
import { DashboardCardProps } from '@health-code-ui/shared/ui';
import { faCalendarCheck } from '@fortawesome/free-solid-svg-icons';
import { QuicklinksFeatureQuicklinks } from '@health-code-ui/quicklinks/feature-quicklinks';
import { useHistory } from 'react-router-dom';
import { appointmentsRoutes } from '@health-code-ui/appointments/state';
import { patientsRoutes } from '@health-code-ui/patients/state';
import { waitingRoomBasePath } from '@health-code-ui/waiting-room/state';

export const QuicklinksShellQuicklinksAdmin = () => {
  const history = useHistory();
  const quickLinks: DashboardCardProps[] = [
    {
      title: 'Create patient file',
      icon: faCalendarCheck,
      disabled: false,
      onCardClick: () => history.push(patientsRoutes.createPatientFile.path),
    },
    {
      title: 'Create Appointment',
      icon: faCalendarCheck,
      disabled: false,
      onCardClick: () => history.push(appointmentsRoutes.createAppointment.path),
    },
    {
      title: 'Manage Appointments',
      icon: faCalendarCheck,
      disabled: false,
      onCardClick: () => history.push(appointmentsRoutes.manageAppointments.path),
    },
    {
      title: 'See Waiting Room',
      icon: faCalendarCheck,
      disabled: false,
      onCardClick: () => history.push(waitingRoomBasePath),
    },
    {
      title: 'See Reports',
      icon: faCalendarCheck,
      disabled: true,
      onCardClick: () => null,
    },
    {
      title: 'See Billing/Claims',
      icon: faCalendarCheck,
      disabled: true,
      onCardClick: () => null,
    },
    {
      title: 'Dispensary',
      icon: faCalendarCheck,
      disabled: true,
      onCardClick: () => null,
    },
  ];

  return <QuicklinksFeatureQuicklinks quickLinks={quickLinks} />;
};

export default QuicklinksShellQuicklinksAdmin;
