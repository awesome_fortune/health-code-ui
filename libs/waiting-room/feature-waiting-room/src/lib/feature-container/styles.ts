import { makeStyles } from '@material-ui/core';
import bgImage from '../../../../../shared/assets/src/lib/images/abstract_bg.jpg';
import { cssVariables } from '@health-code-ui/shared/ui';

export const useStyles = makeStyles({
  heroSection: {
    backgroundImage: `url(${bgImage})`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
  },
  heroTitle: {
    color: cssVariables.colors.white,
    fontWeight: 300,
  },
  heroSubTitle: {
    color: cssVariables.colors.white,
    fontWeight: 300,
  },
  iconButton: {
    color: cssVariables.colors.white,
  },
  iconButtonText: {
    color: cssVariables.colors.white,
    fontWeight: 400,
  },
  title: {
    fontWeight: 300,
    color: cssVariables.colors.black,
    marginTop: 20,
    marginBottom: 20,
  },
});
