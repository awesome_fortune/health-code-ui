import React from 'react';
import { Box, Button, IconButton, TextField, Typography } from '@material-ui/core';
import WallpaperIcon from '@material-ui/icons/Wallpaper';
import { useActionButtonStyles } from '@health-code-ui/shared/ui';
import { WaitingRoomTable } from '../components/waiting-room-table';
import { useStyles } from './styles';

export const WaitingRoomFeatureContainer = () => {
  const buttonClasses = useActionButtonStyles();
  const classes = useStyles();

  return (
    <>
      <Box
        className={classes.heroSection}
        paddingLeft="80px"
        paddingRight="80px"
        height="300px"
        display="flex"
        alignItems="center"
        justifyContent="space-between"
      >
        <Box>
          <Typography className={classes.heroTitle} variant="h4">
            Waiting Room
          </Typography>
          <Typography className={classes.heroSubTitle} variant="h6">
            Here are the patients in the Waiting Room
          </Typography>
        </Box>
        <Box display="flex" alignItems="center">
          <IconButton className={classes.iconButton} aria-label="Change backround">
            <WallpaperIcon />
          </IconButton>
          <Typography className={classes.iconButtonText}>Change wallpaper</Typography>
        </Box>
      </Box>

      <Box paddingLeft="80px" paddingRight="80px">
        <Typography className={classes.title} variant="h5">
          Patients in waiting room
        </Typography>
        <Box display="flex">
          <TextField variant="outlined" fullWidth placeholder="Search by Patient Name" style={{ marginRight: '1em' }} />
          <Button
            variant="outlined"
            classes={{
              root: buttonClasses.root,
              containedPrimary: buttonClasses.containedPrimary,
              label: buttonClasses.label,
            }}
          >
            Search
          </Button>
        </Box>

        <WaitingRoomTable />
      </Box>
    </>
  );
};
