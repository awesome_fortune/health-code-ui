import React from 'react';
import { render } from '@testing-library/react';

import WaitingRoomFeatureWaitingRoom from './waiting-room-feature-waiting-room';

describe('WaitingRoomFeatureWaitingRoom', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<WaitingRoomFeatureWaitingRoom />);
    expect(baseElement).toBeTruthy();
  });
});
