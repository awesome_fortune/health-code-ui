import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
  table: {
    borderCollapse: 'separate',
    borderSpacing: '0 20px',
  },
});
