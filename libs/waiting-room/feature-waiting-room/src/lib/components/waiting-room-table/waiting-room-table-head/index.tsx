import React from 'react';
import { createStyles, TableCell, TableHead, TableRow, Theme, withStyles } from '@material-ui/core';

const StyledTableCell = withStyles((theme: Theme) =>
  createStyles({
    root: {
      borderBottom: 0,
    },
  })
)(TableCell);

export const WaitingRoomTableHead = () => {
  return (
    <TableHead>
      <TableRow>
        <StyledTableCell></StyledTableCell>
        <StyledTableCell>Name and Surname</StyledTableCell>
        <StyledTableCell>Date</StyledTableCell>
        <StyledTableCell></StyledTableCell>
        <StyledTableCell></StyledTableCell>
      </TableRow>
    </TableHead>
  );
};
