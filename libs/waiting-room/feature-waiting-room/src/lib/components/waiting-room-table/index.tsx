import React from 'react';
import { Box, Table, TableBody, TableContainer } from '@material-ui/core';
import { WaitingRoomTableHead } from './waiting-room-table-head';
import { addHours } from 'date-fns/esm';
import { WaitingRoomTableRow } from './waiting-room-table-row';
import { useStyles } from './styles';

const patients = [
  {
    id: 1,
    name: 'Kumbi Mtshakazi',
    date: addHours(new Date(), 2),
  },
  {
    id: 2,
    name: 'Kumbi Mtshakazi',
    date: addHours(new Date(), 3),
  },
];

export const WaitingRoomTable = () => {
  const classes = useStyles();

  return (
    <TableContainer component={Box}>
      <Table aria-label="Waiting room table" className={classes.table}>
        <WaitingRoomTableHead />
        <TableBody>
          {patients.map((patient) => (
            <WaitingRoomTableRow key={patient.id} {...patient} />
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};
