import { makeStyles } from '@material-ui/core';
import { cssVariables } from '@health-code-ui/shared/ui';

export const useStyles = makeStyles({
  viewPatientButtonRoot: {
    backgroundColor: cssVariables.colors.white,
    border: `1px solid ${cssVariables.colors.tahitiGold}`,
    boxShadow: 'none',
    '&:hover': {
      backgroundColor: cssVariables.colors.tahitiGold,
    },
  },
  viewPatientButtonLabel: {
    color: cssVariables.colors.black,
    fontSize: 18,
    letterSpacing: '0.23px',
    fontWeight: 300,
    '&:hover': {
      color: cssVariables.colors.white,
    },
  },
});
