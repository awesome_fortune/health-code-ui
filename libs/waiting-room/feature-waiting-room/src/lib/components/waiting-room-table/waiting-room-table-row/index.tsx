import React from 'react';
import {
  Box,
  createStyles,
  Fab,
  IconButton,
  TableCell,
  TableRow,
  Theme,
  Typography,
  withStyles,
} from '@material-ui/core';
import { cssVariables } from '@health-code-ui/shared/ui';
import { format } from 'date-fns';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import { useStyles } from './styles';
import { useHistory } from 'react-router-dom';
import { consultationBasePath } from '@health-code-ui/consultation/state';

const StyledTableRow = withStyles((theme: Theme) =>
  createStyles({
    root: {
      backgroundColor: cssVariables.colors.white,
      boxShadow: `0px 2px 20px ${cssVariables.colors.blackAlpha2}`,
      borderRadius: '26px',
    },
  })
)(TableRow);

const StyledTableCell = withStyles((theme: Theme) =>
  createStyles({
    root: {
      borderBottom: 0,
    },
  })
)(TableCell);

export const WaitingRoomTableRow = (props) => {
  const classes = useStyles();
  const { name, date } = props;
  const history = useHistory();

  return (
    <StyledTableRow>
      <StyledTableCell style={{ borderTopLeftRadius: 26, borderBottomLeftRadius: 26 }}>
        <Fab classes={{ root: classes.viewPatientButtonRoot, label: classes.viewPatientButtonLabel }} color="primary">
          View
        </Fab>
      </StyledTableCell>
      <StyledTableCell>{name}</StyledTableCell>
      <StyledTableCell>{format(date, 'd/M/yy')}</StyledTableCell>
      <StyledTableCell>
        <Box display="flex" flexDirection="column" alignItems="center">
          <IconButton onClick={() => history.push(consultationBasePath)}>
            <PlayArrowIcon />
          </IconButton>
          <Typography variant="body1">Start Consultation</Typography>
        </Box>
      </StyledTableCell>
      <StyledTableCell>
        <Box display="flex" flexDirection="column" alignItems="center">
          <IconButton>
            <DeleteForeverIcon />
          </IconButton>
          <Typography variant="body1">Remove Patient</Typography>
        </Box>
      </StyledTableCell>
    </StyledTableRow>
  );
};
