import { WaitingRoomProvider } from '@health-code-ui/waiting-room/state';
import React from 'react';
import { WaitingRoomFeatureContainer } from './feature-container';

export const WaitingRoomFeatureWaitingRoom = () => {
  return (
    <WaitingRoomProvider>
      <WaitingRoomFeatureContainer />
    </WaitingRoomProvider>
  );
};

export default WaitingRoomFeatureWaitingRoom;
