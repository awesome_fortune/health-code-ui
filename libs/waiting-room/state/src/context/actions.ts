export enum WaitingRoomActionTypes {
  StartConsultation = 'StartConsultation',
}

export type WaitingRoomActions = { type: WaitingRoomActionTypes.StartConsultation };
