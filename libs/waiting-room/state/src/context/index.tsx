import React, { createContext, useContext, useReducer } from 'react';
import { WaitingRoomActions, WaitingRoomActionTypes } from './actions';

export interface WaitingRoomState {}

const initialState: WaitingRoomState = {};

const WaitingRoomContext = createContext<any>({ state: initialState, actions: null });

const useActions = (dispatch) => ({});

const applySideEffectsMiddleware = (dispatch) => async (action: WaitingRoomActions) => {
  switch (action.type) {
    default:
      dispatch(action);
      break;
  }
};

const waitingRoomReducer = (state: WaitingRoomState, action: WaitingRoomActions) => {
  switch (action.type) {
    case WaitingRoomActionTypes.StartConsultation:
      return {
        ...state,
      };

    default:
      throw new Error(`Unsupported action type ${action.type}`);
  }
};

export const useWaitingRoom = () => {
  const context = useContext(WaitingRoomContext);

  if (!context) {
    throw new Error('useWaitingRoom must be used within a WaitingRoomProvider');
  }

  return context;
};

export const WaitingRoomProvider = (props) => {
  const [state, dispatch] = useReducer(waitingRoomReducer, initialState);
  const actions = useActions(applySideEffectsMiddleware(dispatch));

  return <WaitingRoomContext.Provider value={{ state, actions }} {...props} />;
};
