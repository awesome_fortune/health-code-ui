import React, { lazy, Suspense } from 'react';
import { Route } from 'react-router-dom';
import { waitingRoomBasePath } from '@health-code-ui/waiting-room/state';

const WaitingRoomFeatureWaitingRoom = lazy(() =>
  import('libs/waiting-room/feature-waiting-room/src/lib/waiting-room-feature-waiting-room')
);

export const WaitingRoomShellWaitingRoomAdmin = () => {
  return (
    <Suspense fallback={<div>Loading...</div>}>
      <Route path={waitingRoomBasePath} component={WaitingRoomFeatureWaitingRoom} />
    </Suspense>
  );
};

export default WaitingRoomShellWaitingRoomAdmin;
