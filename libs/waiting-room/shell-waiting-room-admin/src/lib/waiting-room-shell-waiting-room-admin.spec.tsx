import React from 'react';
import { render } from '@testing-library/react';

import WaitingRoomShellWaitingRoomAdmin from './waiting-room-shell-waiting-room-admin';

describe('WaitingRoomShellWaitingRoomAdmin', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<WaitingRoomShellWaitingRoomAdmin />);
    expect(baseElement).toBeTruthy();
  });
});
