import React from 'react';
import { render } from '@testing-library/react';

import ConsultationShellConsultationAdmin from './consultation-shell-consultation-admin';

describe('ConsultationShellConsultationAdmin', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<ConsultationShellConsultationAdmin />);
    expect(baseElement).toBeTruthy();
  });
});
