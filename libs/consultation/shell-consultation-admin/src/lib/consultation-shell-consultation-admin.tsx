import React, { lazy, Suspense } from 'react';
import { Route } from 'react-router-dom';
import { consultationBasePath } from '@health-code-ui/consultation/state';

const ConsultationFeatureConsultation = lazy(() =>
  import('libs/consultation/feature-consultation/src/lib/waiting-room-feature-consultation')
);

export const ConsultationShellConsultationAdmin = () => {
  return (
    <Suspense fallback={<div>Loading...</div>}>
      <Route path={consultationBasePath} component={ConsultationFeatureConsultation} />
    </Suspense>
  );
};

export default ConsultationShellConsultationAdmin;
