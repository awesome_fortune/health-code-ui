import {
  httpClientBase,
  HttpClientMethods,
  StorageKeys,
  StorageService,
  StorageType,
} from '@health-code-ui/shared/util';

const baseUrl = '/api/consultations';
const authState = StorageService.getItem(StorageKeys.AuthState, StorageType.SessionStorage);

export const createConsultation = (body: any) =>
  httpClientBase(baseUrl, { body, token: authState.user.jwttoken, method: HttpClientMethods.POST });

export const getConsultationById = (consultationId: string) =>
  httpClientBase(`${baseUrl}/${consultationId}`, { token: authState.user.jwttoken });

export const getConsultationDurations = () =>
  httpClientBase(`${baseUrl}/durations`, { token: authState.user.jwttoken });

export const getConsultationLanguages = () =>
  httpClientBase(`${baseUrl}/languages`, { token: authState.user.jwttoken });

export const getConsultationNoteTypes = () =>
  httpClientBase(`${baseUrl}/noteTypes`, { token: authState.user.jwttoken });

export const getConsultationVisitTypes = () =>
  httpClientBase(`${baseUrl}/visitTypes`, { token: authState.user.jwttoken });
