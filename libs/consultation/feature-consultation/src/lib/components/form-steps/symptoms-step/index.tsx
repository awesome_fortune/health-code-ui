import React from 'react';
import { useConsultation } from '../../../context';
import { FormProvider, useForm } from 'react-hook-form';
import { VisitTypeFormSection } from './form-sections/visit-type-form-section';
import { PatientComplaintsFormSection } from './form-sections/patient-complaints-form-section';
import { Box, Button } from '@material-ui/core';
import { useActionButtonStyles } from '@health-code-ui/shared/ui';
import { InformationOverviewFormSection } from '../../information-overview-form-section';

export const SymptomsStep = () => {
  const { actions, state } = useConsultation();
  const symptomsForm = useForm({ mode: 'onBlur' });
  const buttonClasses = useActionButtonStyles();

  const onSubmit = (data) => {
    if (symptomsForm.formState.isValid) {
      actions.goToNextStep();
    }
  };

  return (
    <FormProvider {...symptomsForm}>
      <form noValidate autoComplete="off" onSubmit={symptomsForm.handleSubmit(onSubmit)}>
        <VisitTypeFormSection />
        <Box display="flex" width="85%">
          <PatientComplaintsFormSection />
          <InformationOverviewFormSection />
        </Box>
        <Button
          type="submit"
          classes={{
            root: buttonClasses.root,
            containedPrimary: buttonClasses.containedPrimary,
            label: buttonClasses.label,
          }}
          color="primary"
          variant="contained"
        >
          Next
        </Button>
      </form>
    </FormProvider>
  );
};
