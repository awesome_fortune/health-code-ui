import React from 'react';
import { Box, FormControl, FormControlLabel, FormLabel, Radio, RadioGroup } from '@material-ui/core';
import { Controller } from 'react-hook-form';

export const VisitTypeFormSection = () => {
  return (
    <FormControl component="fieldset">
      <FormLabel component="legend">Visit Type</FormLabel>
      <Controller
        name="visitType"
        id="visitType"
        as={
          <RadioGroup>
            <Box marginTop="10px">
              <FormControlLabel
                label="Follow Up"
                labelPlacement="bottom"
                value="followUp"
                control={<Radio color="primary" />}
              />
              <FormControlLabel
                label="Immunisation"
                labelPlacement="bottom"
                value="immunisation"
                control={<Radio color="primary" />}
              />
              <FormControlLabel
                label="Chronic Follow Up"
                labelPlacement="bottom"
                value="chronicFollowUp"
                control={<Radio color="primary" />}
              />
              <FormControlLabel
                label="Generic"
                labelPlacement="bottom"
                value="genericCheckup"
                control={<Radio color="primary" />}
              />
            </Box>
          </RadioGroup>
        }
      />
    </FormControl>
  );
};
