import React from 'react';
import { Box, FormControl, FormControlLabel, FormGroup, FormLabel } from '@material-ui/core';
import { C4hSwitch } from '@health-code-ui/shared/ui';
import { useStyles } from './styles';

export const PatientComplaintsFormSection = () => {
  const classes = useStyles();

  return (
    <Box marginTop="30px" width="70%">
      <FormControl component="fieldset">
        <FormLabel component="legend">Patient Complaints</FormLabel>
        <FormGroup className={classes.formGroup}>
          <FormControlLabel className={classes.formControlLabel} label="General Consultation" control={<C4hSwitch />} />
          <FormControlLabel className={classes.formControlLabel} label="Abdominal Pains" control={<C4hSwitch />} />
          <FormControlLabel className={classes.formControlLabel} label="Back Pains" control={<C4hSwitch />} />
          <FormControlLabel className={classes.formControlLabel} label="Chest Pains" control={<C4hSwitch />} />
          <FormControlLabel className={classes.formControlLabel} label="Depression" control={<C4hSwitch />} />
          <FormControlLabel className={classes.formControlLabel} label="Ear Infection" control={<C4hSwitch />} />
          <FormControlLabel className={classes.formControlLabel} label="Cold/Flu" control={<C4hSwitch />} />
          <FormControlLabel className={classes.formControlLabel} label="Constipation" control={<C4hSwitch />} />
          <FormControlLabel className={classes.formControlLabel} label="Contraception" control={<C4hSwitch />} />
          <FormControlLabel className={classes.formControlLabel} label="Cough" control={<C4hSwitch />} />
          <FormControlLabel className={classes.formControlLabel} label="Diarrhea" control={<C4hSwitch />} />
          <FormControlLabel className={classes.formControlLabel} label="Fatigue" control={<C4hSwitch />} />
          <FormControlLabel className={classes.formControlLabel} label="Fever" control={<C4hSwitch />} />
          <FormControlLabel className={classes.formControlLabel} label="Gyaena Problem" control={<C4hSwitch />} />
          <FormControlLabel className={classes.formControlLabel} label="Heart burn" control={<C4hSwitch />} />
        </FormGroup>
      </FormControl>
    </Box>
  );
};
