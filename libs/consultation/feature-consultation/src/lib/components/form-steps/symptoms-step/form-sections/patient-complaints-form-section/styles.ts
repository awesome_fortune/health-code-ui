import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
  formGroup: {
    flexDirection: 'row',
  },
  formControlLabel: {
    flexBasis: '30%',
  },
});
