import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
  formGroup: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 20,
    width: '65%',
  },
  textFields: {
    flexBasis: '30%',
  },
});
