import { useActionButtonStyles } from '@health-code-ui/shared/ui';
import {
  Box,
  Button,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormLabel,
  Radio,
  RadioGroup,
  TextField,
} from '@material-ui/core';
import React from 'react';
import { Controller, useForm } from 'react-hook-form';
import { useStyles } from './styles';

export const VitalsStep = () => {
  const classes = useStyles();
  const { register, control } = useForm({ mode: 'onBlur' });
  const buttonClasses = useActionButtonStyles();

  return (
    <form noValidate autoComplete="off">
      <FormControl component="fieldset">
        <FormLabel component="legend">Vitals & Metrics</FormLabel>
        <FormGroup className={classes.formGroup}>
          <Box flexBasis="25%" marginBottom="20px">
            <TextField label="Temperature" variant="outlined" color="primary" />
          </Box>
          <Box flexBasis="25%">
            <TextField label="Pulse rate" variant="outlined" color="primary" />
          </Box>
          <Box flexBasis="25%">
            <TextField label="Blood pressure" variant="outlined" color="primary" />
          </Box>
          <Box flexBasis="25%">
            <TextField label="Respiratory rate" variant="outlined" color="primary" />
          </Box>
          <Box flexBasis="25%">
            <TextField label="SP02" variant="outlined" color="primary" />
          </Box>
          <Box flexBasis="25%">
            <TextField label="Weight" variant="outlined" color="primary" />
          </Box>
          <Box flexBasis="25%">
            <TextField label="Height" variant="outlined" color="primary" />
          </Box>
          <Box flexBasis="25%">
            <TextField label="Waist circumference" variant="outlined" color="primary" />
          </Box>
          <Box flexBasis="25%" marginTop="20px">
            <TextField label="BMI" variant="outlined" color="primary" />
          </Box>
        </FormGroup>
      </FormControl>
      <Box display="flex" flexDirection="column">
        <FormControl component="fieldset">
          <FormLabel component="legend">JVP</FormLabel>
          <Controller
            name="jvp"
            id="jvp"
            control={control}
            as={
              <RadioGroup>
                <Box display="flex">
                  <FormControlLabel
                    label="Normal"
                    labelPlacement="bottom"
                    value="normal"
                    control={<Radio color="primary" />}
                  />
                  <FormControlLabel
                    label="Elevated"
                    labelPlacement="bottom"
                    value="elevated"
                    control={<Radio color="primary" />}
                  />
                </Box>
              </RadioGroup>
            }
          />
        </FormControl>

        <FormControl component="fieldset">
          <FormLabel component="legend">Chest</FormLabel>
          <Controller
            name="chest"
            id="chest"
            control={control}
            as={
              <RadioGroup>
                <Box display="flex">
                  <FormControlLabel
                    label="Normal"
                    labelPlacement="bottom"
                    value="normal"
                    control={<Radio color="primary" />}
                  />
                  <FormControlLabel
                    label="Elevated"
                    labelPlacement="bottom"
                    value="elevated"
                    control={<Radio color="primary" />}
                  />
                </Box>
              </RadioGroup>
            }
          />
        </FormControl>

        <FormControl component="fieldset">
          <FormLabel component="legend">Apex</FormLabel>
          <Controller
            name="apex"
            id="apex"
            control={control}
            as={
              <RadioGroup>
                <Box display="flex">
                  <FormControlLabel
                    label="Normal"
                    labelPlacement="bottom"
                    value="normal"
                    control={<Radio color="primary" />}
                  />
                  <FormControlLabel
                    label="Elevated"
                    labelPlacement="bottom"
                    value="elevated"
                    control={<Radio color="primary" />}
                  />
                </Box>
              </RadioGroup>
            }
          />
        </FormControl>
      </Box>

      <Box display="flex" justifyContent="space-between" width="21%" marginTop="20px">
        <Button
          classes={{
            root: buttonClasses.root,
            containedPrimary: buttonClasses.containedPrimary,
            label: buttonClasses.label,
          }}
          color="primary"
          variant="outlined"
        >
          Back
        </Button>
        <Button
          type="submit"
          classes={{
            root: buttonClasses.root,
            containedPrimary: buttonClasses.containedPrimary,
            label: buttonClasses.label,
          }}
          color="primary"
          variant="contained"
        >
          Next
        </Button>
      </Box>
    </form>
  );
};
