import { C4hSwitch } from '@health-code-ui/shared/ui';
import { Box, FormControl, FormControlLabel, FormGroup, FormLabel, TextField } from '@material-ui/core';
import React from 'react';
import { useStyles } from './styles';

export const SymptomsFormSection = () => {
  const classes = useStyles();

  return (
    <Box marginTop="30px" width="70%" paddingRight="30px">
      <FormControl component="fieldset">
        <FormLabel component="legend">Symptoms</FormLabel>
        <FormGroup className={classes.formGroup}>
          <FormControlLabel className={classes.formControlLabel} label="Fever" control={<C4hSwitch />} />
          <FormControlLabel className={classes.formControlLabel} label="Maylgia" control={<C4hSwitch />} />
          <FormControlLabel className={classes.formControlLabel} label="Shortness of breath" control={<C4hSwitch />} />
          <FormControlLabel className={classes.formControlLabel} label="Headaches" control={<C4hSwitch />} />
          <FormControlLabel className={classes.formControlLabel} label="Nasal congestion" control={<C4hSwitch />} />
          <FormControlLabel className={classes.formControlLabel} label="Soar throat" control={<C4hSwitch />} />
          <FormControlLabel className={classes.formControlLabel} label="Ear ache" control={<C4hSwitch />} />
          <FormControlLabel className={classes.formControlLabel} label="Pain with caughing" control={<C4hSwitch />} />
        </FormGroup>
      </FormControl>
      <FormControl component="fieldset" fullWidth>
        <FormLabel component="legend">Medication Taken Thus Far</FormLabel>
        <TextField variant="outlined" color="primary" />
      </FormControl>
    </Box>
  );
};
