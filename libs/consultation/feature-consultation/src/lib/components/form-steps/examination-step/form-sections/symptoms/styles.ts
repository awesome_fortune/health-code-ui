import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
  formGroup: {
    flexDirection: 'row',
    marginBottom: 20,
  },
  formControlLabel: {
    flexBasis: '30%',
  },
});
