import React from 'react';
import { Box, FormControl, FormControlLabel, FormLabel, Radio, RadioGroup } from '@material-ui/core';
import { Controller } from 'react-hook-form';

export const DurationFormSection = () => {
  return (
    <FormControl component="fieldset">
      <FormLabel component="legend">Duration</FormLabel>
      <Controller
        name="duration"
        id="duration"
        as={
          <RadioGroup>
            <Box marginTop="10px">
              <FormControlLabel label="Days" labelPlacement="bottom" value="days" control={<Radio color="primary" />} />
              <FormControlLabel
                label="Weeks"
                labelPlacement="bottom"
                value="weeks"
                control={<Radio color="primary" />}
              />
              <FormControlLabel
                label="Months"
                labelPlacement="bottom"
                value="months"
                control={<Radio color="primary" />}
              />
              <FormControlLabel
                label="Longer"
                labelPlacement="bottom"
                value="longer"
                control={<Radio color="primary" />}
              />
            </Box>
          </RadioGroup>
        }
      />
    </FormControl>
  );
};
