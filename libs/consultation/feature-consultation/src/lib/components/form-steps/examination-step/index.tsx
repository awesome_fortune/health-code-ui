import { useActionButtonStyles } from '@health-code-ui/shared/ui';
import { Box, Button } from '@material-ui/core';
import React from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useConsultation } from '../../../context';
import { InformationOverviewFormSection } from '../../information-overview-form-section';
import { DurationFormSection } from './form-sections/duration';
import { SymptomsFormSection } from './form-sections/symptoms';

export const ExaminationStep = () => {
  const { actions, state } = useConsultation();
  const examinationForm = useForm({ mode: 'onBlur' });
  const buttonClasses = useActionButtonStyles();

  const onSubmit = (data) => {
    actions.goToNextStep();
  };

  return (
    <FormProvider {...examinationForm}>
      <form noValidate autoComplete="off" onSubmit={examinationForm.handleSubmit(onSubmit)}>
        <DurationFormSection />
        <Box display="flex" width="85%">
          <SymptomsFormSection />
          <InformationOverviewFormSection />
        </Box>
        <Box display="flex" justifyContent="space-between" width="17%" marginTop="20px">
          <Button
            classes={{
              root: buttonClasses.root,
              containedPrimary: buttonClasses.containedPrimary,
              label: buttonClasses.label,
            }}
            color="primary"
            variant="contained"
          >
            Back
          </Button>
          <Button
            type="submit"
            classes={{
              root: buttonClasses.root,
              containedPrimary: buttonClasses.containedPrimary,
              label: buttonClasses.label,
            }}
            color="primary"
            variant="contained"
          >
            Next
          </Button>
        </Box>
      </form>
    </FormProvider>
  );
};
