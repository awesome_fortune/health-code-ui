import React, { useState } from 'react';
import { useStyles } from './styles';
import { useHistory } from 'react-router-dom';
import { useConsultation } from '../../context';
import { waitingRoomBasePath } from '@health-code-ui/waiting-room/state';
import { ConfirmDialog, ConfirmDialogProps } from '@health-code-ui/shared/ui';
import { MedicationsStep } from '../form-steps/medications-step';
import { SymptomsStep } from '../form-steps/symptoms-step';
import { ExaminationStep } from '../form-steps/examination-step';
import { VitalsStep } from '../form-steps/vitals-step';
import { NotesStep } from '../form-steps/notes-step';
import { Box, IconButton, Step, StepLabel, Stepper, Typography } from '@material-ui/core';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';

const getSteps = () => ['Symptoms', 'Examination', 'Vitals', 'Medications', 'Notes'];

export const ConsultationFeatureContainer = () => {
  const classes = useStyles();
  const history = useHistory();
  const { state } = useConsultation();
  const { currentStep } = state;
  const [openConfirmDialog, setOpenConfirmDialog] = useState<boolean>(false);

  const confirmDialogProps: ConfirmDialogProps = {
    open: openConfirmDialog,
    dialogTitle: 'Exit',
    dialogText: 'Would you like to go back to the waiting room?',
    negativeActionText: 'No',
    positiveActionText: 'Yes',
    onClose: (result: boolean) => {
      if (result) {
        history.push(waitingRoomBasePath);
      }

      setOpenConfirmDialog(false);
    },
  };

  const handleBackButtonClick = () => setOpenConfirmDialog(true);

  const showStep = (step: number) => {
    switch (step) {
      case 0:
        return <SymptomsStep />;
      case 1:
        return <ExaminationStep />;
      case 2:
        return <VitalsStep />;
      case 3:
        return <MedicationsStep />;
      case 4:
        return <NotesStep />;
      default:
        throw new Error(`Step ${step} is invalid.`);
    }
  };

  return (
    <>
      <Box paddingLeft="80px" paddingRight="80px" paddingTop="60px">
        <Box display="flex">
          <IconButton className={classes.backIconButton} onClick={handleBackButtonClick}>
            <ChevronLeftIcon />
          </IconButton>
          <Typography variant="h4" className={classes.lightText}>
            Consultation
          </Typography>
          <Typography variant="h4" className={classes.boldText}>
            Details
          </Typography>
        </Box>

        <Box display="flex" flexDirection="column">
          <Stepper activeStep={currentStep} className={classes.stepper}>
            {getSteps().map((label) => (
              <Step key={label}>
                <StepLabel>{label}</StepLabel>
              </Step>
            ))}
          </Stepper>

          {showStep(currentStep)}
        </Box>
      </Box>
      <ConfirmDialog {...confirmDialogProps} />
    </>
  );
};

{
  /* <>
      <Box paddingLeft="80px" paddingRight="80px" paddingTop="60px">
        <Box display="flex">
          <IconButton className={classes.backIconButton} onClick={handleBackButtonClick}>
            <ChevronLeftIcon />
          </IconButton>
          <Typography variant="h4" className={classes.lightText}>
            Consultation
          </Typography>
          <Typography variant="h4" className={classes.boldText}>
            Details
          </Typography>
        </Box>
      </Box>
      <ConfirmDialog {...confirmDialogProps} />
    </> */
}
