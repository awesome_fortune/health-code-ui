import React from 'react';
import { Box, TextField, Typography } from '@material-ui/core';
import { useStyles } from './styles';

export const InformationOverviewFormSection = () => {
  const classes = useStyles();

  return (
    <Box display="flex" flexDirection="column" width="380px" className={classes.container}>
      <Typography variant="h5">Information Overview</Typography>
      <TextField label="Visit Type" disabled />
      <TextField label="Patient Complaints" disabled />
      <TextField label="Duration" disabled />
      <TextField label="Symptoms" disabled />
      <TextField label="Medications taken thus far" disabled />
    </Box>
  );
};
