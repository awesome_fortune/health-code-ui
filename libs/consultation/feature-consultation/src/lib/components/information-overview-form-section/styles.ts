import { cssVariables } from '@health-code-ui/shared/ui';
import { makeStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) => ({
  container: {
    backgroundColor: cssVariables.colors.white,
    boxShadow: `0px 2px 15px ${cssVariables.colors.blackAlpha2}`,
    padding: theme.spacing(4),
  },
  textField: {
    marginTop: theme.spacing(4),
  },
}));
