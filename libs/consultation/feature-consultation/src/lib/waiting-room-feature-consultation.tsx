import React from 'react';
import { ConsultationFeatureContainer } from './components/feature-container';
import { ConsultationProvider } from './context';

export const ConsultationFeatureConsultation = () => {
  return (
    <ConsultationProvider>
      <ConsultationFeatureContainer />
    </ConsultationProvider>
  );
};

export default ConsultationFeatureConsultation;
