import React from 'react';
import { render } from '@testing-library/react';

import WaitingRoomFeatureConsultation from './waiting-room-feature-consultation';

describe('WaitingRoomFeatureConsultation', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<WaitingRoomFeatureConsultation />);
    expect(baseElement).toBeTruthy();
  });
});
