export enum ConsultationActionTypes {
  GoToNextStep = 'GoToNextStep',
  GoToPreviousStep = 'GoToPreviousStep',
  SetSymptomsStepData = 'SetSymptomsStepData',
  SetExaminationStepData = 'SetExaminationStepData',
  SetVitalsStepData = 'SetVitalsStepData',
  SetMedicationsStepData = 'SetMedicationsStepData',
  SetNotesStepData = 'SetNotesStepData',
}

export type ConsultationActions =
  | { type: ConsultationActionTypes.GoToNextStep }
  | { type: ConsultationActionTypes.GoToPreviousStep }
  | { type: ConsultationActionTypes.SetSymptomsStepData; payload: any }
  | { type: ConsultationActionTypes.SetExaminationStepData; payload: any }
  | { type: ConsultationActionTypes.SetVitalsStepData; payload: any }
  | { type: ConsultationActionTypes.SetMedicationsStepData; payload: any }
  | { type: ConsultationActionTypes.SetNotesStepData; payload: any };
