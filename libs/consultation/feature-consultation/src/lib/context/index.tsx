import React, { createContext, useContext, useReducer } from 'react';
import { ConsultationActions, ConsultationActionTypes } from './action';

export interface ConsultationState {
  currentStep: number;
}

const initialState: ConsultationState = {
  currentStep: 0,
};

const ConsultationContext = createContext<any>({ state: initialState, actions: null });

const useActions = (dispatch) => ({
  goToNextStep: () => dispatch({ type: ConsultationActionTypes.GoToNextStep }),
  goToPreviousStep: () => dispatch({ type: ConsultationActionTypes.GoToPreviousStep }),
});

const applySideEffectsMiddleware = (dispatch) => async (action: ConsultationActions) => {
  switch (action.type) {
    default:
      dispatch(action);
  }
};

const consultationReducer = (state: ConsultationState, action: ConsultationActions) => {
  switch (action.type) {
    case ConsultationActionTypes.GoToNextStep:
      return {
        ...state,
        currentStep: state.currentStep + 1,
      };
    case ConsultationActionTypes.GoToPreviousStep:
      return {
        ...state,
        currentStep: state.currentStep - 1,
      };

    default:
      throw new Error(`Unsupported action type ${action.type}`);
  }
};

export const useConsultation = () => {
  const context = useContext(ConsultationContext);

  if (!context) {
    throw new Error('useConsultation must be used within a ConsultationProvider');
  }

  return context;
};

export const ConsultationProvider = (props) => {
  const [state, dispatch] = useReducer(consultationReducer, initialState);
  const actions = useActions(applySideEffectsMiddleware(dispatch));

  return <ConsultationContext.Provider value={{ state, actions }} {...props} />;
};
