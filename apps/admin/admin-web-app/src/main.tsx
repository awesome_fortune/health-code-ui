import React from 'react';
import ReactDOM from 'react-dom';
import { ThemeProvider } from '@material-ui/core/styles';
import { BrowserRouter } from 'react-router-dom';
import App from './app/app';
import { cssVariables, theme } from '@health-code-ui/shared/ui';
import { CssBaseline } from '@material-ui/core';

ReactDOM.render(
  <BrowserRouter>
    <ThemeProvider theme={{ ...theme, ...cssVariables }}>
      <CssBaseline />
      <App />
    </ThemeProvider>
  </BrowserRouter>,
  document.getElementById('root')
);
