import React from 'react';
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';
import { authBasePath, AuthProvider } from '@health-code-ui/auth/state';
import { Footer, FooterProps, NavBar, NavBarProps } from '@health-code-ui/shared/ui';
import { AuthShellAuthAdmin } from '@health-code-ui/auth/shell-auth-admin';
import { QuicklinksShellQuicklinksAdmin } from '@health-code-ui/quicklinks/shell-quicklinks-admin';
import { AppointmentsShellAppointmentsAdmin } from '@health-code-ui/appointments/shell-appointments-admin';
import { PatientsShellPatientsAdmin } from '@health-code-ui/patients/shell-patients-admin';
import './app.css';
import { ProtectedRoute } from './components/protected-route';
import { appointmentsBasePath } from '@health-code-ui/appointments/state';
import { patientsBasePath } from '@health-code-ui/patients/state';
import { quicklinksBasePath } from '@health-code-ui/quicklinks/state';
import { waitingRoomBasePath } from '@health-code-ui/waiting-room/state';
import { WaitingRoomShellWaitingRoomAdmin } from '@health-code-ui/waiting-room/shell-waiting-room-admin';
import { consultationBasePath } from '@health-code-ui/consultation/state';
import { ConsultationShellConsultationAdmin } from '@health-code-ui/consultation/shell-consultation-admin';

const navBarProps: NavBarProps = {
  userDisplayName: 'John Doe',
  links: [
    {
      href: '#',
      name: 'Home',
    },
    {
      href: '#',
      name: 'Menu 1',
    },
    {
      href: '#',
      name: 'Menu 2',
    },
    {
      href: '#',
      name: 'Menu 3',
    },
  ],
};

const footerProps: FooterProps = {
  copyright: `Copyright ${new Date().getFullYear().toString()} Code4Hire H-Code`,
  links: [
    {
      href: '#',
      name: 'Privacy Policy',
    },
    {
      href: '#',
      name: 'Cookie Policy',
    },
    {
      href: '#',
      name: 'Terms & conditions',
    },
    {
      href: '#',
      name: 'Legal',
    },
  ],
};

export const App = () => {
  return (
    <Router>
      <AuthProvider>
        <div className="app">
          <div className="navBar">
            <NavBar {...navBarProps} />
          </div>

          <main>
            <Switch>
              <Route exact path="/">
                <Redirect to={authBasePath} />
              </Route>
              <Route path={authBasePath} component={AuthShellAuthAdmin} />
              <ProtectedRoute path={appointmentsBasePath} component={AppointmentsShellAppointmentsAdmin} />
              <ProtectedRoute path={quicklinksBasePath} component={QuicklinksShellQuicklinksAdmin} />
              <ProtectedRoute path={patientsBasePath} component={PatientsShellPatientsAdmin} />
              <ProtectedRoute path={waitingRoomBasePath} component={WaitingRoomShellWaitingRoomAdmin} />
              <ProtectedRoute path={consultationBasePath} component={ConsultationShellConsultationAdmin} />
            </Switch>
          </main>

          <div className="footer">
            <Footer {...footerProps} />
          </div>
        </div>
      </AuthProvider>
    </Router>
  );
};

export default App;
