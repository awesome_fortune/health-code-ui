import React from 'react';
import { authRoutes, useAuth } from '@health-code-ui/auth/state';
import { Redirect, Route } from 'react-router-dom';

export const ProtectedRoute = ({ component: Component, ...rest }) => {
  const { state } = useAuth();

  return (
    <Route
      {...rest}
      component={(props) =>
        state.user !== null ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: authRoutes.login.path,
              state: { from: props.location },
            }}
          />
        )
      }
    />
  );
};
