import React from 'react';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';
import { AuthShellAuthEndUser } from '@health-code-ui/auth/shell-auth-end-user';
import { authBasePath, AuthProvider } from '@health-code-ui/auth/state';
import './app.css';
import { Footer, FooterProps, NavBar, NavBarProps } from '@health-code-ui/shared/ui';
import { ProtectedRoute } from './components/protected-route';
import { QuicklinksShellQuicklinksEndUser } from '@health-code-ui/quicklinks/shell-quicklinks-end-user';
import { AppointmentsShellAppointmentsEndUser } from '@health-code-ui/appointments/shell-appointments-end-user';
import { appointmentsBasePath } from '@health-code-ui/appointments/state';
import { quicklinksBasePath } from '@health-code-ui/quicklinks/state';

const navBarProps: NavBarProps = {
  userDisplayName: 'John Doe',
  links: [
    {
      href: '#',
      name: 'Home',
    },
    {
      href: '#',
      name: 'Menu 1',
    },
    {
      href: '#',
      name: 'Menu 2',
    },
    {
      href: '#',
      name: 'Menu 3',
    },
  ],
};

const footerProps: FooterProps = {
  copyright: `Copyright ${new Date().getFullYear().toString()} Code4Hire H-Code`,
  links: [
    {
      href: '#',
      name: 'Privacy Policy',
    },
    {
      href: '#',
      name: 'Cookie Policy',
    },
    {
      href: '#',
      name: 'Terms & conditions',
    },
    {
      href: '#',
      name: 'Legal',
    },
  ],
};

export const App = () => {
  return (
    <Router>
      <AuthProvider>
        <div className="app">
          <div className="navBar">
            <NavBar {...navBarProps} />
          </div>

          <main>
            <Route exact path="/">
              <Redirect to={authBasePath} />
            </Route>
            <Route path={authBasePath} component={AuthShellAuthEndUser} />
            <ProtectedRoute path={appointmentsBasePath} component={AppointmentsShellAppointmentsEndUser} />
            <ProtectedRoute path={quicklinksBasePath} component={QuicklinksShellQuicklinksEndUser} />
          </main>

          <div className="footer">
            <Footer {...footerProps} />
          </div>
        </div>
      </AuthProvider>
    </Router>
  );
};

export default App;
